pub global activate protoc_plugin
export PATH="$PATH:/root/.pub-cache/bin"

protoc  --dart_out=lib/api /usr/local/include/google/protobuf/timestamp.proto

#remove old files
rm -rf proto_dart/lib/api/*
mkdir -p proto_dart/lib
mkdir -p proto_dart/lib/api

protoc  -I=minigame_proto/proto/  --dart_out=grpc:lib/api/ minigame_proto/proto/*

