///
//  Generated code. Do not modify.
//  source: user.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'common.pb.dart' as $0;
import 'user.pb.dart' as $1;
export 'user.pb.dart';

class UserControllerClient extends $grpc.Client {
  static final _$getUser = $grpc.ClientMethod<$0.UserId, $1.User>(
      '/user.UserController/GetUser',
      ($0.UserId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.User.fromBuffer(value));
  static final _$addTestUser = $grpc.ClientMethod<$1.User, $1.User>(
      '/user.UserController/AddTestUser',
      ($1.User value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.User.fromBuffer(value));
  static final _$addUser = $grpc.ClientMethod<$1.User, $0.UserId>(
      '/user.UserController/AddUser',
      ($1.User value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.UserId.fromBuffer(value));

  UserControllerClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options,
      $core.Iterable<$grpc.ClientInterceptor> interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$1.User> getUser($0.UserId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getUser, request, options: options);
  }

  $grpc.ResponseFuture<$1.User> addTestUser($1.User request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$addTestUser, request, options: options);
  }

  $grpc.ResponseFuture<$0.UserId> addUser($1.User request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$addUser, request, options: options);
  }
}

abstract class UserControllerServiceBase extends $grpc.Service {
  $core.String get $name => 'user.UserController';

  UserControllerServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.UserId, $1.User>(
        'GetUser',
        getUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserId.fromBuffer(value),
        ($1.User value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.User, $1.User>(
        'AddTestUser',
        addTestUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.User.fromBuffer(value),
        ($1.User value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.User, $0.UserId>(
        'AddUser',
        addUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.User.fromBuffer(value),
        ($0.UserId value) => value.writeToBuffer()));
  }

  $async.Future<$1.User> getUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserId> request) async {
    return getUser(call, await request);
  }

  $async.Future<$1.User> addTestUser_Pre(
      $grpc.ServiceCall call, $async.Future<$1.User> request) async {
    return addTestUser(call, await request);
  }

  $async.Future<$0.UserId> addUser_Pre(
      $grpc.ServiceCall call, $async.Future<$1.User> request) async {
    return addUser(call, await request);
  }

  $async.Future<$1.User> getUser($grpc.ServiceCall call, $0.UserId request);
  $async.Future<$1.User> addTestUser($grpc.ServiceCall call, $1.User request);
  $async.Future<$0.UserId> addUser($grpc.ServiceCall call, $1.User request);
}
