///
//  Generated code. Do not modify.
//  source: auth.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use loginCallDescriptor instead')
const LoginCall$json = const {
  '1': 'LoginCall',
  '2': const [
    const {'1': 'username', '3': 1, '4': 1, '5': 9, '10': 'username'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
  ],
};

/// Descriptor for `LoginCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginCallDescriptor = $convert.base64Decode('CglMb2dpbkNhbGwSGgoIdXNlcm5hbWUYASABKAlSCHVzZXJuYW1lEhoKCHBhc3N3b3JkGAIgASgJUghwYXNzd29yZA==');
@$core.Deprecated('Use logoutCallDescriptor instead')
const LogoutCall$json = const {
  '1': 'LogoutCall',
  '2': const [
    const {'1': 'userId', '3': 1, '4': 1, '5': 11, '6': '.common.UserId', '10': 'userId'},
  ],
};

/// Descriptor for `LogoutCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List logoutCallDescriptor = $convert.base64Decode('CgpMb2dvdXRDYWxsEiYKBnVzZXJJZBgBIAEoCzIOLmNvbW1vbi5Vc2VySWRSBnVzZXJJZA==');
@$core.Deprecated('Use loginResponseDescriptor instead')
const LoginResponse$json = const {
  '1': 'LoginResponse',
  '2': const [
    const {'1': 'token', '3': 1, '4': 1, '5': 9, '10': 'token'},
    const {'1': 'user', '3': 2, '4': 1, '5': 11, '6': '.user.User', '10': 'user'},
  ],
};

/// Descriptor for `LoginResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginResponseDescriptor = $convert.base64Decode('Cg1Mb2dpblJlc3BvbnNlEhQKBXRva2VuGAEgASgJUgV0b2tlbhIeCgR1c2VyGAIgASgLMgoudXNlci5Vc2VyUgR1c2Vy');
@$core.Deprecated('Use logoutResponseDescriptor instead')
const LogoutResponse$json = const {
  '1': 'LogoutResponse',
  '2': const [
    const {'1': 'successful', '3': 1, '4': 1, '5': 8, '10': 'successful'},
  ],
};

/// Descriptor for `LogoutResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List logoutResponseDescriptor = $convert.base64Decode('Cg5Mb2dvdXRSZXNwb25zZRIeCgpzdWNjZXNzZnVsGAEgASgIUgpzdWNjZXNzZnVs');
@$core.Deprecated('Use authServiceCallDescriptor instead')
const AuthServiceCall$json = const {
  '1': 'AuthServiceCall',
  '2': const [
    const {'1': 'login_call', '3': 1, '4': 1, '5': 11, '6': '.auth.LoginCall', '9': 0, '10': 'loginCall'},
    const {'1': 'logout_call', '3': 2, '4': 1, '5': 11, '6': '.auth.LogoutCall', '9': 0, '10': 'logoutCall'},
  ],
  '8': const [
    const {'1': 'message'},
  ],
};

/// Descriptor for `AuthServiceCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List authServiceCallDescriptor = $convert.base64Decode('Cg9BdXRoU2VydmljZUNhbGwSMAoKbG9naW5fY2FsbBgBIAEoCzIPLmF1dGguTG9naW5DYWxsSABSCWxvZ2luQ2FsbBIzCgtsb2dvdXRfY2FsbBgCIAEoCzIQLmF1dGguTG9nb3V0Q2FsbEgAUgpsb2dvdXRDYWxsQgkKB21lc3NhZ2U=');
@$core.Deprecated('Use authServiceResponseDescriptor instead')
const AuthServiceResponse$json = const {
  '1': 'AuthServiceResponse',
  '2': const [
    const {'1': 'login_response', '3': 1, '4': 1, '5': 11, '6': '.auth.LoginResponse', '9': 0, '10': 'loginResponse'},
    const {'1': 'logout_respone', '3': 2, '4': 1, '5': 11, '6': '.auth.LogoutResponse', '9': 0, '10': 'logoutRespone'},
  ],
  '8': const [
    const {'1': 'message'},
  ],
};

/// Descriptor for `AuthServiceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List authServiceResponseDescriptor = $convert.base64Decode('ChNBdXRoU2VydmljZVJlc3BvbnNlEjwKDmxvZ2luX3Jlc3BvbnNlGAEgASgLMhMuYXV0aC5Mb2dpblJlc3BvbnNlSABSDWxvZ2luUmVzcG9uc2USPQoObG9nb3V0X3Jlc3BvbmUYAiABKAsyFC5hdXRoLkxvZ291dFJlc3BvbnNlSABSDWxvZ291dFJlc3BvbmVCCQoHbWVzc2FnZQ==');
