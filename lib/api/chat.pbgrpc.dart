///
//  Generated code. Do not modify.
//  source: chat.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'chat.pb.dart' as $5;
import 'common.pb.dart' as $0;
export 'chat.pb.dart';

class ChatServiceClient extends $grpc.Client {
  static final _$sendMessage = $grpc.ClientMethod<$5.Message, $5.SendResponse>(
      '/chat.ChatService/SendMessage',
      ($5.Message value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $5.SendResponse.fromBuffer(value));
  static final _$getEventStream = $grpc.ClientMethod<$0.UserId, $5.Event>(
      '/chat.ChatService/getEventStream',
      ($0.UserId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $5.Event.fromBuffer(value));
  static final _$joinRoom = $grpc.ClientMethod<$5.JoinRequest, $5.JoinResponse>(
      '/chat.ChatService/JoinRoom',
      ($5.JoinRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $5.JoinResponse.fromBuffer(value));
  static final _$leaveRoom =
      $grpc.ClientMethod<$5.LeaveRequest, $5.LeaveResponse>(
          '/chat.ChatService/LeaveRoom',
          ($5.LeaveRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $5.LeaveResponse.fromBuffer(value));
  static final _$getRoomsByUser = $grpc.ClientMethod<$0.UserId, $5.Rooms>(
      '/chat.ChatService/GetRoomsByUser',
      ($0.UserId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $5.Rooms.fromBuffer(value));
  static final _$getAllRooms = $grpc.ClientMethod<$5.RoomRequest, $5.Rooms>(
      '/chat.ChatService/getAllRooms',
      ($5.RoomRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $5.Rooms.fromBuffer(value));
  static final _$getLastMessagesByRoom =
      $grpc.ClientMethod<$5.LastMessagesRequest, $5.Messages>(
          '/chat.ChatService/GetLastMessagesByRoom',
          ($5.LastMessagesRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $5.Messages.fromBuffer(value));

  ChatServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options,
      $core.Iterable<$grpc.ClientInterceptor> interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$5.SendResponse> sendMessage($5.Message request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$sendMessage, request, options: options);
  }

  $grpc.ResponseStream<$5.Event> getEventStream($0.UserId request,
      {$grpc.CallOptions options}) {
    return $createStreamingCall(
        _$getEventStream, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$5.JoinResponse> joinRoom($5.JoinRequest request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$joinRoom, request, options: options);
  }

  $grpc.ResponseFuture<$5.LeaveResponse> leaveRoom($5.LeaveRequest request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$leaveRoom, request, options: options);
  }

  $grpc.ResponseFuture<$5.Rooms> getRoomsByUser($0.UserId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getRoomsByUser, request, options: options);
  }

  $grpc.ResponseFuture<$5.Rooms> getAllRooms($5.RoomRequest request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getAllRooms, request, options: options);
  }

  $grpc.ResponseFuture<$5.Messages> getLastMessagesByRoom(
      $5.LastMessagesRequest request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getLastMessagesByRoom, request, options: options);
  }
}

abstract class ChatServiceBase extends $grpc.Service {
  $core.String get $name => 'chat.ChatService';

  ChatServiceBase() {
    $addMethod($grpc.ServiceMethod<$5.Message, $5.SendResponse>(
        'SendMessage',
        sendMessage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $5.Message.fromBuffer(value),
        ($5.SendResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserId, $5.Event>(
        'getEventStream',
        getEventStream_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.UserId.fromBuffer(value),
        ($5.Event value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$5.JoinRequest, $5.JoinResponse>(
        'JoinRoom',
        joinRoom_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $5.JoinRequest.fromBuffer(value),
        ($5.JoinResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$5.LeaveRequest, $5.LeaveResponse>(
        'LeaveRoom',
        leaveRoom_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $5.LeaveRequest.fromBuffer(value),
        ($5.LeaveResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserId, $5.Rooms>(
        'GetRoomsByUser',
        getRoomsByUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserId.fromBuffer(value),
        ($5.Rooms value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$5.RoomRequest, $5.Rooms>(
        'getAllRooms',
        getAllRooms_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $5.RoomRequest.fromBuffer(value),
        ($5.Rooms value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$5.LastMessagesRequest, $5.Messages>(
        'GetLastMessagesByRoom',
        getLastMessagesByRoom_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $5.LastMessagesRequest.fromBuffer(value),
        ($5.Messages value) => value.writeToBuffer()));
  }

  $async.Future<$5.SendResponse> sendMessage_Pre(
      $grpc.ServiceCall call, $async.Future<$5.Message> request) async {
    return sendMessage(call, await request);
  }

  $async.Stream<$5.Event> getEventStream_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserId> request) async* {
    yield* getEventStream(call, await request);
  }

  $async.Future<$5.JoinResponse> joinRoom_Pre(
      $grpc.ServiceCall call, $async.Future<$5.JoinRequest> request) async {
    return joinRoom(call, await request);
  }

  $async.Future<$5.LeaveResponse> leaveRoom_Pre(
      $grpc.ServiceCall call, $async.Future<$5.LeaveRequest> request) async {
    return leaveRoom(call, await request);
  }

  $async.Future<$5.Rooms> getRoomsByUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserId> request) async {
    return getRoomsByUser(call, await request);
  }

  $async.Future<$5.Rooms> getAllRooms_Pre(
      $grpc.ServiceCall call, $async.Future<$5.RoomRequest> request) async {
    return getAllRooms(call, await request);
  }

  $async.Future<$5.Messages> getLastMessagesByRoom_Pre($grpc.ServiceCall call,
      $async.Future<$5.LastMessagesRequest> request) async {
    return getLastMessagesByRoom(call, await request);
  }

  $async.Future<$5.SendResponse> sendMessage(
      $grpc.ServiceCall call, $5.Message request);
  $async.Stream<$5.Event> getEventStream(
      $grpc.ServiceCall call, $0.UserId request);
  $async.Future<$5.JoinResponse> joinRoom(
      $grpc.ServiceCall call, $5.JoinRequest request);
  $async.Future<$5.LeaveResponse> leaveRoom(
      $grpc.ServiceCall call, $5.LeaveRequest request);
  $async.Future<$5.Rooms> getRoomsByUser(
      $grpc.ServiceCall call, $0.UserId request);
  $async.Future<$5.Rooms> getAllRooms(
      $grpc.ServiceCall call, $5.RoomRequest request);
  $async.Future<$5.Messages> getLastMessagesByRoom(
      $grpc.ServiceCall call, $5.LastMessagesRequest request);
}
