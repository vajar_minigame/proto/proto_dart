///
//  Generated code. Do not modify.
//  source: controller.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'monster.pb.dart' as $3;
import 'inventory.pb.dart' as $6;
import 'quest.pb.dart' as $8;
import 'battle.pb.dart' as $4;
import 'auth.pb.dart' as $2;
import 'user.pb.dart' as $1;
import 'interactions.pb.dart' as $7;

enum Event_Event {
  monEvent, 
  itemEvent, 
  questEvent, 
  battleEvent, 
  notSet
}

class Event extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Event_Event> _Event_EventByTag = {
    1 : Event_Event.monEvent,
    2 : Event_Event.itemEvent,
    3 : Event_Event.questEvent,
    4 : Event_Event.battleEvent,
    0 : Event_Event.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Event', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ctrlmsg'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4])
    ..aOM<$3.MonsterEvent>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'monEvent', protoName: 'monEvent', subBuilder: $3.MonsterEvent.create)
    ..aOM<$6.ItemEvent>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemEvent', protoName: 'itemEvent', subBuilder: $6.ItemEvent.create)
    ..aOM<$8.QuestEvent>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'questEvent', protoName: 'questEvent', subBuilder: $8.QuestEvent.create)
    ..aOM<$4.BattleEvent>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'battleEvent', protoName: 'battleEvent', subBuilder: $4.BattleEvent.create)
    ..hasRequiredFields = false
  ;

  Event._() : super();
  factory Event({
    $3.MonsterEvent monEvent,
    $6.ItemEvent itemEvent,
    $8.QuestEvent questEvent,
    $4.BattleEvent battleEvent,
  }) {
    final _result = create();
    if (monEvent != null) {
      _result.monEvent = monEvent;
    }
    if (itemEvent != null) {
      _result.itemEvent = itemEvent;
    }
    if (questEvent != null) {
      _result.questEvent = questEvent;
    }
    if (battleEvent != null) {
      _result.battleEvent = battleEvent;
    }
    return _result;
  }
  factory Event.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Event.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Event clone() => Event()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Event copyWith(void Function(Event) updates) => super.copyWith((message) => updates(message as Event)) as Event; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Event create() => Event._();
  Event createEmptyInstance() => create();
  static $pb.PbList<Event> createRepeated() => $pb.PbList<Event>();
  @$core.pragma('dart2js:noInline')
  static Event getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Event>(create);
  static Event _defaultInstance;

  Event_Event whichEvent() => _Event_EventByTag[$_whichOneof(0)];
  void clearEvent() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $3.MonsterEvent get monEvent => $_getN(0);
  @$pb.TagNumber(1)
  set monEvent($3.MonsterEvent v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMonEvent() => $_has(0);
  @$pb.TagNumber(1)
  void clearMonEvent() => clearField(1);
  @$pb.TagNumber(1)
  $3.MonsterEvent ensureMonEvent() => $_ensure(0);

  @$pb.TagNumber(2)
  $6.ItemEvent get itemEvent => $_getN(1);
  @$pb.TagNumber(2)
  set itemEvent($6.ItemEvent v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasItemEvent() => $_has(1);
  @$pb.TagNumber(2)
  void clearItemEvent() => clearField(2);
  @$pb.TagNumber(2)
  $6.ItemEvent ensureItemEvent() => $_ensure(1);

  @$pb.TagNumber(3)
  $8.QuestEvent get questEvent => $_getN(2);
  @$pb.TagNumber(3)
  set questEvent($8.QuestEvent v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasQuestEvent() => $_has(2);
  @$pb.TagNumber(3)
  void clearQuestEvent() => clearField(3);
  @$pb.TagNumber(3)
  $8.QuestEvent ensureQuestEvent() => $_ensure(2);

  @$pb.TagNumber(4)
  $4.BattleEvent get battleEvent => $_getN(3);
  @$pb.TagNumber(4)
  set battleEvent($4.BattleEvent v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasBattleEvent() => $_has(3);
  @$pb.TagNumber(4)
  void clearBattleEvent() => clearField(4);
  @$pb.TagNumber(4)
  $4.BattleEvent ensureBattleEvent() => $_ensure(3);
}

enum FuncCall_Message {
  authServiceCall, 
  monServiceCall, 
  userServiceCall, 
  invServiceCall, 
  interactServiceCall, 
  questServiceCall, 
  notSet
}

class FuncCall extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, FuncCall_Message> _FuncCall_MessageByTag = {
    1 : FuncCall_Message.authServiceCall,
    2 : FuncCall_Message.monServiceCall,
    3 : FuncCall_Message.userServiceCall,
    4 : FuncCall_Message.invServiceCall,
    5 : FuncCall_Message.interactServiceCall,
    6 : FuncCall_Message.questServiceCall,
    0 : FuncCall_Message.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FuncCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ctrlmsg'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4, 5, 6])
    ..aOM<$2.AuthServiceCall>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'authServiceCall', subBuilder: $2.AuthServiceCall.create)
    ..aOM<$3.MonServiceCall>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'monServiceCall', subBuilder: $3.MonServiceCall.create)
    ..aOM<$1.UserServiceCall>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userServiceCall', subBuilder: $1.UserServiceCall.create)
    ..aOM<$6.InventoryServiceCall>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'invServiceCall', subBuilder: $6.InventoryServiceCall.create)
    ..aOM<$7.InteractionServiceCall>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'interactServiceCall', subBuilder: $7.InteractionServiceCall.create)
    ..aOM<$8.QuestServiceCall>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'questServiceCall', subBuilder: $8.QuestServiceCall.create)
    ..hasRequiredFields = false
  ;

  FuncCall._() : super();
  factory FuncCall({
    $2.AuthServiceCall authServiceCall,
    $3.MonServiceCall monServiceCall,
    $1.UserServiceCall userServiceCall,
    $6.InventoryServiceCall invServiceCall,
    $7.InteractionServiceCall interactServiceCall,
    $8.QuestServiceCall questServiceCall,
  }) {
    final _result = create();
    if (authServiceCall != null) {
      _result.authServiceCall = authServiceCall;
    }
    if (monServiceCall != null) {
      _result.monServiceCall = monServiceCall;
    }
    if (userServiceCall != null) {
      _result.userServiceCall = userServiceCall;
    }
    if (invServiceCall != null) {
      _result.invServiceCall = invServiceCall;
    }
    if (interactServiceCall != null) {
      _result.interactServiceCall = interactServiceCall;
    }
    if (questServiceCall != null) {
      _result.questServiceCall = questServiceCall;
    }
    return _result;
  }
  factory FuncCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FuncCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FuncCall clone() => FuncCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FuncCall copyWith(void Function(FuncCall) updates) => super.copyWith((message) => updates(message as FuncCall)) as FuncCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FuncCall create() => FuncCall._();
  FuncCall createEmptyInstance() => create();
  static $pb.PbList<FuncCall> createRepeated() => $pb.PbList<FuncCall>();
  @$core.pragma('dart2js:noInline')
  static FuncCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FuncCall>(create);
  static FuncCall _defaultInstance;

  FuncCall_Message whichMessage() => _FuncCall_MessageByTag[$_whichOneof(0)];
  void clearMessage() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $2.AuthServiceCall get authServiceCall => $_getN(0);
  @$pb.TagNumber(1)
  set authServiceCall($2.AuthServiceCall v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasAuthServiceCall() => $_has(0);
  @$pb.TagNumber(1)
  void clearAuthServiceCall() => clearField(1);
  @$pb.TagNumber(1)
  $2.AuthServiceCall ensureAuthServiceCall() => $_ensure(0);

  @$pb.TagNumber(2)
  $3.MonServiceCall get monServiceCall => $_getN(1);
  @$pb.TagNumber(2)
  set monServiceCall($3.MonServiceCall v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasMonServiceCall() => $_has(1);
  @$pb.TagNumber(2)
  void clearMonServiceCall() => clearField(2);
  @$pb.TagNumber(2)
  $3.MonServiceCall ensureMonServiceCall() => $_ensure(1);

  @$pb.TagNumber(3)
  $1.UserServiceCall get userServiceCall => $_getN(2);
  @$pb.TagNumber(3)
  set userServiceCall($1.UserServiceCall v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasUserServiceCall() => $_has(2);
  @$pb.TagNumber(3)
  void clearUserServiceCall() => clearField(3);
  @$pb.TagNumber(3)
  $1.UserServiceCall ensureUserServiceCall() => $_ensure(2);

  @$pb.TagNumber(4)
  $6.InventoryServiceCall get invServiceCall => $_getN(3);
  @$pb.TagNumber(4)
  set invServiceCall($6.InventoryServiceCall v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasInvServiceCall() => $_has(3);
  @$pb.TagNumber(4)
  void clearInvServiceCall() => clearField(4);
  @$pb.TagNumber(4)
  $6.InventoryServiceCall ensureInvServiceCall() => $_ensure(3);

  @$pb.TagNumber(5)
  $7.InteractionServiceCall get interactServiceCall => $_getN(4);
  @$pb.TagNumber(5)
  set interactServiceCall($7.InteractionServiceCall v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasInteractServiceCall() => $_has(4);
  @$pb.TagNumber(5)
  void clearInteractServiceCall() => clearField(5);
  @$pb.TagNumber(5)
  $7.InteractionServiceCall ensureInteractServiceCall() => $_ensure(4);

  @$pb.TagNumber(6)
  $8.QuestServiceCall get questServiceCall => $_getN(5);
  @$pb.TagNumber(6)
  set questServiceCall($8.QuestServiceCall v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasQuestServiceCall() => $_has(5);
  @$pb.TagNumber(6)
  void clearQuestServiceCall() => clearField(6);
  @$pb.TagNumber(6)
  $8.QuestServiceCall ensureQuestServiceCall() => $_ensure(5);
}

enum Message_Msg {
  call, 
  response, 
  event, 
  notSet
}

class Message extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Message_Msg> _Message_MsgByTag = {
    1 : Message_Msg.call,
    2 : Message_Msg.response,
    3 : Message_Msg.event,
    0 : Message_Msg.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Message', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ctrlmsg'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3])
    ..aOM<FuncCall>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'call', subBuilder: FuncCall.create)
    ..aOM<FuncResponse>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'response', subBuilder: FuncResponse.create)
    ..aOM<Event>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'event', subBuilder: Event.create)
    ..hasRequiredFields = false
  ;

  Message._() : super();
  factory Message({
    FuncCall call,
    FuncResponse response,
    Event event,
  }) {
    final _result = create();
    if (call != null) {
      _result.call = call;
    }
    if (response != null) {
      _result.response = response;
    }
    if (event != null) {
      _result.event = event;
    }
    return _result;
  }
  factory Message.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Message.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Message clone() => Message()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Message copyWith(void Function(Message) updates) => super.copyWith((message) => updates(message as Message)) as Message; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Message create() => Message._();
  Message createEmptyInstance() => create();
  static $pb.PbList<Message> createRepeated() => $pb.PbList<Message>();
  @$core.pragma('dart2js:noInline')
  static Message getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Message>(create);
  static Message _defaultInstance;

  Message_Msg whichMsg() => _Message_MsgByTag[$_whichOneof(0)];
  void clearMsg() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  FuncCall get call => $_getN(0);
  @$pb.TagNumber(1)
  set call(FuncCall v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasCall() => $_has(0);
  @$pb.TagNumber(1)
  void clearCall() => clearField(1);
  @$pb.TagNumber(1)
  FuncCall ensureCall() => $_ensure(0);

  @$pb.TagNumber(2)
  FuncResponse get response => $_getN(1);
  @$pb.TagNumber(2)
  set response(FuncResponse v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasResponse() => $_has(1);
  @$pb.TagNumber(2)
  void clearResponse() => clearField(2);
  @$pb.TagNumber(2)
  FuncResponse ensureResponse() => $_ensure(1);

  @$pb.TagNumber(3)
  Event get event => $_getN(2);
  @$pb.TagNumber(3)
  set event(Event v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasEvent() => $_has(2);
  @$pb.TagNumber(3)
  void clearEvent() => clearField(3);
  @$pb.TagNumber(3)
  Event ensureEvent() => $_ensure(2);
}

enum FuncResponse_Message {
  monServiceRes, 
  authServiceRes, 
  userServiceRes, 
  invServiceRes, 
  interactServiceRes, 
  questServiceRes, 
  notSet
}

class FuncResponse extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, FuncResponse_Message> _FuncResponse_MessageByTag = {
    1 : FuncResponse_Message.monServiceRes,
    2 : FuncResponse_Message.authServiceRes,
    3 : FuncResponse_Message.userServiceRes,
    4 : FuncResponse_Message.invServiceRes,
    5 : FuncResponse_Message.interactServiceRes,
    6 : FuncResponse_Message.questServiceRes,
    0 : FuncResponse_Message.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FuncResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ctrlmsg'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4, 5, 6])
    ..aOM<$3.MonServiceResponse>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'monServiceRes', subBuilder: $3.MonServiceResponse.create)
    ..aOM<$2.AuthServiceResponse>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'authServiceRes', subBuilder: $2.AuthServiceResponse.create)
    ..aOM<$1.UserServiceResponse>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userServiceRes', subBuilder: $1.UserServiceResponse.create)
    ..aOM<$6.InventoryServiceResponse>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'invServiceRes', subBuilder: $6.InventoryServiceResponse.create)
    ..aOM<$7.InteractionServiceResponse>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'interactServiceRes', subBuilder: $7.InteractionServiceResponse.create)
    ..aOM<$8.QuestServiceResponse>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'questServiceRes', subBuilder: $8.QuestServiceResponse.create)
    ..hasRequiredFields = false
  ;

  FuncResponse._() : super();
  factory FuncResponse({
    $3.MonServiceResponse monServiceRes,
    $2.AuthServiceResponse authServiceRes,
    $1.UserServiceResponse userServiceRes,
    $6.InventoryServiceResponse invServiceRes,
    $7.InteractionServiceResponse interactServiceRes,
    $8.QuestServiceResponse questServiceRes,
  }) {
    final _result = create();
    if (monServiceRes != null) {
      _result.monServiceRes = monServiceRes;
    }
    if (authServiceRes != null) {
      _result.authServiceRes = authServiceRes;
    }
    if (userServiceRes != null) {
      _result.userServiceRes = userServiceRes;
    }
    if (invServiceRes != null) {
      _result.invServiceRes = invServiceRes;
    }
    if (interactServiceRes != null) {
      _result.interactServiceRes = interactServiceRes;
    }
    if (questServiceRes != null) {
      _result.questServiceRes = questServiceRes;
    }
    return _result;
  }
  factory FuncResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FuncResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FuncResponse clone() => FuncResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FuncResponse copyWith(void Function(FuncResponse) updates) => super.copyWith((message) => updates(message as FuncResponse)) as FuncResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FuncResponse create() => FuncResponse._();
  FuncResponse createEmptyInstance() => create();
  static $pb.PbList<FuncResponse> createRepeated() => $pb.PbList<FuncResponse>();
  @$core.pragma('dart2js:noInline')
  static FuncResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FuncResponse>(create);
  static FuncResponse _defaultInstance;

  FuncResponse_Message whichMessage() => _FuncResponse_MessageByTag[$_whichOneof(0)];
  void clearMessage() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $3.MonServiceResponse get monServiceRes => $_getN(0);
  @$pb.TagNumber(1)
  set monServiceRes($3.MonServiceResponse v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMonServiceRes() => $_has(0);
  @$pb.TagNumber(1)
  void clearMonServiceRes() => clearField(1);
  @$pb.TagNumber(1)
  $3.MonServiceResponse ensureMonServiceRes() => $_ensure(0);

  @$pb.TagNumber(2)
  $2.AuthServiceResponse get authServiceRes => $_getN(1);
  @$pb.TagNumber(2)
  set authServiceRes($2.AuthServiceResponse v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasAuthServiceRes() => $_has(1);
  @$pb.TagNumber(2)
  void clearAuthServiceRes() => clearField(2);
  @$pb.TagNumber(2)
  $2.AuthServiceResponse ensureAuthServiceRes() => $_ensure(1);

  @$pb.TagNumber(3)
  $1.UserServiceResponse get userServiceRes => $_getN(2);
  @$pb.TagNumber(3)
  set userServiceRes($1.UserServiceResponse v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasUserServiceRes() => $_has(2);
  @$pb.TagNumber(3)
  void clearUserServiceRes() => clearField(3);
  @$pb.TagNumber(3)
  $1.UserServiceResponse ensureUserServiceRes() => $_ensure(2);

  @$pb.TagNumber(4)
  $6.InventoryServiceResponse get invServiceRes => $_getN(3);
  @$pb.TagNumber(4)
  set invServiceRes($6.InventoryServiceResponse v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasInvServiceRes() => $_has(3);
  @$pb.TagNumber(4)
  void clearInvServiceRes() => clearField(4);
  @$pb.TagNumber(4)
  $6.InventoryServiceResponse ensureInvServiceRes() => $_ensure(3);

  @$pb.TagNumber(5)
  $7.InteractionServiceResponse get interactServiceRes => $_getN(4);
  @$pb.TagNumber(5)
  set interactServiceRes($7.InteractionServiceResponse v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasInteractServiceRes() => $_has(4);
  @$pb.TagNumber(5)
  void clearInteractServiceRes() => clearField(5);
  @$pb.TagNumber(5)
  $7.InteractionServiceResponse ensureInteractServiceRes() => $_ensure(4);

  @$pb.TagNumber(6)
  $8.QuestServiceResponse get questServiceRes => $_getN(5);
  @$pb.TagNumber(6)
  set questServiceRes($8.QuestServiceResponse v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasQuestServiceRes() => $_has(5);
  @$pb.TagNumber(6)
  void clearQuestServiceRes() => clearField(6);
  @$pb.TagNumber(6)
  $8.QuestServiceResponse ensureQuestServiceRes() => $_ensure(5);
}

