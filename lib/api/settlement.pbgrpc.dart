///
//  Generated code. Do not modify.
//  source: settlement.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'settlement.pb.dart' as $11;
import 'common.pb.dart' as $0;
export 'settlement.pb.dart';

class SettlementServicesClient extends $grpc.Client {
  static final _$addSettlement =
      $grpc.ClientMethod<$11.Settlement, $11.Settlement>(
          '/settlement.SettlementServices/AddSettlement',
          ($11.Settlement value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $11.Settlement.fromBuffer(value));
  static final _$addTestSettlement =
      $grpc.ClientMethod<$11.Settlement, $11.Settlement>(
          '/settlement.SettlementServices/AddTestSettlement',
          ($11.Settlement value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $11.Settlement.fromBuffer(value));
  static final _$getSettlementByID =
      $grpc.ClientMethod<$0.SettlementId, $11.Settlement>(
          '/settlement.SettlementServices/GetSettlementByID',
          ($0.SettlementId value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $11.Settlement.fromBuffer(value));
  static final _$getSettlementsByUserID =
      $grpc.ClientMethod<$0.UserId, $11.SettlementList>(
          '/settlement.SettlementServices/GetSettlementsByUserID',
          ($0.UserId value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $11.SettlementList.fromBuffer(value));
  static final _$deleteSettlement =
      $grpc.ClientMethod<$0.SettlementId, $0.ResponseStatus>(
          '/settlement.SettlementServices/DeleteSettlement',
          ($0.SettlementId value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));
  static final _$buildBuilding =
      $grpc.ClientMethod<$11.BuildBuildingCall, $0.ResponseStatus>(
          '/settlement.SettlementServices/BuildBuilding',
          ($11.BuildBuildingCall value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));
  static final _$upgradeBuilding =
      $grpc.ClientMethod<$11.UpgradeBuildingCall, $0.ResponseStatus>(
          '/settlement.SettlementServices/UpgradeBuilding',
          ($11.UpgradeBuildingCall value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));

  SettlementServicesClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options,
      $core.Iterable<$grpc.ClientInterceptor> interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$11.Settlement> addSettlement($11.Settlement request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$addSettlement, request, options: options);
  }

  $grpc.ResponseFuture<$11.Settlement> addTestSettlement($11.Settlement request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$addTestSettlement, request, options: options);
  }

  $grpc.ResponseFuture<$11.Settlement> getSettlementByID(
      $0.SettlementId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getSettlementByID, request, options: options);
  }

  $grpc.ResponseFuture<$11.SettlementList> getSettlementsByUserID(
      $0.UserId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getSettlementsByUserID, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.ResponseStatus> deleteSettlement(
      $0.SettlementId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$deleteSettlement, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResponseStatus> buildBuilding(
      $11.BuildBuildingCall request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$buildBuilding, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResponseStatus> upgradeBuilding(
      $11.UpgradeBuildingCall request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$upgradeBuilding, request, options: options);
  }
}

abstract class SettlementServicesServiceBase extends $grpc.Service {
  $core.String get $name => 'settlement.SettlementServices';

  SettlementServicesServiceBase() {
    $addMethod($grpc.ServiceMethod<$11.Settlement, $11.Settlement>(
        'AddSettlement',
        addSettlement_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $11.Settlement.fromBuffer(value),
        ($11.Settlement value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$11.Settlement, $11.Settlement>(
        'AddTestSettlement',
        addTestSettlement_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $11.Settlement.fromBuffer(value),
        ($11.Settlement value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SettlementId, $11.Settlement>(
        'GetSettlementByID',
        getSettlementByID_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SettlementId.fromBuffer(value),
        ($11.Settlement value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserId, $11.SettlementList>(
        'GetSettlementsByUserID',
        getSettlementsByUserID_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserId.fromBuffer(value),
        ($11.SettlementList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SettlementId, $0.ResponseStatus>(
        'DeleteSettlement',
        deleteSettlement_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SettlementId.fromBuffer(value),
        ($0.ResponseStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$11.BuildBuildingCall, $0.ResponseStatus>(
        'BuildBuilding',
        buildBuilding_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $11.BuildBuildingCall.fromBuffer(value),
        ($0.ResponseStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$11.UpgradeBuildingCall, $0.ResponseStatus>(
        'UpgradeBuilding',
        upgradeBuilding_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $11.UpgradeBuildingCall.fromBuffer(value),
        ($0.ResponseStatus value) => value.writeToBuffer()));
  }

  $async.Future<$11.Settlement> addSettlement_Pre(
      $grpc.ServiceCall call, $async.Future<$11.Settlement> request) async {
    return addSettlement(call, await request);
  }

  $async.Future<$11.Settlement> addTestSettlement_Pre(
      $grpc.ServiceCall call, $async.Future<$11.Settlement> request) async {
    return addTestSettlement(call, await request);
  }

  $async.Future<$11.Settlement> getSettlementByID_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SettlementId> request) async {
    return getSettlementByID(call, await request);
  }

  $async.Future<$11.SettlementList> getSettlementsByUserID_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserId> request) async {
    return getSettlementsByUserID(call, await request);
  }

  $async.Future<$0.ResponseStatus> deleteSettlement_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SettlementId> request) async {
    return deleteSettlement(call, await request);
  }

  $async.Future<$0.ResponseStatus> buildBuilding_Pre($grpc.ServiceCall call,
      $async.Future<$11.BuildBuildingCall> request) async {
    return buildBuilding(call, await request);
  }

  $async.Future<$0.ResponseStatus> upgradeBuilding_Pre($grpc.ServiceCall call,
      $async.Future<$11.UpgradeBuildingCall> request) async {
    return upgradeBuilding(call, await request);
  }

  $async.Future<$11.Settlement> addSettlement(
      $grpc.ServiceCall call, $11.Settlement request);
  $async.Future<$11.Settlement> addTestSettlement(
      $grpc.ServiceCall call, $11.Settlement request);
  $async.Future<$11.Settlement> getSettlementByID(
      $grpc.ServiceCall call, $0.SettlementId request);
  $async.Future<$11.SettlementList> getSettlementsByUserID(
      $grpc.ServiceCall call, $0.UserId request);
  $async.Future<$0.ResponseStatus> deleteSettlement(
      $grpc.ServiceCall call, $0.SettlementId request);
  $async.Future<$0.ResponseStatus> buildBuilding(
      $grpc.ServiceCall call, $11.BuildBuildingCall request);
  $async.Future<$0.ResponseStatus> upgradeBuilding(
      $grpc.ServiceCall call, $11.UpgradeBuildingCall request);
}
