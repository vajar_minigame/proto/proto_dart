///
//  Generated code. Do not modify.
//  source: inventory.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use inventoryDescriptor instead')
const Inventory$json = const {
  '1': 'Inventory',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.common.UserId', '10': 'id'},
    const {'1': 'items', '3': 2, '4': 3, '5': 11, '6': '.item.Item', '10': 'items'},
  ],
};

/// Descriptor for `Inventory`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inventoryDescriptor = $convert.base64Decode('CglJbnZlbnRvcnkSHgoCaWQYASABKAsyDi5jb21tb24uVXNlcklkUgJpZBIgCgVpdGVtcxgCIAMoCzIKLml0ZW0uSXRlbVIFaXRlbXM=');
@$core.Deprecated('Use itemListDescriptor instead')
const ItemList$json = const {
  '1': 'ItemList',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.item.Item', '10': 'items'},
  ],
};

/// Descriptor for `ItemList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemListDescriptor = $convert.base64Decode('CghJdGVtTGlzdBIgCgVpdGVtcxgBIAMoCzIKLml0ZW0uSXRlbVIFaXRlbXM=');
@$core.Deprecated('Use itemDefListDescriptor instead')
const ItemDefList$json = const {
  '1': 'ItemDefList',
  '2': const [
    const {'1': 'itemDefs', '3': 1, '4': 3, '5': 11, '6': '.item.ItemDefinition', '10': 'itemDefs'},
  ],
};

/// Descriptor for `ItemDefList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemDefListDescriptor = $convert.base64Decode('CgtJdGVtRGVmTGlzdBIwCghpdGVtRGVmcxgBIAMoCzIULml0ZW0uSXRlbURlZmluaXRpb25SCGl0ZW1EZWZz');
@$core.Deprecated('Use itemIdListDescriptor instead')
const ItemIdList$json = const {
  '1': 'ItemIdList',
  '2': const [
    const {'1': 'itemIds', '3': 1, '4': 3, '5': 11, '6': '.common.ItemId', '10': 'itemIds'},
  ],
};

/// Descriptor for `ItemIdList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemIdListDescriptor = $convert.base64Decode('CgpJdGVtSWRMaXN0EigKB2l0ZW1JZHMYASADKAsyDi5jb21tb24uSXRlbUlkUgdpdGVtSWRz');
@$core.Deprecated('Use itemDefinitionDescriptor instead')
const ItemDefinition$json = const {
  '1': 'ItemDefinition',
  '2': const [
    const {'1': 'type', '3': 2, '4': 1, '5': 9, '10': 'type'},
    const {'1': 'tags', '3': 3, '4': 3, '5': 9, '10': 'tags'},
    const {'1': 'food', '3': 4, '4': 1, '5': 11, '6': '.item.Food', '9': 0, '10': 'food'},
  ],
  '8': const [
    const {'1': 'itemContent'},
  ],
};

/// Descriptor for `ItemDefinition`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemDefinitionDescriptor = $convert.base64Decode('Cg5JdGVtRGVmaW5pdGlvbhISCgR0eXBlGAIgASgJUgR0eXBlEhIKBHRhZ3MYAyADKAlSBHRhZ3MSIAoEZm9vZBgEIAEoCzIKLml0ZW0uRm9vZEgAUgRmb29kQg0KC2l0ZW1Db250ZW50');
@$core.Deprecated('Use itemDescriptor instead')
const Item$json = const {
  '1': 'Item',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
    const {'1': 'itemDef', '3': 2, '4': 1, '5': 11, '6': '.item.ItemDefinition', '10': 'itemDef'},
  ],
};

/// Descriptor for `Item`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemDescriptor = $convert.base64Decode('CgRJdGVtEg4KAmlkGAEgASgFUgJpZBIuCgdpdGVtRGVmGAIgASgLMhQuaXRlbS5JdGVtRGVmaW5pdGlvblIHaXRlbURlZg==');
@$core.Deprecated('Use effectDescriptor instead')
const Effect$json = const {
  '1': 'Effect',
  '2': const [
    const {'1': 'buff', '3': 1, '4': 1, '5': 11, '6': '.item.Buff', '9': 0, '10': 'buff'},
    const {'1': 'rec', '3': 2, '4': 1, '5': 11, '6': '.item.Recovery', '9': 0, '10': 'rec'},
  ],
  '8': const [
    const {'1': 'effect'},
  ],
};

/// Descriptor for `Effect`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List effectDescriptor = $convert.base64Decode('CgZFZmZlY3QSIAoEYnVmZhgBIAEoCzIKLml0ZW0uQnVmZkgAUgRidWZmEiIKA3JlYxgCIAEoCzIOLml0ZW0uUmVjb3ZlcnlIAFIDcmVjQggKBmVmZmVjdA==');
@$core.Deprecated('Use buffDescriptor instead')
const Buff$json = const {
  '1': 'Buff',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 9, '10': 'type'},
    const {'1': 'value', '3': 2, '4': 1, '5': 2, '10': 'value'},
  ],
};

/// Descriptor for `Buff`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List buffDescriptor = $convert.base64Decode('CgRCdWZmEhIKBHR5cGUYASABKAlSBHR5cGUSFAoFdmFsdWUYAiABKAJSBXZhbHVl');
@$core.Deprecated('Use recoveryDescriptor instead')
const Recovery$json = const {
  '1': 'Recovery',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 9, '10': 'type'},
    const {'1': 'value', '3': 2, '4': 1, '5': 2, '10': 'value'},
  ],
};

/// Descriptor for `Recovery`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List recoveryDescriptor = $convert.base64Decode('CghSZWNvdmVyeRISCgR0eXBlGAEgASgJUgR0eXBlEhQKBXZhbHVlGAIgASgCUgV2YWx1ZQ==');
@$core.Deprecated('Use pageDescriptor instead')
const Page$json = const {
  '1': 'Page',
  '2': const [
    const {'1': 'size', '3': 1, '4': 1, '5': 5, '10': 'size'},
    const {'1': 'count', '3': 2, '4': 1, '5': 5, '10': 'count'},
  ],
};

/// Descriptor for `Page`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pageDescriptor = $convert.base64Decode('CgRQYWdlEhIKBHNpemUYASABKAVSBHNpemUSFAoFY291bnQYAiABKAVSBWNvdW50');
@$core.Deprecated('Use foodDescriptor instead')
const Food$json = const {
  '1': 'Food',
  '2': const [
    const {'1': 'effects', '3': 1, '4': 3, '5': 11, '6': '.item.Effect', '10': 'effects'},
  ],
};

/// Descriptor for `Food`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List foodDescriptor = $convert.base64Decode('CgRGb29kEiYKB2VmZmVjdHMYASADKAsyDC5pdGVtLkVmZmVjdFIHZWZmZWN0cw==');
@$core.Deprecated('Use itemTypeDescriptor instead')
const ItemType$json = const {
  '1': 'ItemType',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 9, '10': 'type'},
  ],
};

/// Descriptor for `ItemType`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemTypeDescriptor = $convert.base64Decode('CghJdGVtVHlwZRISCgR0eXBlGAEgASgJUgR0eXBl');
@$core.Deprecated('Use addItemDescriptor instead')
const AddItem$json = const {
  '1': 'AddItem',
  '2': const [
    const {'1': 'item', '3': 1, '4': 1, '5': 11, '6': '.item.Item', '10': 'item'},
  ],
};

/// Descriptor for `AddItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addItemDescriptor = $convert.base64Decode('CgdBZGRJdGVtEh4KBGl0ZW0YASABKAsyCi5pdGVtLkl0ZW1SBGl0ZW0=');
@$core.Deprecated('Use getItemDescriptor instead')
const GetItem$json = const {
  '1': 'GetItem',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.common.ItemId', '10': 'id'},
  ],
};

/// Descriptor for `GetItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getItemDescriptor = $convert.base64Decode('CgdHZXRJdGVtEh4KAmlkGAEgASgLMg4uY29tbW9uLkl0ZW1JZFICaWQ=');
@$core.Deprecated('Use getInventoryDescriptor instead')
const GetInventory$json = const {
  '1': 'GetInventory',
  '2': const [
    const {'1': 'userId', '3': 1, '4': 1, '5': 11, '6': '.common.UserId', '10': 'userId'},
  ],
};

/// Descriptor for `GetInventory`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getInventoryDescriptor = $convert.base64Decode('CgxHZXRJbnZlbnRvcnkSJgoGdXNlcklkGAEgASgLMg4uY29tbW9uLlVzZXJJZFIGdXNlcklk');
@$core.Deprecated('Use addItemToInventoryCallDescriptor instead')
const AddItemToInventoryCall$json = const {
  '1': 'AddItemToInventoryCall',
  '2': const [
    const {'1': 'userId', '3': 1, '4': 1, '5': 11, '6': '.common.UserId', '10': 'userId'},
    const {'1': 'itemId', '3': 2, '4': 1, '5': 11, '6': '.common.ItemId', '10': 'itemId'},
  ],
};

/// Descriptor for `AddItemToInventoryCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addItemToInventoryCallDescriptor = $convert.base64Decode('ChZBZGRJdGVtVG9JbnZlbnRvcnlDYWxsEiYKBnVzZXJJZBgBIAEoCzIOLmNvbW1vbi5Vc2VySWRSBnVzZXJJZBImCgZpdGVtSWQYAiABKAsyDi5jb21tb24uSXRlbUlkUgZpdGVtSWQ=');
@$core.Deprecated('Use updateItemDescriptor instead')
const UpdateItem$json = const {
  '1': 'UpdateItem',
  '2': const [
    const {'1': 'item', '3': 1, '4': 1, '5': 11, '6': '.item.Item', '10': 'item'},
  ],
};

/// Descriptor for `UpdateItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateItemDescriptor = $convert.base64Decode('CgpVcGRhdGVJdGVtEh4KBGl0ZW0YASABKAsyCi5pdGVtLkl0ZW1SBGl0ZW0=');
@$core.Deprecated('Use deleteItemDescriptor instead')
const DeleteItem$json = const {
  '1': 'DeleteItem',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.common.ItemId', '10': 'id'},
  ],
};

/// Descriptor for `DeleteItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteItemDescriptor = $convert.base64Decode('CgpEZWxldGVJdGVtEh4KAmlkGAEgASgLMg4uY29tbW9uLkl0ZW1JZFICaWQ=');
@$core.Deprecated('Use itemDeletedEventDescriptor instead')
const ItemDeletedEvent$json = const {
  '1': 'ItemDeletedEvent',
  '2': const [
    const {'1': 'itemId', '3': 1, '4': 1, '5': 11, '6': '.common.ItemId', '10': 'itemId'},
  ],
};

/// Descriptor for `ItemDeletedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemDeletedEventDescriptor = $convert.base64Decode('ChBJdGVtRGVsZXRlZEV2ZW50EiYKBml0ZW1JZBgBIAEoCzIOLmNvbW1vbi5JdGVtSWRSBml0ZW1JZA==');
@$core.Deprecated('Use itemAddedEventDescriptor instead')
const ItemAddedEvent$json = const {
  '1': 'ItemAddedEvent',
  '2': const [
    const {'1': 'item', '3': 1, '4': 1, '5': 11, '6': '.item.Item', '10': 'item'},
  ],
};

/// Descriptor for `ItemAddedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemAddedEventDescriptor = $convert.base64Decode('Cg5JdGVtQWRkZWRFdmVudBIeCgRpdGVtGAEgASgLMgouaXRlbS5JdGVtUgRpdGVt');
@$core.Deprecated('Use itemUpdatedEventDescriptor instead')
const ItemUpdatedEvent$json = const {
  '1': 'ItemUpdatedEvent',
  '2': const [
    const {'1': 'item', '3': 1, '4': 1, '5': 11, '6': '.item.Item', '10': 'item'},
  ],
};

/// Descriptor for `ItemUpdatedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemUpdatedEventDescriptor = $convert.base64Decode('ChBJdGVtVXBkYXRlZEV2ZW50Eh4KBGl0ZW0YASABKAsyCi5pdGVtLkl0ZW1SBGl0ZW0=');
@$core.Deprecated('Use itemEventDescriptor instead')
const ItemEvent$json = const {
  '1': 'ItemEvent',
  '2': const [
    const {'1': 'addedEvent', '3': 1, '4': 1, '5': 11, '6': '.item.ItemAddedEvent', '9': 0, '10': 'addedEvent'},
    const {'1': 'deleteEvent', '3': 2, '4': 1, '5': 11, '6': '.item.ItemDeletedEvent', '9': 0, '10': 'deleteEvent'},
    const {'1': 'updateEvent', '3': 3, '4': 1, '5': 11, '6': '.item.ItemUpdatedEvent', '9': 0, '10': 'updateEvent'},
  ],
  '8': const [
    const {'1': 'event'},
  ],
};

/// Descriptor for `ItemEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemEventDescriptor = $convert.base64Decode('CglJdGVtRXZlbnQSNgoKYWRkZWRFdmVudBgBIAEoCzIULml0ZW0uSXRlbUFkZGVkRXZlbnRIAFIKYWRkZWRFdmVudBI6CgtkZWxldGVFdmVudBgCIAEoCzIWLml0ZW0uSXRlbURlbGV0ZWRFdmVudEgAUgtkZWxldGVFdmVudBI6Cgt1cGRhdGVFdmVudBgDIAEoCzIWLml0ZW0uSXRlbVVwZGF0ZWRFdmVudEgAUgt1cGRhdGVFdmVudEIHCgVldmVudA==');
@$core.Deprecated('Use inventoryServiceCallDescriptor instead')
const InventoryServiceCall$json = const {
  '1': 'InventoryServiceCall',
  '2': const [
    const {'1': 'addItem', '3': 1, '4': 1, '5': 11, '6': '.item.AddItem', '9': 0, '10': 'addItem'},
    const {'1': 'getItem', '3': 2, '4': 1, '5': 11, '6': '.item.GetItem', '9': 0, '10': 'getItem'},
    const {'1': 'getInventory', '3': 3, '4': 1, '5': 11, '6': '.item.GetInventory', '9': 0, '10': 'getInventory'},
    const {'1': 'updateItem', '3': 4, '4': 1, '5': 11, '6': '.item.UpdateItem', '9': 0, '10': 'updateItem'},
    const {'1': 'deleteItem', '3': 5, '4': 1, '5': 11, '6': '.item.DeleteItem', '9': 0, '10': 'deleteItem'},
    const {'1': 'metaData', '3': 6, '4': 1, '5': 11, '6': '.common.MetaData', '10': 'metaData'},
  ],
  '8': const [
    const {'1': 'method'},
  ],
};

/// Descriptor for `InventoryServiceCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inventoryServiceCallDescriptor = $convert.base64Decode('ChRJbnZlbnRvcnlTZXJ2aWNlQ2FsbBIpCgdhZGRJdGVtGAEgASgLMg0uaXRlbS5BZGRJdGVtSABSB2FkZEl0ZW0SKQoHZ2V0SXRlbRgCIAEoCzINLml0ZW0uR2V0SXRlbUgAUgdnZXRJdGVtEjgKDGdldEludmVudG9yeRgDIAEoCzISLml0ZW0uR2V0SW52ZW50b3J5SABSDGdldEludmVudG9yeRIyCgp1cGRhdGVJdGVtGAQgASgLMhAuaXRlbS5VcGRhdGVJdGVtSABSCnVwZGF0ZUl0ZW0SMgoKZGVsZXRlSXRlbRgFIAEoCzIQLml0ZW0uRGVsZXRlSXRlbUgAUgpkZWxldGVJdGVtEiwKCG1ldGFEYXRhGAYgASgLMhAuY29tbW9uLk1ldGFEYXRhUghtZXRhRGF0YUIICgZtZXRob2Q=');
@$core.Deprecated('Use inventoryServiceResponseDescriptor instead')
const InventoryServiceResponse$json = const {
  '1': 'InventoryServiceResponse',
  '2': const [
    const {'1': 'itemId', '3': 1, '4': 1, '5': 11, '6': '.common.ItemId', '9': 0, '10': 'itemId'},
    const {'1': 'item', '3': 2, '4': 1, '5': 11, '6': '.item.Item', '9': 0, '10': 'item'},
    const {'1': 'status', '3': 3, '4': 1, '5': 11, '6': '.common.ResponseStatus', '9': 0, '10': 'status'},
    const {'1': 'inv', '3': 4, '4': 1, '5': 11, '6': '.item.Inventory', '9': 0, '10': 'inv'},
    const {'1': 'metaData', '3': 5, '4': 1, '5': 11, '6': '.common.MetaData', '10': 'metaData'},
  ],
  '8': const [
    const {'1': 'data'},
  ],
};

/// Descriptor for `InventoryServiceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List inventoryServiceResponseDescriptor = $convert.base64Decode('ChhJbnZlbnRvcnlTZXJ2aWNlUmVzcG9uc2USKAoGaXRlbUlkGAEgASgLMg4uY29tbW9uLkl0ZW1JZEgAUgZpdGVtSWQSIAoEaXRlbRgCIAEoCzIKLml0ZW0uSXRlbUgAUgRpdGVtEjAKBnN0YXR1cxgDIAEoCzIWLmNvbW1vbi5SZXNwb25zZVN0YXR1c0gAUgZzdGF0dXMSIwoDaW52GAQgASgLMg8uaXRlbS5JbnZlbnRvcnlIAFIDaW52EiwKCG1ldGFEYXRhGAUgASgLMhAuY29tbW9uLk1ldGFEYXRhUghtZXRhRGF0YUIGCgRkYXRh');
