///
//  Generated code. Do not modify.
//  source: reward.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'monster.pb.dart' as $3;
import 'inventory.pb.dart' as $6;
import 'common.pb.dart' as $0;

class Reward extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Reward', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'reward'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', $pb.PbFieldType.O3)
    ..aOM<$3.Activity>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'activity', subBuilder: $3.Activity.create)
    ..aOM<$6.ItemIdList>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemList', protoName: 'itemList', subBuilder: $6.ItemIdList.create)
    ..aOM<$0.UserId>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'owner', subBuilder: $0.UserId.create)
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'collected')
    ..hasRequiredFields = false
  ;

  Reward._() : super();
  factory Reward({
    $core.int id,
    $3.Activity activity,
    $6.ItemIdList itemList,
    $0.UserId owner,
    $core.bool collected,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (activity != null) {
      _result.activity = activity;
    }
    if (itemList != null) {
      _result.itemList = itemList;
    }
    if (owner != null) {
      _result.owner = owner;
    }
    if (collected != null) {
      _result.collected = collected;
    }
    return _result;
  }
  factory Reward.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Reward.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Reward clone() => Reward()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Reward copyWith(void Function(Reward) updates) => super.copyWith((message) => updates(message as Reward)) as Reward; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Reward create() => Reward._();
  Reward createEmptyInstance() => create();
  static $pb.PbList<Reward> createRepeated() => $pb.PbList<Reward>();
  @$core.pragma('dart2js:noInline')
  static Reward getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Reward>(create);
  static Reward _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get id => $_getIZ(0);
  @$pb.TagNumber(1)
  set id($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $3.Activity get activity => $_getN(1);
  @$pb.TagNumber(2)
  set activity($3.Activity v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasActivity() => $_has(1);
  @$pb.TagNumber(2)
  void clearActivity() => clearField(2);
  @$pb.TagNumber(2)
  $3.Activity ensureActivity() => $_ensure(1);

  @$pb.TagNumber(3)
  $6.ItemIdList get itemList => $_getN(2);
  @$pb.TagNumber(3)
  set itemList($6.ItemIdList v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasItemList() => $_has(2);
  @$pb.TagNumber(3)
  void clearItemList() => clearField(3);
  @$pb.TagNumber(3)
  $6.ItemIdList ensureItemList() => $_ensure(2);

  @$pb.TagNumber(4)
  $0.UserId get owner => $_getN(3);
  @$pb.TagNumber(4)
  set owner($0.UserId v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasOwner() => $_has(3);
  @$pb.TagNumber(4)
  void clearOwner() => clearField(4);
  @$pb.TagNumber(4)
  $0.UserId ensureOwner() => $_ensure(3);

  @$pb.TagNumber(5)
  $core.bool get collected => $_getBF(4);
  @$pb.TagNumber(5)
  set collected($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasCollected() => $_has(4);
  @$pb.TagNumber(5)
  void clearCollected() => clearField(5);
}

class RewardList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RewardList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'reward'), createEmptyInstance: create)
    ..pc<Reward>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reward', $pb.PbFieldType.PM, subBuilder: Reward.create)
    ..hasRequiredFields = false
  ;

  RewardList._() : super();
  factory RewardList({
    $core.Iterable<Reward> reward,
  }) {
    final _result = create();
    if (reward != null) {
      _result.reward.addAll(reward);
    }
    return _result;
  }
  factory RewardList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RewardList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RewardList clone() => RewardList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RewardList copyWith(void Function(RewardList) updates) => super.copyWith((message) => updates(message as RewardList)) as RewardList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RewardList create() => RewardList._();
  RewardList createEmptyInstance() => create();
  static $pb.PbList<RewardList> createRepeated() => $pb.PbList<RewardList>();
  @$core.pragma('dart2js:noInline')
  static RewardList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RewardList>(create);
  static RewardList _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Reward> get reward => $_getList(0);
}

