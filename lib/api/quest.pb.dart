///
//  Generated code. Do not modify.
//  source: quest.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'common.pb.dart' as $0;
import 'google/protobuf/timestamp.pb.dart' as $12;

class Quest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Quest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..aOM<$0.QuestId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', subBuilder: $0.QuestId.create)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'difficulty', $pb.PbFieldType.O3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'recommendedStrength', $pb.PbFieldType.O3, protoName: 'recommendedStrength')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'playerID', $pb.PbFieldType.O3, protoName: 'playerID')
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'duration', $pb.PbFieldType.O3)
    ..aOM<ActiveQuest>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'activeQuest', protoName: 'activeQuest', subBuilder: ActiveQuest.create)
    ..hasRequiredFields = false
  ;

  Quest._() : super();
  factory Quest({
    $0.QuestId id,
    $core.int difficulty,
    $core.int recommendedStrength,
    $core.int playerID,
    $core.int duration,
    ActiveQuest activeQuest,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (difficulty != null) {
      _result.difficulty = difficulty;
    }
    if (recommendedStrength != null) {
      _result.recommendedStrength = recommendedStrength;
    }
    if (playerID != null) {
      _result.playerID = playerID;
    }
    if (duration != null) {
      _result.duration = duration;
    }
    if (activeQuest != null) {
      _result.activeQuest = activeQuest;
    }
    return _result;
  }
  factory Quest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Quest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Quest clone() => Quest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Quest copyWith(void Function(Quest) updates) => super.copyWith((message) => updates(message as Quest)) as Quest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Quest create() => Quest._();
  Quest createEmptyInstance() => create();
  static $pb.PbList<Quest> createRepeated() => $pb.PbList<Quest>();
  @$core.pragma('dart2js:noInline')
  static Quest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Quest>(create);
  static Quest _defaultInstance;

  @$pb.TagNumber(1)
  $0.QuestId get id => $_getN(0);
  @$pb.TagNumber(1)
  set id($0.QuestId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
  @$pb.TagNumber(1)
  $0.QuestId ensureId() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.int get difficulty => $_getIZ(1);
  @$pb.TagNumber(2)
  set difficulty($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDifficulty() => $_has(1);
  @$pb.TagNumber(2)
  void clearDifficulty() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get recommendedStrength => $_getIZ(2);
  @$pb.TagNumber(3)
  set recommendedStrength($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRecommendedStrength() => $_has(2);
  @$pb.TagNumber(3)
  void clearRecommendedStrength() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get playerID => $_getIZ(3);
  @$pb.TagNumber(4)
  set playerID($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPlayerID() => $_has(3);
  @$pb.TagNumber(4)
  void clearPlayerID() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get duration => $_getIZ(4);
  @$pb.TagNumber(5)
  set duration($core.int v) { $_setSignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasDuration() => $_has(4);
  @$pb.TagNumber(5)
  void clearDuration() => clearField(5);

  @$pb.TagNumber(6)
  ActiveQuest get activeQuest => $_getN(5);
  @$pb.TagNumber(6)
  set activeQuest(ActiveQuest v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasActiveQuest() => $_has(5);
  @$pb.TagNumber(6)
  void clearActiveQuest() => clearField(6);
  @$pb.TagNumber(6)
  ActiveQuest ensureActiveQuest() => $_ensure(5);
}

class ActiveQuest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ActiveQuest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..aOM<$12.Timestamp>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startTime', subBuilder: $12.Timestamp.create)
    ..pc<$0.MonId>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'monsterIds', $pb.PbFieldType.PM, protoName: 'monsterIds', subBuilder: $0.MonId.create)
    ..hasRequiredFields = false
  ;

  ActiveQuest._() : super();
  factory ActiveQuest({
    $12.Timestamp startTime,
    $core.Iterable<$0.MonId> monsterIds,
  }) {
    final _result = create();
    if (startTime != null) {
      _result.startTime = startTime;
    }
    if (monsterIds != null) {
      _result.monsterIds.addAll(monsterIds);
    }
    return _result;
  }
  factory ActiveQuest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ActiveQuest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ActiveQuest clone() => ActiveQuest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ActiveQuest copyWith(void Function(ActiveQuest) updates) => super.copyWith((message) => updates(message as ActiveQuest)) as ActiveQuest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ActiveQuest create() => ActiveQuest._();
  ActiveQuest createEmptyInstance() => create();
  static $pb.PbList<ActiveQuest> createRepeated() => $pb.PbList<ActiveQuest>();
  @$core.pragma('dart2js:noInline')
  static ActiveQuest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ActiveQuest>(create);
  static ActiveQuest _defaultInstance;

  @$pb.TagNumber(1)
  $12.Timestamp get startTime => $_getN(0);
  @$pb.TagNumber(1)
  set startTime($12.Timestamp v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStartTime() => $_has(0);
  @$pb.TagNumber(1)
  void clearStartTime() => clearField(1);
  @$pb.TagNumber(1)
  $12.Timestamp ensureStartTime() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$0.MonId> get monsterIds => $_getList(1);
}

class StartQuestRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'StartQuestRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..aOM<$0.QuestId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'questId', protoName: 'questId', subBuilder: $0.QuestId.create)
    ..pc<$0.MonId>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'monsterIds', $pb.PbFieldType.PM, protoName: 'monsterIds', subBuilder: $0.MonId.create)
    ..hasRequiredFields = false
  ;

  StartQuestRequest._() : super();
  factory StartQuestRequest({
    $0.QuestId questId,
    $core.Iterable<$0.MonId> monsterIds,
  }) {
    final _result = create();
    if (questId != null) {
      _result.questId = questId;
    }
    if (monsterIds != null) {
      _result.monsterIds.addAll(monsterIds);
    }
    return _result;
  }
  factory StartQuestRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory StartQuestRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  StartQuestRequest clone() => StartQuestRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  StartQuestRequest copyWith(void Function(StartQuestRequest) updates) => super.copyWith((message) => updates(message as StartQuestRequest)) as StartQuestRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StartQuestRequest create() => StartQuestRequest._();
  StartQuestRequest createEmptyInstance() => create();
  static $pb.PbList<StartQuestRequest> createRepeated() => $pb.PbList<StartQuestRequest>();
  @$core.pragma('dart2js:noInline')
  static StartQuestRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<StartQuestRequest>(create);
  static StartQuestRequest _defaultInstance;

  @$pb.TagNumber(1)
  $0.QuestId get questId => $_getN(0);
  @$pb.TagNumber(1)
  set questId($0.QuestId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuestId() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuestId() => clearField(1);
  @$pb.TagNumber(1)
  $0.QuestId ensureQuestId() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$0.MonId> get monsterIds => $_getList(1);
}

class GetQuestsByUser extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetQuestsByUser', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..aOM<$0.UserId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId', protoName: 'userId', subBuilder: $0.UserId.create)
    ..hasRequiredFields = false
  ;

  GetQuestsByUser._() : super();
  factory GetQuestsByUser({
    $0.UserId userId,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    return _result;
  }
  factory GetQuestsByUser.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetQuestsByUser.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetQuestsByUser clone() => GetQuestsByUser()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetQuestsByUser copyWith(void Function(GetQuestsByUser) updates) => super.copyWith((message) => updates(message as GetQuestsByUser)) as GetQuestsByUser; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetQuestsByUser create() => GetQuestsByUser._();
  GetQuestsByUser createEmptyInstance() => create();
  static $pb.PbList<GetQuestsByUser> createRepeated() => $pb.PbList<GetQuestsByUser>();
  @$core.pragma('dart2js:noInline')
  static GetQuestsByUser getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetQuestsByUser>(create);
  static GetQuestsByUser _defaultInstance;

  @$pb.TagNumber(1)
  $0.UserId get userId => $_getN(0);
  @$pb.TagNumber(1)
  set userId($0.UserId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);
  @$pb.TagNumber(1)
  $0.UserId ensureUserId() => $_ensure(0);
}

class GetQuest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetQuest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..aOM<$0.QuestId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'questId', protoName: 'questId', subBuilder: $0.QuestId.create)
    ..hasRequiredFields = false
  ;

  GetQuest._() : super();
  factory GetQuest({
    $0.QuestId questId,
  }) {
    final _result = create();
    if (questId != null) {
      _result.questId = questId;
    }
    return _result;
  }
  factory GetQuest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetQuest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetQuest clone() => GetQuest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetQuest copyWith(void Function(GetQuest) updates) => super.copyWith((message) => updates(message as GetQuest)) as GetQuest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetQuest create() => GetQuest._();
  GetQuest createEmptyInstance() => create();
  static $pb.PbList<GetQuest> createRepeated() => $pb.PbList<GetQuest>();
  @$core.pragma('dart2js:noInline')
  static GetQuest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetQuest>(create);
  static GetQuest _defaultInstance;

  @$pb.TagNumber(1)
  $0.QuestId get questId => $_getN(0);
  @$pb.TagNumber(1)
  set questId($0.QuestId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuestId() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuestId() => clearField(1);
  @$pb.TagNumber(1)
  $0.QuestId ensureQuestId() => $_ensure(0);
}

class StartQuestResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'StartQuestResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..aOM<$0.ResponseStatus>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', subBuilder: $0.ResponseStatus.create)
    ..hasRequiredFields = false
  ;

  StartQuestResponse._() : super();
  factory StartQuestResponse({
    $0.ResponseStatus status,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory StartQuestResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory StartQuestResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  StartQuestResponse clone() => StartQuestResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  StartQuestResponse copyWith(void Function(StartQuestResponse) updates) => super.copyWith((message) => updates(message as StartQuestResponse)) as StartQuestResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StartQuestResponse create() => StartQuestResponse._();
  StartQuestResponse createEmptyInstance() => create();
  static $pb.PbList<StartQuestResponse> createRepeated() => $pb.PbList<StartQuestResponse>();
  @$core.pragma('dart2js:noInline')
  static StartQuestResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<StartQuestResponse>(create);
  static StartQuestResponse _defaultInstance;

  @$pb.TagNumber(1)
  $0.ResponseStatus get status => $_getN(0);
  @$pb.TagNumber(1)
  set status($0.ResponseStatus v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);
  @$pb.TagNumber(1)
  $0.ResponseStatus ensureStatus() => $_ensure(0);
}

class GetQuestResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetQuestResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..aOM<Quest>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quest', subBuilder: Quest.create)
    ..hasRequiredFields = false
  ;

  GetQuestResponse._() : super();
  factory GetQuestResponse({
    Quest quest,
  }) {
    final _result = create();
    if (quest != null) {
      _result.quest = quest;
    }
    return _result;
  }
  factory GetQuestResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetQuestResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetQuestResponse clone() => GetQuestResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetQuestResponse copyWith(void Function(GetQuestResponse) updates) => super.copyWith((message) => updates(message as GetQuestResponse)) as GetQuestResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetQuestResponse create() => GetQuestResponse._();
  GetQuestResponse createEmptyInstance() => create();
  static $pb.PbList<GetQuestResponse> createRepeated() => $pb.PbList<GetQuestResponse>();
  @$core.pragma('dart2js:noInline')
  static GetQuestResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetQuestResponse>(create);
  static GetQuestResponse _defaultInstance;

  @$pb.TagNumber(1)
  Quest get quest => $_getN(0);
  @$pb.TagNumber(1)
  set quest(Quest v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuest() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuest() => clearField(1);
  @$pb.TagNumber(1)
  Quest ensureQuest() => $_ensure(0);
}

class GetQuestByUserResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetQuestByUserResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..pc<Quest>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quests', $pb.PbFieldType.PM, subBuilder: Quest.create)
    ..hasRequiredFields = false
  ;

  GetQuestByUserResponse._() : super();
  factory GetQuestByUserResponse({
    $core.Iterable<Quest> quests,
  }) {
    final _result = create();
    if (quests != null) {
      _result.quests.addAll(quests);
    }
    return _result;
  }
  factory GetQuestByUserResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetQuestByUserResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetQuestByUserResponse clone() => GetQuestByUserResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetQuestByUserResponse copyWith(void Function(GetQuestByUserResponse) updates) => super.copyWith((message) => updates(message as GetQuestByUserResponse)) as GetQuestByUserResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetQuestByUserResponse create() => GetQuestByUserResponse._();
  GetQuestByUserResponse createEmptyInstance() => create();
  static $pb.PbList<GetQuestByUserResponse> createRepeated() => $pb.PbList<GetQuestByUserResponse>();
  @$core.pragma('dart2js:noInline')
  static GetQuestByUserResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetQuestByUserResponse>(create);
  static GetQuestByUserResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Quest> get quests => $_getList(0);
}

class QuestList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QuestList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..pc<Quest>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quests', $pb.PbFieldType.PM, subBuilder: Quest.create)
    ..hasRequiredFields = false
  ;

  QuestList._() : super();
  factory QuestList({
    $core.Iterable<Quest> quests,
  }) {
    final _result = create();
    if (quests != null) {
      _result.quests.addAll(quests);
    }
    return _result;
  }
  factory QuestList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QuestList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QuestList clone() => QuestList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QuestList copyWith(void Function(QuestList) updates) => super.copyWith((message) => updates(message as QuestList)) as QuestList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QuestList create() => QuestList._();
  QuestList createEmptyInstance() => create();
  static $pb.PbList<QuestList> createRepeated() => $pb.PbList<QuestList>();
  @$core.pragma('dart2js:noInline')
  static QuestList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QuestList>(create);
  static QuestList _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Quest> get quests => $_getList(0);
}

class QuestCompletedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QuestCompletedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..aOM<$0.QuestId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'questId', protoName: 'questId', subBuilder: $0.QuestId.create)
    ..hasRequiredFields = false
  ;

  QuestCompletedEvent._() : super();
  factory QuestCompletedEvent({
    $0.QuestId questId,
  }) {
    final _result = create();
    if (questId != null) {
      _result.questId = questId;
    }
    return _result;
  }
  factory QuestCompletedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QuestCompletedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QuestCompletedEvent clone() => QuestCompletedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QuestCompletedEvent copyWith(void Function(QuestCompletedEvent) updates) => super.copyWith((message) => updates(message as QuestCompletedEvent)) as QuestCompletedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QuestCompletedEvent create() => QuestCompletedEvent._();
  QuestCompletedEvent createEmptyInstance() => create();
  static $pb.PbList<QuestCompletedEvent> createRepeated() => $pb.PbList<QuestCompletedEvent>();
  @$core.pragma('dart2js:noInline')
  static QuestCompletedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QuestCompletedEvent>(create);
  static QuestCompletedEvent _defaultInstance;

  @$pb.TagNumber(1)
  $0.QuestId get questId => $_getN(0);
  @$pb.TagNumber(1)
  set questId($0.QuestId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuestId() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuestId() => clearField(1);
  @$pb.TagNumber(1)
  $0.QuestId ensureQuestId() => $_ensure(0);
}

class QuestUpdatedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QuestUpdatedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..aOM<Quest>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quest', subBuilder: Quest.create)
    ..hasRequiredFields = false
  ;

  QuestUpdatedEvent._() : super();
  factory QuestUpdatedEvent({
    Quest quest,
  }) {
    final _result = create();
    if (quest != null) {
      _result.quest = quest;
    }
    return _result;
  }
  factory QuestUpdatedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QuestUpdatedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QuestUpdatedEvent clone() => QuestUpdatedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QuestUpdatedEvent copyWith(void Function(QuestUpdatedEvent) updates) => super.copyWith((message) => updates(message as QuestUpdatedEvent)) as QuestUpdatedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QuestUpdatedEvent create() => QuestUpdatedEvent._();
  QuestUpdatedEvent createEmptyInstance() => create();
  static $pb.PbList<QuestUpdatedEvent> createRepeated() => $pb.PbList<QuestUpdatedEvent>();
  @$core.pragma('dart2js:noInline')
  static QuestUpdatedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QuestUpdatedEvent>(create);
  static QuestUpdatedEvent _defaultInstance;

  @$pb.TagNumber(1)
  Quest get quest => $_getN(0);
  @$pb.TagNumber(1)
  set quest(Quest v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuest() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuest() => clearField(1);
  @$pb.TagNumber(1)
  Quest ensureQuest() => $_ensure(0);
}

class QuestDeletedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QuestDeletedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..aOM<$0.QuestId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'questId', protoName: 'questId', subBuilder: $0.QuestId.create)
    ..hasRequiredFields = false
  ;

  QuestDeletedEvent._() : super();
  factory QuestDeletedEvent({
    $0.QuestId questId,
  }) {
    final _result = create();
    if (questId != null) {
      _result.questId = questId;
    }
    return _result;
  }
  factory QuestDeletedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QuestDeletedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QuestDeletedEvent clone() => QuestDeletedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QuestDeletedEvent copyWith(void Function(QuestDeletedEvent) updates) => super.copyWith((message) => updates(message as QuestDeletedEvent)) as QuestDeletedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QuestDeletedEvent create() => QuestDeletedEvent._();
  QuestDeletedEvent createEmptyInstance() => create();
  static $pb.PbList<QuestDeletedEvent> createRepeated() => $pb.PbList<QuestDeletedEvent>();
  @$core.pragma('dart2js:noInline')
  static QuestDeletedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QuestDeletedEvent>(create);
  static QuestDeletedEvent _defaultInstance;

  @$pb.TagNumber(1)
  $0.QuestId get questId => $_getN(0);
  @$pb.TagNumber(1)
  set questId($0.QuestId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuestId() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuestId() => clearField(1);
  @$pb.TagNumber(1)
  $0.QuestId ensureQuestId() => $_ensure(0);
}

class QuestStartedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QuestStartedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..aOM<Quest>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quest', subBuilder: Quest.create)
    ..hasRequiredFields = false
  ;

  QuestStartedEvent._() : super();
  factory QuestStartedEvent({
    Quest quest,
  }) {
    final _result = create();
    if (quest != null) {
      _result.quest = quest;
    }
    return _result;
  }
  factory QuestStartedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QuestStartedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QuestStartedEvent clone() => QuestStartedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QuestStartedEvent copyWith(void Function(QuestStartedEvent) updates) => super.copyWith((message) => updates(message as QuestStartedEvent)) as QuestStartedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QuestStartedEvent create() => QuestStartedEvent._();
  QuestStartedEvent createEmptyInstance() => create();
  static $pb.PbList<QuestStartedEvent> createRepeated() => $pb.PbList<QuestStartedEvent>();
  @$core.pragma('dart2js:noInline')
  static QuestStartedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QuestStartedEvent>(create);
  static QuestStartedEvent _defaultInstance;

  @$pb.TagNumber(1)
  Quest get quest => $_getN(0);
  @$pb.TagNumber(1)
  set quest(Quest v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuest() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuest() => clearField(1);
  @$pb.TagNumber(1)
  Quest ensureQuest() => $_ensure(0);
}

enum QuestEvent_Event {
  completedEvent, 
  updatedEvent, 
  deletedEvent, 
  startedEvent, 
  notSet
}

class QuestEvent extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, QuestEvent_Event> _QuestEvent_EventByTag = {
    1 : QuestEvent_Event.completedEvent,
    2 : QuestEvent_Event.updatedEvent,
    3 : QuestEvent_Event.deletedEvent,
    4 : QuestEvent_Event.startedEvent,
    0 : QuestEvent_Event.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QuestEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4])
    ..aOM<QuestCompletedEvent>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'completedEvent', protoName: 'completedEvent', subBuilder: QuestCompletedEvent.create)
    ..aOM<QuestUpdatedEvent>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updatedEvent', protoName: 'updatedEvent', subBuilder: QuestUpdatedEvent.create)
    ..aOM<QuestDeletedEvent>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deletedEvent', protoName: 'deletedEvent', subBuilder: QuestDeletedEvent.create)
    ..aOM<QuestStartedEvent>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startedEvent', protoName: 'startedEvent', subBuilder: QuestStartedEvent.create)
    ..hasRequiredFields = false
  ;

  QuestEvent._() : super();
  factory QuestEvent({
    QuestCompletedEvent completedEvent,
    QuestUpdatedEvent updatedEvent,
    QuestDeletedEvent deletedEvent,
    QuestStartedEvent startedEvent,
  }) {
    final _result = create();
    if (completedEvent != null) {
      _result.completedEvent = completedEvent;
    }
    if (updatedEvent != null) {
      _result.updatedEvent = updatedEvent;
    }
    if (deletedEvent != null) {
      _result.deletedEvent = deletedEvent;
    }
    if (startedEvent != null) {
      _result.startedEvent = startedEvent;
    }
    return _result;
  }
  factory QuestEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QuestEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QuestEvent clone() => QuestEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QuestEvent copyWith(void Function(QuestEvent) updates) => super.copyWith((message) => updates(message as QuestEvent)) as QuestEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QuestEvent create() => QuestEvent._();
  QuestEvent createEmptyInstance() => create();
  static $pb.PbList<QuestEvent> createRepeated() => $pb.PbList<QuestEvent>();
  @$core.pragma('dart2js:noInline')
  static QuestEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QuestEvent>(create);
  static QuestEvent _defaultInstance;

  QuestEvent_Event whichEvent() => _QuestEvent_EventByTag[$_whichOneof(0)];
  void clearEvent() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  QuestCompletedEvent get completedEvent => $_getN(0);
  @$pb.TagNumber(1)
  set completedEvent(QuestCompletedEvent v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasCompletedEvent() => $_has(0);
  @$pb.TagNumber(1)
  void clearCompletedEvent() => clearField(1);
  @$pb.TagNumber(1)
  QuestCompletedEvent ensureCompletedEvent() => $_ensure(0);

  @$pb.TagNumber(2)
  QuestUpdatedEvent get updatedEvent => $_getN(1);
  @$pb.TagNumber(2)
  set updatedEvent(QuestUpdatedEvent v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasUpdatedEvent() => $_has(1);
  @$pb.TagNumber(2)
  void clearUpdatedEvent() => clearField(2);
  @$pb.TagNumber(2)
  QuestUpdatedEvent ensureUpdatedEvent() => $_ensure(1);

  @$pb.TagNumber(3)
  QuestDeletedEvent get deletedEvent => $_getN(2);
  @$pb.TagNumber(3)
  set deletedEvent(QuestDeletedEvent v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasDeletedEvent() => $_has(2);
  @$pb.TagNumber(3)
  void clearDeletedEvent() => clearField(3);
  @$pb.TagNumber(3)
  QuestDeletedEvent ensureDeletedEvent() => $_ensure(2);

  @$pb.TagNumber(4)
  QuestStartedEvent get startedEvent => $_getN(3);
  @$pb.TagNumber(4)
  set startedEvent(QuestStartedEvent v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasStartedEvent() => $_has(3);
  @$pb.TagNumber(4)
  void clearStartedEvent() => clearField(4);
  @$pb.TagNumber(4)
  QuestStartedEvent ensureStartedEvent() => $_ensure(3);
}

enum QuestServiceCall_Method {
  startQuest, 
  getQuest, 
  questQuestByUser, 
  notSet
}

class QuestServiceCall extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, QuestServiceCall_Method> _QuestServiceCall_MethodByTag = {
    1 : QuestServiceCall_Method.startQuest,
    2 : QuestServiceCall_Method.getQuest,
    3 : QuestServiceCall_Method.questQuestByUser,
    0 : QuestServiceCall_Method.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QuestServiceCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3])
    ..aOM<StartQuestRequest>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startQuest', protoName: 'startQuest', subBuilder: StartQuestRequest.create)
    ..aOM<GetQuest>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'getQuest', protoName: 'getQuest', subBuilder: GetQuest.create)
    ..aOM<GetQuestsByUser>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'questQuestByUser', protoName: 'questQuestByUser', subBuilder: GetQuestsByUser.create)
    ..aOM<$0.MetaData>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'metaData', protoName: 'metaData', subBuilder: $0.MetaData.create)
    ..hasRequiredFields = false
  ;

  QuestServiceCall._() : super();
  factory QuestServiceCall({
    StartQuestRequest startQuest,
    GetQuest getQuest,
    GetQuestsByUser questQuestByUser,
    $0.MetaData metaData,
  }) {
    final _result = create();
    if (startQuest != null) {
      _result.startQuest = startQuest;
    }
    if (getQuest != null) {
      _result.getQuest = getQuest;
    }
    if (questQuestByUser != null) {
      _result.questQuestByUser = questQuestByUser;
    }
    if (metaData != null) {
      _result.metaData = metaData;
    }
    return _result;
  }
  factory QuestServiceCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QuestServiceCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QuestServiceCall clone() => QuestServiceCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QuestServiceCall copyWith(void Function(QuestServiceCall) updates) => super.copyWith((message) => updates(message as QuestServiceCall)) as QuestServiceCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QuestServiceCall create() => QuestServiceCall._();
  QuestServiceCall createEmptyInstance() => create();
  static $pb.PbList<QuestServiceCall> createRepeated() => $pb.PbList<QuestServiceCall>();
  @$core.pragma('dart2js:noInline')
  static QuestServiceCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QuestServiceCall>(create);
  static QuestServiceCall _defaultInstance;

  QuestServiceCall_Method whichMethod() => _QuestServiceCall_MethodByTag[$_whichOneof(0)];
  void clearMethod() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  StartQuestRequest get startQuest => $_getN(0);
  @$pb.TagNumber(1)
  set startQuest(StartQuestRequest v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStartQuest() => $_has(0);
  @$pb.TagNumber(1)
  void clearStartQuest() => clearField(1);
  @$pb.TagNumber(1)
  StartQuestRequest ensureStartQuest() => $_ensure(0);

  @$pb.TagNumber(2)
  GetQuest get getQuest => $_getN(1);
  @$pb.TagNumber(2)
  set getQuest(GetQuest v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasGetQuest() => $_has(1);
  @$pb.TagNumber(2)
  void clearGetQuest() => clearField(2);
  @$pb.TagNumber(2)
  GetQuest ensureGetQuest() => $_ensure(1);

  @$pb.TagNumber(3)
  GetQuestsByUser get questQuestByUser => $_getN(2);
  @$pb.TagNumber(3)
  set questQuestByUser(GetQuestsByUser v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasQuestQuestByUser() => $_has(2);
  @$pb.TagNumber(3)
  void clearQuestQuestByUser() => clearField(3);
  @$pb.TagNumber(3)
  GetQuestsByUser ensureQuestQuestByUser() => $_ensure(2);

  @$pb.TagNumber(4)
  $0.MetaData get metaData => $_getN(3);
  @$pb.TagNumber(4)
  set metaData($0.MetaData v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasMetaData() => $_has(3);
  @$pb.TagNumber(4)
  void clearMetaData() => clearField(4);
  @$pb.TagNumber(4)
  $0.MetaData ensureMetaData() => $_ensure(3);
}

enum QuestServiceResponse_Data {
  startQuestResponse, 
  getQuestResponse, 
  getQuestByUserResponse, 
  notSet
}

class QuestServiceResponse extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, QuestServiceResponse_Data> _QuestServiceResponse_DataByTag = {
    1 : QuestServiceResponse_Data.startQuestResponse,
    2 : QuestServiceResponse_Data.getQuestResponse,
    3 : QuestServiceResponse_Data.getQuestByUserResponse,
    0 : QuestServiceResponse_Data.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QuestServiceResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'quest'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3])
    ..aOM<StartQuestResponse>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startQuestResponse', protoName: 'startQuestResponse', subBuilder: StartQuestResponse.create)
    ..aOM<GetQuestResponse>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'getQuestResponse', protoName: 'getQuestResponse', subBuilder: GetQuestResponse.create)
    ..aOM<GetQuestByUserResponse>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'getQuestByUserResponse', protoName: 'getQuestByUserResponse', subBuilder: GetQuestByUserResponse.create)
    ..aOM<$0.MetaData>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'metaData', protoName: 'metaData', subBuilder: $0.MetaData.create)
    ..hasRequiredFields = false
  ;

  QuestServiceResponse._() : super();
  factory QuestServiceResponse({
    StartQuestResponse startQuestResponse,
    GetQuestResponse getQuestResponse,
    GetQuestByUserResponse getQuestByUserResponse,
    $0.MetaData metaData,
  }) {
    final _result = create();
    if (startQuestResponse != null) {
      _result.startQuestResponse = startQuestResponse;
    }
    if (getQuestResponse != null) {
      _result.getQuestResponse = getQuestResponse;
    }
    if (getQuestByUserResponse != null) {
      _result.getQuestByUserResponse = getQuestByUserResponse;
    }
    if (metaData != null) {
      _result.metaData = metaData;
    }
    return _result;
  }
  factory QuestServiceResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QuestServiceResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QuestServiceResponse clone() => QuestServiceResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QuestServiceResponse copyWith(void Function(QuestServiceResponse) updates) => super.copyWith((message) => updates(message as QuestServiceResponse)) as QuestServiceResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QuestServiceResponse create() => QuestServiceResponse._();
  QuestServiceResponse createEmptyInstance() => create();
  static $pb.PbList<QuestServiceResponse> createRepeated() => $pb.PbList<QuestServiceResponse>();
  @$core.pragma('dart2js:noInline')
  static QuestServiceResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QuestServiceResponse>(create);
  static QuestServiceResponse _defaultInstance;

  QuestServiceResponse_Data whichData() => _QuestServiceResponse_DataByTag[$_whichOneof(0)];
  void clearData() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  StartQuestResponse get startQuestResponse => $_getN(0);
  @$pb.TagNumber(1)
  set startQuestResponse(StartQuestResponse v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStartQuestResponse() => $_has(0);
  @$pb.TagNumber(1)
  void clearStartQuestResponse() => clearField(1);
  @$pb.TagNumber(1)
  StartQuestResponse ensureStartQuestResponse() => $_ensure(0);

  @$pb.TagNumber(2)
  GetQuestResponse get getQuestResponse => $_getN(1);
  @$pb.TagNumber(2)
  set getQuestResponse(GetQuestResponse v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasGetQuestResponse() => $_has(1);
  @$pb.TagNumber(2)
  void clearGetQuestResponse() => clearField(2);
  @$pb.TagNumber(2)
  GetQuestResponse ensureGetQuestResponse() => $_ensure(1);

  @$pb.TagNumber(3)
  GetQuestByUserResponse get getQuestByUserResponse => $_getN(2);
  @$pb.TagNumber(3)
  set getQuestByUserResponse(GetQuestByUserResponse v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasGetQuestByUserResponse() => $_has(2);
  @$pb.TagNumber(3)
  void clearGetQuestByUserResponse() => clearField(3);
  @$pb.TagNumber(3)
  GetQuestByUserResponse ensureGetQuestByUserResponse() => $_ensure(2);

  @$pb.TagNumber(4)
  $0.MetaData get metaData => $_getN(3);
  @$pb.TagNumber(4)
  set metaData($0.MetaData v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasMetaData() => $_has(3);
  @$pb.TagNumber(4)
  void clearMetaData() => clearField(4);
  @$pb.TagNumber(4)
  $0.MetaData ensureMetaData() => $_ensure(3);
}

