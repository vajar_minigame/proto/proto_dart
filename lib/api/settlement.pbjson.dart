///
//  Generated code. Do not modify.
//  source: settlement.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use settlementDescriptor instead')
const Settlement$json = const {
  '1': 'Settlement',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'id', '3': 2, '4': 1, '5': 3, '10': 'id'},
    const {'1': 'playerID', '3': 3, '4': 1, '5': 3, '10': 'playerID'},
    const {'1': 'pos', '3': 4, '4': 1, '5': 11, '6': '.settlement.Position', '10': 'pos'},
    const {'1': 'depot', '3': 5, '4': 1, '5': 11, '6': '.settlement.Depot', '10': 'depot'},
    const {'1': 'buildings', '3': 6, '4': 3, '5': 11, '6': '.settlement.Building', '10': 'buildings'},
    const {'1': 'buildQueue', '3': 7, '4': 3, '5': 11, '6': '.settlement.BuildQueueItem', '10': 'buildQueue'},
    const {'1': 'buildQueueSize', '3': 8, '4': 1, '5': 3, '10': 'buildQueueSize'},
    const {'1': 'last_update', '3': 9, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'lastUpdate'},
  ],
};

/// Descriptor for `Settlement`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List settlementDescriptor = $convert.base64Decode('CgpTZXR0bGVtZW50EhIKBG5hbWUYASABKAlSBG5hbWUSDgoCaWQYAiABKANSAmlkEhoKCHBsYXllcklEGAMgASgDUghwbGF5ZXJJRBImCgNwb3MYBCABKAsyFC5zZXR0bGVtZW50LlBvc2l0aW9uUgNwb3MSJwoFZGVwb3QYBSABKAsyES5zZXR0bGVtZW50LkRlcG90UgVkZXBvdBIyCglidWlsZGluZ3MYBiADKAsyFC5zZXR0bGVtZW50LkJ1aWxkaW5nUglidWlsZGluZ3MSOgoKYnVpbGRRdWV1ZRgHIAMoCzIaLnNldHRsZW1lbnQuQnVpbGRRdWV1ZUl0ZW1SCmJ1aWxkUXVldWUSJgoOYnVpbGRRdWV1ZVNpemUYCCABKANSDmJ1aWxkUXVldWVTaXplEjsKC2xhc3RfdXBkYXRlGAkgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIKbGFzdFVwZGF0ZQ==');
@$core.Deprecated('Use buildQueueItemDescriptor instead')
const BuildQueueItem$json = const {
  '1': 'BuildQueueItem',
  '2': const [
    const {'1': 'buildingId', '3': 1, '4': 1, '5': 3, '10': 'buildingId'},
    const {'1': 'build_start', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'buildStart'},
    const {'1': 'build_end', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'buildEnd'},
  ],
};

/// Descriptor for `BuildQueueItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List buildQueueItemDescriptor = $convert.base64Decode('Cg5CdWlsZFF1ZXVlSXRlbRIeCgpidWlsZGluZ0lkGAEgASgDUgpidWlsZGluZ0lkEjsKC2J1aWxkX3N0YXJ0GAIgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIKYnVpbGRTdGFydBI3CglidWlsZF9lbmQYAyABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUghidWlsZEVuZA==');
@$core.Deprecated('Use positionDescriptor instead')
const Position$json = const {
  '1': 'Position',
  '2': const [
    const {'1': 'x', '3': 1, '4': 1, '5': 3, '10': 'x'},
    const {'1': 'y', '3': 2, '4': 1, '5': 3, '10': 'y'},
  ],
};

/// Descriptor for `Position`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List positionDescriptor = $convert.base64Decode('CghQb3NpdGlvbhIMCgF4GAEgASgDUgF4EgwKAXkYAiABKANSAXk=');
@$core.Deprecated('Use depotDescriptor instead')
const Depot$json = const {
  '1': 'Depot',
  '2': const [
    const {'1': 'Store', '3': 1, '4': 3, '5': 11, '6': '.settlement.ResourceStorage', '10': 'Store'},
  ],
};

/// Descriptor for `Depot`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List depotDescriptor = $convert.base64Decode('CgVEZXBvdBIxCgVTdG9yZRgBIAMoCzIbLnNldHRsZW1lbnQuUmVzb3VyY2VTdG9yYWdlUgVTdG9yZQ==');
@$core.Deprecated('Use resourceStorageDescriptor instead')
const ResourceStorage$json = const {
  '1': 'ResourceStorage',
  '2': const [
    const {'1': 'capacity', '3': 1, '4': 1, '5': 3, '10': 'capacity'},
    const {'1': 'currentStored', '3': 2, '4': 1, '5': 3, '10': 'currentStored'},
  ],
};

/// Descriptor for `ResourceStorage`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List resourceStorageDescriptor = $convert.base64Decode('Cg9SZXNvdXJjZVN0b3JhZ2USGgoIY2FwYWNpdHkYASABKANSCGNhcGFjaXR5EiQKDWN1cnJlbnRTdG9yZWQYAiABKANSDWN1cnJlbnRTdG9yZWQ=');
@$core.Deprecated('Use settlementListDescriptor instead')
const SettlementList$json = const {
  '1': 'SettlementList',
  '2': const [
    const {'1': 'settlements', '3': 1, '4': 3, '5': 11, '6': '.settlement.Settlement', '10': 'settlements'},
  ],
};

/// Descriptor for `SettlementList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List settlementListDescriptor = $convert.base64Decode('Cg5TZXR0bGVtZW50TGlzdBI4CgtzZXR0bGVtZW50cxgBIAMoCzIWLnNldHRsZW1lbnQuU2V0dGxlbWVudFILc2V0dGxlbWVudHM=');
@$core.Deprecated('Use buildingDescriptor instead')
const Building$json = const {
  '1': 'Building',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 3, '10': 'id'},
    const {'1': 'buildDef', '3': 3, '4': 1, '5': 9, '10': 'buildDef'},
    const {'1': 'inConstruction', '3': 4, '4': 1, '5': 8, '10': 'inConstruction'},
    const {'1': 'hibernated', '3': 5, '4': 1, '5': 8, '10': 'hibernated'},
  ],
};

/// Descriptor for `Building`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List buildingDescriptor = $convert.base64Decode('CghCdWlsZGluZxIOCgJpZBgBIAEoA1ICaWQSGgoIYnVpbGREZWYYAyABKAlSCGJ1aWxkRGVmEiYKDmluQ29uc3RydWN0aW9uGAQgASgIUg5pbkNvbnN0cnVjdGlvbhIeCgpoaWJlcm5hdGVkGAUgASgIUgpoaWJlcm5hdGVk');
@$core.Deprecated('Use buildBuildingCallDescriptor instead')
const BuildBuildingCall$json = const {
  '1': 'BuildBuildingCall',
  '2': const [
    const {'1': 'settlementId', '3': 1, '4': 1, '5': 11, '6': '.common.SettlementId', '10': 'settlementId'},
    const {'1': 'buildDef', '3': 2, '4': 1, '5': 9, '10': 'buildDef'},
  ],
};

/// Descriptor for `BuildBuildingCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List buildBuildingCallDescriptor = $convert.base64Decode('ChFCdWlsZEJ1aWxkaW5nQ2FsbBI4CgxzZXR0bGVtZW50SWQYASABKAsyFC5jb21tb24uU2V0dGxlbWVudElkUgxzZXR0bGVtZW50SWQSGgoIYnVpbGREZWYYAiABKAlSCGJ1aWxkRGVm');
@$core.Deprecated('Use upgradeBuildingCallDescriptor instead')
const UpgradeBuildingCall$json = const {
  '1': 'UpgradeBuildingCall',
  '2': const [
    const {'1': 'settlementId', '3': 1, '4': 1, '5': 11, '6': '.common.SettlementId', '10': 'settlementId'},
    const {'1': 'buildingId', '3': 2, '4': 1, '5': 11, '6': '.common.BuildingId', '10': 'buildingId'},
  ],
};

/// Descriptor for `UpgradeBuildingCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List upgradeBuildingCallDescriptor = $convert.base64Decode('ChNVcGdyYWRlQnVpbGRpbmdDYWxsEjgKDHNldHRsZW1lbnRJZBgBIAEoCzIULmNvbW1vbi5TZXR0bGVtZW50SWRSDHNldHRsZW1lbnRJZBIyCgpidWlsZGluZ0lkGAIgASgLMhIuY29tbW9uLkJ1aWxkaW5nSWRSCmJ1aWxkaW5nSWQ=');
