///
//  Generated code. Do not modify.
//  source: settlement.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'google/protobuf/timestamp.pb.dart' as $12;
import 'common.pb.dart' as $0;

class Settlement extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Settlement', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'settlement'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'playerID', protoName: 'playerID')
    ..aOM<Position>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pos', subBuilder: Position.create)
    ..aOM<Depot>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'depot', subBuilder: Depot.create)
    ..pc<Building>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buildings', $pb.PbFieldType.PM, subBuilder: Building.create)
    ..pc<BuildQueueItem>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buildQueue', $pb.PbFieldType.PM, protoName: 'buildQueue', subBuilder: BuildQueueItem.create)
    ..aInt64(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buildQueueSize', protoName: 'buildQueueSize')
    ..aOM<$12.Timestamp>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastUpdate', subBuilder: $12.Timestamp.create)
    ..hasRequiredFields = false
  ;

  Settlement._() : super();
  factory Settlement({
    $core.String name,
    $fixnum.Int64 id,
    $fixnum.Int64 playerID,
    Position pos,
    Depot depot,
    $core.Iterable<Building> buildings,
    $core.Iterable<BuildQueueItem> buildQueue,
    $fixnum.Int64 buildQueueSize,
    $12.Timestamp lastUpdate,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (id != null) {
      _result.id = id;
    }
    if (playerID != null) {
      _result.playerID = playerID;
    }
    if (pos != null) {
      _result.pos = pos;
    }
    if (depot != null) {
      _result.depot = depot;
    }
    if (buildings != null) {
      _result.buildings.addAll(buildings);
    }
    if (buildQueue != null) {
      _result.buildQueue.addAll(buildQueue);
    }
    if (buildQueueSize != null) {
      _result.buildQueueSize = buildQueueSize;
    }
    if (lastUpdate != null) {
      _result.lastUpdate = lastUpdate;
    }
    return _result;
  }
  factory Settlement.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Settlement.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Settlement clone() => Settlement()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Settlement copyWith(void Function(Settlement) updates) => super.copyWith((message) => updates(message as Settlement)) as Settlement; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Settlement create() => Settlement._();
  Settlement createEmptyInstance() => create();
  static $pb.PbList<Settlement> createRepeated() => $pb.PbList<Settlement>();
  @$core.pragma('dart2js:noInline')
  static Settlement getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Settlement>(create);
  static Settlement _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get id => $_getI64(1);
  @$pb.TagNumber(2)
  set id($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasId() => $_has(1);
  @$pb.TagNumber(2)
  void clearId() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get playerID => $_getI64(2);
  @$pb.TagNumber(3)
  set playerID($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPlayerID() => $_has(2);
  @$pb.TagNumber(3)
  void clearPlayerID() => clearField(3);

  @$pb.TagNumber(4)
  Position get pos => $_getN(3);
  @$pb.TagNumber(4)
  set pos(Position v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasPos() => $_has(3);
  @$pb.TagNumber(4)
  void clearPos() => clearField(4);
  @$pb.TagNumber(4)
  Position ensurePos() => $_ensure(3);

  @$pb.TagNumber(5)
  Depot get depot => $_getN(4);
  @$pb.TagNumber(5)
  set depot(Depot v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasDepot() => $_has(4);
  @$pb.TagNumber(5)
  void clearDepot() => clearField(5);
  @$pb.TagNumber(5)
  Depot ensureDepot() => $_ensure(4);

  @$pb.TagNumber(6)
  $core.List<Building> get buildings => $_getList(5);

  @$pb.TagNumber(7)
  $core.List<BuildQueueItem> get buildQueue => $_getList(6);

  @$pb.TagNumber(8)
  $fixnum.Int64 get buildQueueSize => $_getI64(7);
  @$pb.TagNumber(8)
  set buildQueueSize($fixnum.Int64 v) { $_setInt64(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasBuildQueueSize() => $_has(7);
  @$pb.TagNumber(8)
  void clearBuildQueueSize() => clearField(8);

  @$pb.TagNumber(9)
  $12.Timestamp get lastUpdate => $_getN(8);
  @$pb.TagNumber(9)
  set lastUpdate($12.Timestamp v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasLastUpdate() => $_has(8);
  @$pb.TagNumber(9)
  void clearLastUpdate() => clearField(9);
  @$pb.TagNumber(9)
  $12.Timestamp ensureLastUpdate() => $_ensure(8);
}

class BuildQueueItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BuildQueueItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'settlement'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buildingId', protoName: 'buildingId')
    ..aOM<$12.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buildStart', subBuilder: $12.Timestamp.create)
    ..aOM<$12.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buildEnd', subBuilder: $12.Timestamp.create)
    ..hasRequiredFields = false
  ;

  BuildQueueItem._() : super();
  factory BuildQueueItem({
    $fixnum.Int64 buildingId,
    $12.Timestamp buildStart,
    $12.Timestamp buildEnd,
  }) {
    final _result = create();
    if (buildingId != null) {
      _result.buildingId = buildingId;
    }
    if (buildStart != null) {
      _result.buildStart = buildStart;
    }
    if (buildEnd != null) {
      _result.buildEnd = buildEnd;
    }
    return _result;
  }
  factory BuildQueueItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BuildQueueItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BuildQueueItem clone() => BuildQueueItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BuildQueueItem copyWith(void Function(BuildQueueItem) updates) => super.copyWith((message) => updates(message as BuildQueueItem)) as BuildQueueItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BuildQueueItem create() => BuildQueueItem._();
  BuildQueueItem createEmptyInstance() => create();
  static $pb.PbList<BuildQueueItem> createRepeated() => $pb.PbList<BuildQueueItem>();
  @$core.pragma('dart2js:noInline')
  static BuildQueueItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BuildQueueItem>(create);
  static BuildQueueItem _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get buildingId => $_getI64(0);
  @$pb.TagNumber(1)
  set buildingId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasBuildingId() => $_has(0);
  @$pb.TagNumber(1)
  void clearBuildingId() => clearField(1);

  @$pb.TagNumber(2)
  $12.Timestamp get buildStart => $_getN(1);
  @$pb.TagNumber(2)
  set buildStart($12.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasBuildStart() => $_has(1);
  @$pb.TagNumber(2)
  void clearBuildStart() => clearField(2);
  @$pb.TagNumber(2)
  $12.Timestamp ensureBuildStart() => $_ensure(1);

  @$pb.TagNumber(3)
  $12.Timestamp get buildEnd => $_getN(2);
  @$pb.TagNumber(3)
  set buildEnd($12.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasBuildEnd() => $_has(2);
  @$pb.TagNumber(3)
  void clearBuildEnd() => clearField(3);
  @$pb.TagNumber(3)
  $12.Timestamp ensureBuildEnd() => $_ensure(2);
}

class Position extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Position', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'settlement'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'x')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'y')
    ..hasRequiredFields = false
  ;

  Position._() : super();
  factory Position({
    $fixnum.Int64 x,
    $fixnum.Int64 y,
  }) {
    final _result = create();
    if (x != null) {
      _result.x = x;
    }
    if (y != null) {
      _result.y = y;
    }
    return _result;
  }
  factory Position.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Position.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Position clone() => Position()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Position copyWith(void Function(Position) updates) => super.copyWith((message) => updates(message as Position)) as Position; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Position create() => Position._();
  Position createEmptyInstance() => create();
  static $pb.PbList<Position> createRepeated() => $pb.PbList<Position>();
  @$core.pragma('dart2js:noInline')
  static Position getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Position>(create);
  static Position _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get x => $_getI64(0);
  @$pb.TagNumber(1)
  set x($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasX() => $_has(0);
  @$pb.TagNumber(1)
  void clearX() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get y => $_getI64(1);
  @$pb.TagNumber(2)
  set y($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasY() => $_has(1);
  @$pb.TagNumber(2)
  void clearY() => clearField(2);
}

class Depot extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Depot', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'settlement'), createEmptyInstance: create)
    ..pc<ResourceStorage>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Store', $pb.PbFieldType.PM, protoName: 'Store', subBuilder: ResourceStorage.create)
    ..hasRequiredFields = false
  ;

  Depot._() : super();
  factory Depot({
    $core.Iterable<ResourceStorage> store,
  }) {
    final _result = create();
    if (store != null) {
      _result.store.addAll(store);
    }
    return _result;
  }
  factory Depot.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Depot.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Depot clone() => Depot()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Depot copyWith(void Function(Depot) updates) => super.copyWith((message) => updates(message as Depot)) as Depot; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Depot create() => Depot._();
  Depot createEmptyInstance() => create();
  static $pb.PbList<Depot> createRepeated() => $pb.PbList<Depot>();
  @$core.pragma('dart2js:noInline')
  static Depot getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Depot>(create);
  static Depot _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<ResourceStorage> get store => $_getList(0);
}

class ResourceStorage extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ResourceStorage', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'settlement'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'capacity')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'currentStored', protoName: 'currentStored')
    ..hasRequiredFields = false
  ;

  ResourceStorage._() : super();
  factory ResourceStorage({
    $fixnum.Int64 capacity,
    $fixnum.Int64 currentStored,
  }) {
    final _result = create();
    if (capacity != null) {
      _result.capacity = capacity;
    }
    if (currentStored != null) {
      _result.currentStored = currentStored;
    }
    return _result;
  }
  factory ResourceStorage.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ResourceStorage.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ResourceStorage clone() => ResourceStorage()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ResourceStorage copyWith(void Function(ResourceStorage) updates) => super.copyWith((message) => updates(message as ResourceStorage)) as ResourceStorage; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ResourceStorage create() => ResourceStorage._();
  ResourceStorage createEmptyInstance() => create();
  static $pb.PbList<ResourceStorage> createRepeated() => $pb.PbList<ResourceStorage>();
  @$core.pragma('dart2js:noInline')
  static ResourceStorage getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ResourceStorage>(create);
  static ResourceStorage _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get capacity => $_getI64(0);
  @$pb.TagNumber(1)
  set capacity($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCapacity() => $_has(0);
  @$pb.TagNumber(1)
  void clearCapacity() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get currentStored => $_getI64(1);
  @$pb.TagNumber(2)
  set currentStored($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCurrentStored() => $_has(1);
  @$pb.TagNumber(2)
  void clearCurrentStored() => clearField(2);
}

class SettlementList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SettlementList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'settlement'), createEmptyInstance: create)
    ..pc<Settlement>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'settlements', $pb.PbFieldType.PM, subBuilder: Settlement.create)
    ..hasRequiredFields = false
  ;

  SettlementList._() : super();
  factory SettlementList({
    $core.Iterable<Settlement> settlements,
  }) {
    final _result = create();
    if (settlements != null) {
      _result.settlements.addAll(settlements);
    }
    return _result;
  }
  factory SettlementList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SettlementList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SettlementList clone() => SettlementList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SettlementList copyWith(void Function(SettlementList) updates) => super.copyWith((message) => updates(message as SettlementList)) as SettlementList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SettlementList create() => SettlementList._();
  SettlementList createEmptyInstance() => create();
  static $pb.PbList<SettlementList> createRepeated() => $pb.PbList<SettlementList>();
  @$core.pragma('dart2js:noInline')
  static SettlementList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SettlementList>(create);
  static SettlementList _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Settlement> get settlements => $_getList(0);
}

class Building extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Building', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'settlement'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buildDef', protoName: 'buildDef')
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'inConstruction', protoName: 'inConstruction')
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hibernated')
    ..hasRequiredFields = false
  ;

  Building._() : super();
  factory Building({
    $fixnum.Int64 id,
    $core.String buildDef,
    $core.bool inConstruction,
    $core.bool hibernated,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (buildDef != null) {
      _result.buildDef = buildDef;
    }
    if (inConstruction != null) {
      _result.inConstruction = inConstruction;
    }
    if (hibernated != null) {
      _result.hibernated = hibernated;
    }
    return _result;
  }
  factory Building.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Building.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Building clone() => Building()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Building copyWith(void Function(Building) updates) => super.copyWith((message) => updates(message as Building)) as Building; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Building create() => Building._();
  Building createEmptyInstance() => create();
  static $pb.PbList<Building> createRepeated() => $pb.PbList<Building>();
  @$core.pragma('dart2js:noInline')
  static Building getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Building>(create);
  static Building _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(1)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(3)
  $core.String get buildDef => $_getSZ(1);
  @$pb.TagNumber(3)
  set buildDef($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(3)
  $core.bool hasBuildDef() => $_has(1);
  @$pb.TagNumber(3)
  void clearBuildDef() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get inConstruction => $_getBF(2);
  @$pb.TagNumber(4)
  set inConstruction($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(4)
  $core.bool hasInConstruction() => $_has(2);
  @$pb.TagNumber(4)
  void clearInConstruction() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get hibernated => $_getBF(3);
  @$pb.TagNumber(5)
  set hibernated($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(5)
  $core.bool hasHibernated() => $_has(3);
  @$pb.TagNumber(5)
  void clearHibernated() => clearField(5);
}

class BuildBuildingCall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BuildBuildingCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'settlement'), createEmptyInstance: create)
    ..aOM<$0.SettlementId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'settlementId', protoName: 'settlementId', subBuilder: $0.SettlementId.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buildDef', protoName: 'buildDef')
    ..hasRequiredFields = false
  ;

  BuildBuildingCall._() : super();
  factory BuildBuildingCall({
    $0.SettlementId settlementId,
    $core.String buildDef,
  }) {
    final _result = create();
    if (settlementId != null) {
      _result.settlementId = settlementId;
    }
    if (buildDef != null) {
      _result.buildDef = buildDef;
    }
    return _result;
  }
  factory BuildBuildingCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BuildBuildingCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BuildBuildingCall clone() => BuildBuildingCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BuildBuildingCall copyWith(void Function(BuildBuildingCall) updates) => super.copyWith((message) => updates(message as BuildBuildingCall)) as BuildBuildingCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BuildBuildingCall create() => BuildBuildingCall._();
  BuildBuildingCall createEmptyInstance() => create();
  static $pb.PbList<BuildBuildingCall> createRepeated() => $pb.PbList<BuildBuildingCall>();
  @$core.pragma('dart2js:noInline')
  static BuildBuildingCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BuildBuildingCall>(create);
  static BuildBuildingCall _defaultInstance;

  @$pb.TagNumber(1)
  $0.SettlementId get settlementId => $_getN(0);
  @$pb.TagNumber(1)
  set settlementId($0.SettlementId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSettlementId() => $_has(0);
  @$pb.TagNumber(1)
  void clearSettlementId() => clearField(1);
  @$pb.TagNumber(1)
  $0.SettlementId ensureSettlementId() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get buildDef => $_getSZ(1);
  @$pb.TagNumber(2)
  set buildDef($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasBuildDef() => $_has(1);
  @$pb.TagNumber(2)
  void clearBuildDef() => clearField(2);
}

class UpgradeBuildingCall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpgradeBuildingCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'settlement'), createEmptyInstance: create)
    ..aOM<$0.SettlementId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'settlementId', protoName: 'settlementId', subBuilder: $0.SettlementId.create)
    ..aOM<$0.BuildingId>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buildingId', protoName: 'buildingId', subBuilder: $0.BuildingId.create)
    ..hasRequiredFields = false
  ;

  UpgradeBuildingCall._() : super();
  factory UpgradeBuildingCall({
    $0.SettlementId settlementId,
    $0.BuildingId buildingId,
  }) {
    final _result = create();
    if (settlementId != null) {
      _result.settlementId = settlementId;
    }
    if (buildingId != null) {
      _result.buildingId = buildingId;
    }
    return _result;
  }
  factory UpgradeBuildingCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpgradeBuildingCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpgradeBuildingCall clone() => UpgradeBuildingCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpgradeBuildingCall copyWith(void Function(UpgradeBuildingCall) updates) => super.copyWith((message) => updates(message as UpgradeBuildingCall)) as UpgradeBuildingCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpgradeBuildingCall create() => UpgradeBuildingCall._();
  UpgradeBuildingCall createEmptyInstance() => create();
  static $pb.PbList<UpgradeBuildingCall> createRepeated() => $pb.PbList<UpgradeBuildingCall>();
  @$core.pragma('dart2js:noInline')
  static UpgradeBuildingCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpgradeBuildingCall>(create);
  static UpgradeBuildingCall _defaultInstance;

  @$pb.TagNumber(1)
  $0.SettlementId get settlementId => $_getN(0);
  @$pb.TagNumber(1)
  set settlementId($0.SettlementId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSettlementId() => $_has(0);
  @$pb.TagNumber(1)
  void clearSettlementId() => clearField(1);
  @$pb.TagNumber(1)
  $0.SettlementId ensureSettlementId() => $_ensure(0);

  @$pb.TagNumber(2)
  $0.BuildingId get buildingId => $_getN(1);
  @$pb.TagNumber(2)
  set buildingId($0.BuildingId v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasBuildingId() => $_has(1);
  @$pb.TagNumber(2)
  void clearBuildingId() => clearField(2);
  @$pb.TagNumber(2)
  $0.BuildingId ensureBuildingId() => $_ensure(1);
}

