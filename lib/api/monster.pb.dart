///
//  Generated code. Do not modify.
//  source: monster.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'google/protobuf/timestamp.pb.dart' as $12;
import 'common.pb.dart' as $0;

class Monster extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Monster', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', $pb.PbFieldType.O3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'playerID', $pb.PbFieldType.O3, protoName: 'playerID')
    ..aOM<BattleValues>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'battleValues', protoName: 'battleValues', subBuilder: BattleValues.create)
    ..aOM<BodyValues>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'bodyValues', protoName: 'bodyValues', subBuilder: BodyValues.create)
    ..aOM<Activity>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'activity', subBuilder: Activity.create)
    ..aOM<StatusFlags>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', subBuilder: StatusFlags.create)
    ..aOM<$12.Timestamp>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastUpdate', subBuilder: $12.Timestamp.create)
    ..aOB(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isTest')
    ..hasRequiredFields = false
  ;

  Monster._() : super();
  factory Monster({
    $core.String name,
    $core.int id,
    $core.int playerID,
    BattleValues battleValues,
    BodyValues bodyValues,
    Activity activity,
    StatusFlags status,
    $12.Timestamp lastUpdate,
    $core.bool isTest,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (id != null) {
      _result.id = id;
    }
    if (playerID != null) {
      _result.playerID = playerID;
    }
    if (battleValues != null) {
      _result.battleValues = battleValues;
    }
    if (bodyValues != null) {
      _result.bodyValues = bodyValues;
    }
    if (activity != null) {
      _result.activity = activity;
    }
    if (status != null) {
      _result.status = status;
    }
    if (lastUpdate != null) {
      _result.lastUpdate = lastUpdate;
    }
    if (isTest != null) {
      _result.isTest = isTest;
    }
    return _result;
  }
  factory Monster.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Monster.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Monster clone() => Monster()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Monster copyWith(void Function(Monster) updates) => super.copyWith((message) => updates(message as Monster)) as Monster; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Monster create() => Monster._();
  Monster createEmptyInstance() => create();
  static $pb.PbList<Monster> createRepeated() => $pb.PbList<Monster>();
  @$core.pragma('dart2js:noInline')
  static Monster getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Monster>(create);
  static Monster _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get id => $_getIZ(1);
  @$pb.TagNumber(2)
  set id($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasId() => $_has(1);
  @$pb.TagNumber(2)
  void clearId() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get playerID => $_getIZ(2);
  @$pb.TagNumber(3)
  set playerID($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPlayerID() => $_has(2);
  @$pb.TagNumber(3)
  void clearPlayerID() => clearField(3);

  @$pb.TagNumber(4)
  BattleValues get battleValues => $_getN(3);
  @$pb.TagNumber(4)
  set battleValues(BattleValues v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasBattleValues() => $_has(3);
  @$pb.TagNumber(4)
  void clearBattleValues() => clearField(4);
  @$pb.TagNumber(4)
  BattleValues ensureBattleValues() => $_ensure(3);

  @$pb.TagNumber(5)
  BodyValues get bodyValues => $_getN(4);
  @$pb.TagNumber(5)
  set bodyValues(BodyValues v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasBodyValues() => $_has(4);
  @$pb.TagNumber(5)
  void clearBodyValues() => clearField(5);
  @$pb.TagNumber(5)
  BodyValues ensureBodyValues() => $_ensure(4);

  @$pb.TagNumber(6)
  Activity get activity => $_getN(5);
  @$pb.TagNumber(6)
  set activity(Activity v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasActivity() => $_has(5);
  @$pb.TagNumber(6)
  void clearActivity() => clearField(6);
  @$pb.TagNumber(6)
  Activity ensureActivity() => $_ensure(5);

  @$pb.TagNumber(7)
  StatusFlags get status => $_getN(6);
  @$pb.TagNumber(7)
  set status(StatusFlags v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasStatus() => $_has(6);
  @$pb.TagNumber(7)
  void clearStatus() => clearField(7);
  @$pb.TagNumber(7)
  StatusFlags ensureStatus() => $_ensure(6);

  @$pb.TagNumber(8)
  $12.Timestamp get lastUpdate => $_getN(7);
  @$pb.TagNumber(8)
  set lastUpdate($12.Timestamp v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasLastUpdate() => $_has(7);
  @$pb.TagNumber(8)
  void clearLastUpdate() => clearField(8);
  @$pb.TagNumber(8)
  $12.Timestamp ensureLastUpdate() => $_ensure(7);

  @$pb.TagNumber(10)
  $core.bool get isTest => $_getBF(8);
  @$pb.TagNumber(10)
  set isTest($core.bool v) { $_setBool(8, v); }
  @$pb.TagNumber(10)
  $core.bool hasIsTest() => $_has(8);
  @$pb.TagNumber(10)
  void clearIsTest() => clearField(10);
}

class StatusFlags extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'StatusFlags', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isAlive')
    ..hasRequiredFields = false
  ;

  StatusFlags._() : super();
  factory StatusFlags({
    $core.bool isAlive,
  }) {
    final _result = create();
    if (isAlive != null) {
      _result.isAlive = isAlive;
    }
    return _result;
  }
  factory StatusFlags.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory StatusFlags.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  StatusFlags clone() => StatusFlags()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  StatusFlags copyWith(void Function(StatusFlags) updates) => super.copyWith((message) => updates(message as StatusFlags)) as StatusFlags; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StatusFlags create() => StatusFlags._();
  StatusFlags createEmptyInstance() => create();
  static $pb.PbList<StatusFlags> createRepeated() => $pb.PbList<StatusFlags>();
  @$core.pragma('dart2js:noInline')
  static StatusFlags getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<StatusFlags>(create);
  static StatusFlags _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get isAlive => $_getBF(0);
  @$pb.TagNumber(1)
  set isAlive($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasIsAlive() => $_has(0);
  @$pb.TagNumber(1)
  void clearIsAlive() => clearField(1);
}

class BattleValues extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BattleValues', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'attack', $pb.PbFieldType.O3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'defense', $pb.PbFieldType.O3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxHp', $pb.PbFieldType.O3)
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'remainingHp', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  BattleValues._() : super();
  factory BattleValues({
    $core.int attack,
    $core.int defense,
    $core.int maxHp,
    $core.int remainingHp,
  }) {
    final _result = create();
    if (attack != null) {
      _result.attack = attack;
    }
    if (defense != null) {
      _result.defense = defense;
    }
    if (maxHp != null) {
      _result.maxHp = maxHp;
    }
    if (remainingHp != null) {
      _result.remainingHp = remainingHp;
    }
    return _result;
  }
  factory BattleValues.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BattleValues.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BattleValues clone() => BattleValues()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BattleValues copyWith(void Function(BattleValues) updates) => super.copyWith((message) => updates(message as BattleValues)) as BattleValues; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BattleValues create() => BattleValues._();
  BattleValues createEmptyInstance() => create();
  static $pb.PbList<BattleValues> createRepeated() => $pb.PbList<BattleValues>();
  @$core.pragma('dart2js:noInline')
  static BattleValues getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BattleValues>(create);
  static BattleValues _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get attack => $_getIZ(0);
  @$pb.TagNumber(1)
  set attack($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAttack() => $_has(0);
  @$pb.TagNumber(1)
  void clearAttack() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get defense => $_getIZ(1);
  @$pb.TagNumber(2)
  set defense($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDefense() => $_has(1);
  @$pb.TagNumber(2)
  void clearDefense() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get maxHp => $_getIZ(2);
  @$pb.TagNumber(3)
  set maxHp($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMaxHp() => $_has(2);
  @$pb.TagNumber(3)
  void clearMaxHp() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get remainingHp => $_getIZ(3);
  @$pb.TagNumber(4)
  set remainingHp($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasRemainingHp() => $_has(3);
  @$pb.TagNumber(4)
  void clearRemainingHp() => clearField(4);
}

class BodyValues extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BodyValues', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'remainingSaturation', $pb.PbFieldType.OD)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxSaturation', $pb.PbFieldType.OD)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'mass', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  BodyValues._() : super();
  factory BodyValues({
    $core.double remainingSaturation,
    $core.double maxSaturation,
    $core.int mass,
  }) {
    final _result = create();
    if (remainingSaturation != null) {
      _result.remainingSaturation = remainingSaturation;
    }
    if (maxSaturation != null) {
      _result.maxSaturation = maxSaturation;
    }
    if (mass != null) {
      _result.mass = mass;
    }
    return _result;
  }
  factory BodyValues.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BodyValues.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BodyValues clone() => BodyValues()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BodyValues copyWith(void Function(BodyValues) updates) => super.copyWith((message) => updates(message as BodyValues)) as BodyValues; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BodyValues create() => BodyValues._();
  BodyValues createEmptyInstance() => create();
  static $pb.PbList<BodyValues> createRepeated() => $pb.PbList<BodyValues>();
  @$core.pragma('dart2js:noInline')
  static BodyValues getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BodyValues>(create);
  static BodyValues _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get remainingSaturation => $_getN(0);
  @$pb.TagNumber(1)
  set remainingSaturation($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRemainingSaturation() => $_has(0);
  @$pb.TagNumber(1)
  void clearRemainingSaturation() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get maxSaturation => $_getN(1);
  @$pb.TagNumber(2)
  set maxSaturation($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMaxSaturation() => $_has(1);
  @$pb.TagNumber(2)
  void clearMaxSaturation() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get mass => $_getIZ(2);
  @$pb.TagNumber(3)
  set mass($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMass() => $_has(2);
  @$pb.TagNumber(3)
  void clearMass() => clearField(3);
}

enum Activity_Activity {
  questId, 
  battleID, 
  notSet
}

class Activity extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Activity_Activity> _Activity_ActivityByTag = {
    1 : Activity_Activity.questId,
    2 : Activity_Activity.battleID,
    0 : Activity_Activity.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Activity', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOM<$0.QuestId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'questId', protoName: 'questId', subBuilder: $0.QuestId.create)
    ..aOM<$0.BattleId>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'battleID', protoName: 'battleID', subBuilder: $0.BattleId.create)
    ..hasRequiredFields = false
  ;

  Activity._() : super();
  factory Activity({
    $0.QuestId questId,
    $0.BattleId battleID,
  }) {
    final _result = create();
    if (questId != null) {
      _result.questId = questId;
    }
    if (battleID != null) {
      _result.battleID = battleID;
    }
    return _result;
  }
  factory Activity.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Activity.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Activity clone() => Activity()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Activity copyWith(void Function(Activity) updates) => super.copyWith((message) => updates(message as Activity)) as Activity; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Activity create() => Activity._();
  Activity createEmptyInstance() => create();
  static $pb.PbList<Activity> createRepeated() => $pb.PbList<Activity>();
  @$core.pragma('dart2js:noInline')
  static Activity getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Activity>(create);
  static Activity _defaultInstance;

  Activity_Activity whichActivity() => _Activity_ActivityByTag[$_whichOneof(0)];
  void clearActivity() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $0.QuestId get questId => $_getN(0);
  @$pb.TagNumber(1)
  set questId($0.QuestId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuestId() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuestId() => clearField(1);
  @$pb.TagNumber(1)
  $0.QuestId ensureQuestId() => $_ensure(0);

  @$pb.TagNumber(2)
  $0.BattleId get battleID => $_getN(1);
  @$pb.TagNumber(2)
  set battleID($0.BattleId v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasBattleID() => $_has(1);
  @$pb.TagNumber(2)
  void clearBattleID() => clearField(2);
  @$pb.TagNumber(2)
  $0.BattleId ensureBattleID() => $_ensure(1);
}

class AddMonCall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AddMonCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOM<Monster>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'mon', subBuilder: Monster.create)
    ..hasRequiredFields = false
  ;

  AddMonCall._() : super();
  factory AddMonCall({
    Monster mon,
  }) {
    final _result = create();
    if (mon != null) {
      _result.mon = mon;
    }
    return _result;
  }
  factory AddMonCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddMonCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AddMonCall clone() => AddMonCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AddMonCall copyWith(void Function(AddMonCall) updates) => super.copyWith((message) => updates(message as AddMonCall)) as AddMonCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddMonCall create() => AddMonCall._();
  AddMonCall createEmptyInstance() => create();
  static $pb.PbList<AddMonCall> createRepeated() => $pb.PbList<AddMonCall>();
  @$core.pragma('dart2js:noInline')
  static AddMonCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddMonCall>(create);
  static AddMonCall _defaultInstance;

  @$pb.TagNumber(1)
  Monster get mon => $_getN(0);
  @$pb.TagNumber(1)
  set mon(Monster v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMon() => $_has(0);
  @$pb.TagNumber(1)
  void clearMon() => clearField(1);
  @$pb.TagNumber(1)
  Monster ensureMon() => $_ensure(0);
}

class GetMonByIDCall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetMonByIDCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOM<$0.MonId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', subBuilder: $0.MonId.create)
    ..hasRequiredFields = false
  ;

  GetMonByIDCall._() : super();
  factory GetMonByIDCall({
    $0.MonId id,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    return _result;
  }
  factory GetMonByIDCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetMonByIDCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetMonByIDCall clone() => GetMonByIDCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetMonByIDCall copyWith(void Function(GetMonByIDCall) updates) => super.copyWith((message) => updates(message as GetMonByIDCall)) as GetMonByIDCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetMonByIDCall create() => GetMonByIDCall._();
  GetMonByIDCall createEmptyInstance() => create();
  static $pb.PbList<GetMonByIDCall> createRepeated() => $pb.PbList<GetMonByIDCall>();
  @$core.pragma('dart2js:noInline')
  static GetMonByIDCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetMonByIDCall>(create);
  static GetMonByIDCall _defaultInstance;

  @$pb.TagNumber(1)
  $0.MonId get id => $_getN(0);
  @$pb.TagNumber(1)
  set id($0.MonId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
  @$pb.TagNumber(1)
  $0.MonId ensureId() => $_ensure(0);
}

class GetMonByUserIDCall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetMonByUserIDCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOM<$0.UserId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId', protoName: 'userId', subBuilder: $0.UserId.create)
    ..hasRequiredFields = false
  ;

  GetMonByUserIDCall._() : super();
  factory GetMonByUserIDCall({
    $0.UserId userId,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    return _result;
  }
  factory GetMonByUserIDCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetMonByUserIDCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetMonByUserIDCall clone() => GetMonByUserIDCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetMonByUserIDCall copyWith(void Function(GetMonByUserIDCall) updates) => super.copyWith((message) => updates(message as GetMonByUserIDCall)) as GetMonByUserIDCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetMonByUserIDCall create() => GetMonByUserIDCall._();
  GetMonByUserIDCall createEmptyInstance() => create();
  static $pb.PbList<GetMonByUserIDCall> createRepeated() => $pb.PbList<GetMonByUserIDCall>();
  @$core.pragma('dart2js:noInline')
  static GetMonByUserIDCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetMonByUserIDCall>(create);
  static GetMonByUserIDCall _defaultInstance;

  @$pb.TagNumber(1)
  $0.UserId get userId => $_getN(0);
  @$pb.TagNumber(1)
  set userId($0.UserId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);
  @$pb.TagNumber(1)
  $0.UserId ensureUserId() => $_ensure(0);
}

class UpdateMonCall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpdateMonCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOM<Monster>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'mon', subBuilder: Monster.create)
    ..hasRequiredFields = false
  ;

  UpdateMonCall._() : super();
  factory UpdateMonCall({
    Monster mon,
  }) {
    final _result = create();
    if (mon != null) {
      _result.mon = mon;
    }
    return _result;
  }
  factory UpdateMonCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateMonCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpdateMonCall clone() => UpdateMonCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpdateMonCall copyWith(void Function(UpdateMonCall) updates) => super.copyWith((message) => updates(message as UpdateMonCall)) as UpdateMonCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateMonCall create() => UpdateMonCall._();
  UpdateMonCall createEmptyInstance() => create();
  static $pb.PbList<UpdateMonCall> createRepeated() => $pb.PbList<UpdateMonCall>();
  @$core.pragma('dart2js:noInline')
  static UpdateMonCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateMonCall>(create);
  static UpdateMonCall _defaultInstance;

  @$pb.TagNumber(1)
  Monster get mon => $_getN(0);
  @$pb.TagNumber(1)
  set mon(Monster v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMon() => $_has(0);
  @$pb.TagNumber(1)
  void clearMon() => clearField(1);
  @$pb.TagNumber(1)
  Monster ensureMon() => $_ensure(0);
}

class DeleteMonCall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DeleteMonCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOM<$0.MonId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', subBuilder: $0.MonId.create)
    ..hasRequiredFields = false
  ;

  DeleteMonCall._() : super();
  factory DeleteMonCall({
    $0.MonId id,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    return _result;
  }
  factory DeleteMonCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeleteMonCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DeleteMonCall clone() => DeleteMonCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DeleteMonCall copyWith(void Function(DeleteMonCall) updates) => super.copyWith((message) => updates(message as DeleteMonCall)) as DeleteMonCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeleteMonCall create() => DeleteMonCall._();
  DeleteMonCall createEmptyInstance() => create();
  static $pb.PbList<DeleteMonCall> createRepeated() => $pb.PbList<DeleteMonCall>();
  @$core.pragma('dart2js:noInline')
  static DeleteMonCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeleteMonCall>(create);
  static DeleteMonCall _defaultInstance;

  @$pb.TagNumber(1)
  $0.MonId get id => $_getN(0);
  @$pb.TagNumber(1)
  set id($0.MonId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
  @$pb.TagNumber(1)
  $0.MonId ensureId() => $_ensure(0);
}

class LockRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LockRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOM<$0.MonId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', subBuilder: $0.MonId.create)
    ..aOM<Activity>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'activity', subBuilder: Activity.create)
    ..hasRequiredFields = false
  ;

  LockRequest._() : super();
  factory LockRequest({
    $0.MonId id,
    Activity activity,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (activity != null) {
      _result.activity = activity;
    }
    return _result;
  }
  factory LockRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LockRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LockRequest clone() => LockRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LockRequest copyWith(void Function(LockRequest) updates) => super.copyWith((message) => updates(message as LockRequest)) as LockRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LockRequest create() => LockRequest._();
  LockRequest createEmptyInstance() => create();
  static $pb.PbList<LockRequest> createRepeated() => $pb.PbList<LockRequest>();
  @$core.pragma('dart2js:noInline')
  static LockRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LockRequest>(create);
  static LockRequest _defaultInstance;

  @$pb.TagNumber(1)
  $0.MonId get id => $_getN(0);
  @$pb.TagNumber(1)
  set id($0.MonId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
  @$pb.TagNumber(1)
  $0.MonId ensureId() => $_ensure(0);

  @$pb.TagNumber(2)
  Activity get activity => $_getN(1);
  @$pb.TagNumber(2)
  set activity(Activity v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasActivity() => $_has(1);
  @$pb.TagNumber(2)
  void clearActivity() => clearField(2);
  @$pb.TagNumber(2)
  Activity ensureActivity() => $_ensure(1);
}

class ReleaseRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ReleaseRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOM<$0.MonId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', subBuilder: $0.MonId.create)
    ..hasRequiredFields = false
  ;

  ReleaseRequest._() : super();
  factory ReleaseRequest({
    $0.MonId id,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    return _result;
  }
  factory ReleaseRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ReleaseRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ReleaseRequest clone() => ReleaseRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ReleaseRequest copyWith(void Function(ReleaseRequest) updates) => super.copyWith((message) => updates(message as ReleaseRequest)) as ReleaseRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ReleaseRequest create() => ReleaseRequest._();
  ReleaseRequest createEmptyInstance() => create();
  static $pb.PbList<ReleaseRequest> createRepeated() => $pb.PbList<ReleaseRequest>();
  @$core.pragma('dart2js:noInline')
  static ReleaseRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ReleaseRequest>(create);
  static ReleaseRequest _defaultInstance;

  @$pb.TagNumber(1)
  $0.MonId get id => $_getN(0);
  @$pb.TagNumber(1)
  set id($0.MonId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
  @$pb.TagNumber(1)
  $0.MonId ensureId() => $_ensure(0);
}

class MonsterDeletedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MonsterDeletedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOM<$0.MonId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'monId', protoName: 'monId', subBuilder: $0.MonId.create)
    ..hasRequiredFields = false
  ;

  MonsterDeletedEvent._() : super();
  factory MonsterDeletedEvent({
    $0.MonId monId,
  }) {
    final _result = create();
    if (monId != null) {
      _result.monId = monId;
    }
    return _result;
  }
  factory MonsterDeletedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MonsterDeletedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MonsterDeletedEvent clone() => MonsterDeletedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MonsterDeletedEvent copyWith(void Function(MonsterDeletedEvent) updates) => super.copyWith((message) => updates(message as MonsterDeletedEvent)) as MonsterDeletedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MonsterDeletedEvent create() => MonsterDeletedEvent._();
  MonsterDeletedEvent createEmptyInstance() => create();
  static $pb.PbList<MonsterDeletedEvent> createRepeated() => $pb.PbList<MonsterDeletedEvent>();
  @$core.pragma('dart2js:noInline')
  static MonsterDeletedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MonsterDeletedEvent>(create);
  static MonsterDeletedEvent _defaultInstance;

  @$pb.TagNumber(1)
  $0.MonId get monId => $_getN(0);
  @$pb.TagNumber(1)
  set monId($0.MonId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMonId() => $_has(0);
  @$pb.TagNumber(1)
  void clearMonId() => clearField(1);
  @$pb.TagNumber(1)
  $0.MonId ensureMonId() => $_ensure(0);
}

class MonsterAddedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MonsterAddedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOM<Monster>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'mon', subBuilder: Monster.create)
    ..hasRequiredFields = false
  ;

  MonsterAddedEvent._() : super();
  factory MonsterAddedEvent({
    Monster mon,
  }) {
    final _result = create();
    if (mon != null) {
      _result.mon = mon;
    }
    return _result;
  }
  factory MonsterAddedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MonsterAddedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MonsterAddedEvent clone() => MonsterAddedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MonsterAddedEvent copyWith(void Function(MonsterAddedEvent) updates) => super.copyWith((message) => updates(message as MonsterAddedEvent)) as MonsterAddedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MonsterAddedEvent create() => MonsterAddedEvent._();
  MonsterAddedEvent createEmptyInstance() => create();
  static $pb.PbList<MonsterAddedEvent> createRepeated() => $pb.PbList<MonsterAddedEvent>();
  @$core.pragma('dart2js:noInline')
  static MonsterAddedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MonsterAddedEvent>(create);
  static MonsterAddedEvent _defaultInstance;

  @$pb.TagNumber(1)
  Monster get mon => $_getN(0);
  @$pb.TagNumber(1)
  set mon(Monster v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMon() => $_has(0);
  @$pb.TagNumber(1)
  void clearMon() => clearField(1);
  @$pb.TagNumber(1)
  Monster ensureMon() => $_ensure(0);
}

class MonsterUpdatedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MonsterUpdatedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..aOM<Monster>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'mon', subBuilder: Monster.create)
    ..hasRequiredFields = false
  ;

  MonsterUpdatedEvent._() : super();
  factory MonsterUpdatedEvent({
    Monster mon,
  }) {
    final _result = create();
    if (mon != null) {
      _result.mon = mon;
    }
    return _result;
  }
  factory MonsterUpdatedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MonsterUpdatedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MonsterUpdatedEvent clone() => MonsterUpdatedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MonsterUpdatedEvent copyWith(void Function(MonsterUpdatedEvent) updates) => super.copyWith((message) => updates(message as MonsterUpdatedEvent)) as MonsterUpdatedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MonsterUpdatedEvent create() => MonsterUpdatedEvent._();
  MonsterUpdatedEvent createEmptyInstance() => create();
  static $pb.PbList<MonsterUpdatedEvent> createRepeated() => $pb.PbList<MonsterUpdatedEvent>();
  @$core.pragma('dart2js:noInline')
  static MonsterUpdatedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MonsterUpdatedEvent>(create);
  static MonsterUpdatedEvent _defaultInstance;

  @$pb.TagNumber(1)
  Monster get mon => $_getN(0);
  @$pb.TagNumber(1)
  set mon(Monster v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMon() => $_has(0);
  @$pb.TagNumber(1)
  void clearMon() => clearField(1);
  @$pb.TagNumber(1)
  Monster ensureMon() => $_ensure(0);
}

enum MonsterEvent_Event {
  addedEvent, 
  deleteEvent, 
  updateEvent, 
  notSet
}

class MonsterEvent extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, MonsterEvent_Event> _MonsterEvent_EventByTag = {
    1 : MonsterEvent_Event.addedEvent,
    2 : MonsterEvent_Event.deleteEvent,
    3 : MonsterEvent_Event.updateEvent,
    0 : MonsterEvent_Event.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MonsterEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3])
    ..aOM<MonsterAddedEvent>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'addedEvent', protoName: 'addedEvent', subBuilder: MonsterAddedEvent.create)
    ..aOM<MonsterDeletedEvent>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deleteEvent', protoName: 'deleteEvent', subBuilder: MonsterDeletedEvent.create)
    ..aOM<MonsterUpdatedEvent>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updateEvent', protoName: 'updateEvent', subBuilder: MonsterUpdatedEvent.create)
    ..hasRequiredFields = false
  ;

  MonsterEvent._() : super();
  factory MonsterEvent({
    MonsterAddedEvent addedEvent,
    MonsterDeletedEvent deleteEvent,
    MonsterUpdatedEvent updateEvent,
  }) {
    final _result = create();
    if (addedEvent != null) {
      _result.addedEvent = addedEvent;
    }
    if (deleteEvent != null) {
      _result.deleteEvent = deleteEvent;
    }
    if (updateEvent != null) {
      _result.updateEvent = updateEvent;
    }
    return _result;
  }
  factory MonsterEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MonsterEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MonsterEvent clone() => MonsterEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MonsterEvent copyWith(void Function(MonsterEvent) updates) => super.copyWith((message) => updates(message as MonsterEvent)) as MonsterEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MonsterEvent create() => MonsterEvent._();
  MonsterEvent createEmptyInstance() => create();
  static $pb.PbList<MonsterEvent> createRepeated() => $pb.PbList<MonsterEvent>();
  @$core.pragma('dart2js:noInline')
  static MonsterEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MonsterEvent>(create);
  static MonsterEvent _defaultInstance;

  MonsterEvent_Event whichEvent() => _MonsterEvent_EventByTag[$_whichOneof(0)];
  void clearEvent() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  MonsterAddedEvent get addedEvent => $_getN(0);
  @$pb.TagNumber(1)
  set addedEvent(MonsterAddedEvent v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasAddedEvent() => $_has(0);
  @$pb.TagNumber(1)
  void clearAddedEvent() => clearField(1);
  @$pb.TagNumber(1)
  MonsterAddedEvent ensureAddedEvent() => $_ensure(0);

  @$pb.TagNumber(2)
  MonsterDeletedEvent get deleteEvent => $_getN(1);
  @$pb.TagNumber(2)
  set deleteEvent(MonsterDeletedEvent v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasDeleteEvent() => $_has(1);
  @$pb.TagNumber(2)
  void clearDeleteEvent() => clearField(2);
  @$pb.TagNumber(2)
  MonsterDeletedEvent ensureDeleteEvent() => $_ensure(1);

  @$pb.TagNumber(3)
  MonsterUpdatedEvent get updateEvent => $_getN(2);
  @$pb.TagNumber(3)
  set updateEvent(MonsterUpdatedEvent v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasUpdateEvent() => $_has(2);
  @$pb.TagNumber(3)
  void clearUpdateEvent() => clearField(3);
  @$pb.TagNumber(3)
  MonsterUpdatedEvent ensureUpdateEvent() => $_ensure(2);
}

enum MonServiceCall_Method {
  addMon, 
  getbyId, 
  getbyuserId, 
  update, 
  delete, 
  notSet
}

class MonServiceCall extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, MonServiceCall_Method> _MonServiceCall_MethodByTag = {
    1 : MonServiceCall_Method.addMon,
    2 : MonServiceCall_Method.getbyId,
    3 : MonServiceCall_Method.getbyuserId,
    4 : MonServiceCall_Method.update,
    5 : MonServiceCall_Method.delete,
    0 : MonServiceCall_Method.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MonServiceCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4, 5])
    ..aOM<AddMonCall>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'addMon', protoName: 'addMon', subBuilder: AddMonCall.create)
    ..aOM<GetMonByIDCall>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'getbyId', protoName: 'getbyId', subBuilder: GetMonByIDCall.create)
    ..aOM<GetMonByUserIDCall>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'getbyuserId', protoName: 'getbyuserId', subBuilder: GetMonByUserIDCall.create)
    ..aOM<UpdateMonCall>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'update', subBuilder: UpdateMonCall.create)
    ..aOM<DeleteMonCall>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'delete', subBuilder: DeleteMonCall.create)
    ..aOM<$0.MetaData>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'metaData', protoName: 'metaData', subBuilder: $0.MetaData.create)
    ..hasRequiredFields = false
  ;

  MonServiceCall._() : super();
  factory MonServiceCall({
    AddMonCall addMon,
    GetMonByIDCall getbyId,
    GetMonByUserIDCall getbyuserId,
    UpdateMonCall update,
    DeleteMonCall delete,
    $0.MetaData metaData,
  }) {
    final _result = create();
    if (addMon != null) {
      _result.addMon = addMon;
    }
    if (getbyId != null) {
      _result.getbyId = getbyId;
    }
    if (getbyuserId != null) {
      _result.getbyuserId = getbyuserId;
    }
    if (update != null) {
      _result.update = update;
    }
    if (delete != null) {
      _result.delete = delete;
    }
    if (metaData != null) {
      _result.metaData = metaData;
    }
    return _result;
  }
  factory MonServiceCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MonServiceCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MonServiceCall clone() => MonServiceCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MonServiceCall copyWith(void Function(MonServiceCall) updates) => super.copyWith((message) => updates(message as MonServiceCall)) as MonServiceCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MonServiceCall create() => MonServiceCall._();
  MonServiceCall createEmptyInstance() => create();
  static $pb.PbList<MonServiceCall> createRepeated() => $pb.PbList<MonServiceCall>();
  @$core.pragma('dart2js:noInline')
  static MonServiceCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MonServiceCall>(create);
  static MonServiceCall _defaultInstance;

  MonServiceCall_Method whichMethod() => _MonServiceCall_MethodByTag[$_whichOneof(0)];
  void clearMethod() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  AddMonCall get addMon => $_getN(0);
  @$pb.TagNumber(1)
  set addMon(AddMonCall v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasAddMon() => $_has(0);
  @$pb.TagNumber(1)
  void clearAddMon() => clearField(1);
  @$pb.TagNumber(1)
  AddMonCall ensureAddMon() => $_ensure(0);

  @$pb.TagNumber(2)
  GetMonByIDCall get getbyId => $_getN(1);
  @$pb.TagNumber(2)
  set getbyId(GetMonByIDCall v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasGetbyId() => $_has(1);
  @$pb.TagNumber(2)
  void clearGetbyId() => clearField(2);
  @$pb.TagNumber(2)
  GetMonByIDCall ensureGetbyId() => $_ensure(1);

  @$pb.TagNumber(3)
  GetMonByUserIDCall get getbyuserId => $_getN(2);
  @$pb.TagNumber(3)
  set getbyuserId(GetMonByUserIDCall v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasGetbyuserId() => $_has(2);
  @$pb.TagNumber(3)
  void clearGetbyuserId() => clearField(3);
  @$pb.TagNumber(3)
  GetMonByUserIDCall ensureGetbyuserId() => $_ensure(2);

  @$pb.TagNumber(4)
  UpdateMonCall get update => $_getN(3);
  @$pb.TagNumber(4)
  set update(UpdateMonCall v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasUpdate() => $_has(3);
  @$pb.TagNumber(4)
  void clearUpdate() => clearField(4);
  @$pb.TagNumber(4)
  UpdateMonCall ensureUpdate() => $_ensure(3);

  @$pb.TagNumber(5)
  DeleteMonCall get delete => $_getN(4);
  @$pb.TagNumber(5)
  set delete(DeleteMonCall v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasDelete() => $_has(4);
  @$pb.TagNumber(5)
  void clearDelete() => clearField(5);
  @$pb.TagNumber(5)
  DeleteMonCall ensureDelete() => $_ensure(4);

  @$pb.TagNumber(6)
  $0.MetaData get metaData => $_getN(5);
  @$pb.TagNumber(6)
  set metaData($0.MetaData v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasMetaData() => $_has(5);
  @$pb.TagNumber(6)
  void clearMetaData() => clearField(6);
  @$pb.TagNumber(6)
  $0.MetaData ensureMetaData() => $_ensure(5);
}

class MonsterList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MonsterList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..pc<Monster>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'monsters', $pb.PbFieldType.PM, subBuilder: Monster.create)
    ..hasRequiredFields = false
  ;

  MonsterList._() : super();
  factory MonsterList({
    $core.Iterable<Monster> monsters,
  }) {
    final _result = create();
    if (monsters != null) {
      _result.monsters.addAll(monsters);
    }
    return _result;
  }
  factory MonsterList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MonsterList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MonsterList clone() => MonsterList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MonsterList copyWith(void Function(MonsterList) updates) => super.copyWith((message) => updates(message as MonsterList)) as MonsterList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MonsterList create() => MonsterList._();
  MonsterList createEmptyInstance() => create();
  static $pb.PbList<MonsterList> createRepeated() => $pb.PbList<MonsterList>();
  @$core.pragma('dart2js:noInline')
  static MonsterList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MonsterList>(create);
  static MonsterList _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Monster> get monsters => $_getList(0);
}

enum MonServiceResponse_Data {
  id, 
  mon, 
  status, 
  monsters, 
  notSet
}

class MonServiceResponse extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, MonServiceResponse_Data> _MonServiceResponse_DataByTag = {
    1 : MonServiceResponse_Data.id,
    2 : MonServiceResponse_Data.mon,
    3 : MonServiceResponse_Data.status,
    4 : MonServiceResponse_Data.monsters,
    0 : MonServiceResponse_Data.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MonServiceResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'monster'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4])
    ..aOM<$0.MonId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', subBuilder: $0.MonId.create)
    ..aOM<Monster>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'mon', subBuilder: Monster.create)
    ..aOM<$0.ResponseStatus>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', subBuilder: $0.ResponseStatus.create)
    ..aOM<MonsterList>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'monsters', subBuilder: MonsterList.create)
    ..aOM<$0.MetaData>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'metaData', protoName: 'metaData', subBuilder: $0.MetaData.create)
    ..hasRequiredFields = false
  ;

  MonServiceResponse._() : super();
  factory MonServiceResponse({
    $0.MonId id,
    Monster mon,
    $0.ResponseStatus status,
    MonsterList monsters,
    $0.MetaData metaData,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (mon != null) {
      _result.mon = mon;
    }
    if (status != null) {
      _result.status = status;
    }
    if (monsters != null) {
      _result.monsters = monsters;
    }
    if (metaData != null) {
      _result.metaData = metaData;
    }
    return _result;
  }
  factory MonServiceResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MonServiceResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MonServiceResponse clone() => MonServiceResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MonServiceResponse copyWith(void Function(MonServiceResponse) updates) => super.copyWith((message) => updates(message as MonServiceResponse)) as MonServiceResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MonServiceResponse create() => MonServiceResponse._();
  MonServiceResponse createEmptyInstance() => create();
  static $pb.PbList<MonServiceResponse> createRepeated() => $pb.PbList<MonServiceResponse>();
  @$core.pragma('dart2js:noInline')
  static MonServiceResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MonServiceResponse>(create);
  static MonServiceResponse _defaultInstance;

  MonServiceResponse_Data whichData() => _MonServiceResponse_DataByTag[$_whichOneof(0)];
  void clearData() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $0.MonId get id => $_getN(0);
  @$pb.TagNumber(1)
  set id($0.MonId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
  @$pb.TagNumber(1)
  $0.MonId ensureId() => $_ensure(0);

  @$pb.TagNumber(2)
  Monster get mon => $_getN(1);
  @$pb.TagNumber(2)
  set mon(Monster v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasMon() => $_has(1);
  @$pb.TagNumber(2)
  void clearMon() => clearField(2);
  @$pb.TagNumber(2)
  Monster ensureMon() => $_ensure(1);

  @$pb.TagNumber(3)
  $0.ResponseStatus get status => $_getN(2);
  @$pb.TagNumber(3)
  set status($0.ResponseStatus v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
  @$pb.TagNumber(3)
  $0.ResponseStatus ensureStatus() => $_ensure(2);

  @$pb.TagNumber(4)
  MonsterList get monsters => $_getN(3);
  @$pb.TagNumber(4)
  set monsters(MonsterList v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasMonsters() => $_has(3);
  @$pb.TagNumber(4)
  void clearMonsters() => clearField(4);
  @$pb.TagNumber(4)
  MonsterList ensureMonsters() => $_ensure(3);

  @$pb.TagNumber(5)
  $0.MetaData get metaData => $_getN(4);
  @$pb.TagNumber(5)
  set metaData($0.MetaData v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasMetaData() => $_has(4);
  @$pb.TagNumber(5)
  void clearMetaData() => clearField(5);
  @$pb.TagNumber(5)
  $0.MetaData ensureMetaData() => $_ensure(4);
}

