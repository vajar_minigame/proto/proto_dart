///
//  Generated code. Do not modify.
//  source: auth.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'common.pb.dart' as $0;
import 'user.pb.dart' as $1;

class LoginCall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LoginCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'auth'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'username')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'password')
    ..hasRequiredFields = false
  ;

  LoginCall._() : super();
  factory LoginCall({
    $core.String username,
    $core.String password,
  }) {
    final _result = create();
    if (username != null) {
      _result.username = username;
    }
    if (password != null) {
      _result.password = password;
    }
    return _result;
  }
  factory LoginCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LoginCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LoginCall clone() => LoginCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LoginCall copyWith(void Function(LoginCall) updates) => super.copyWith((message) => updates(message as LoginCall)) as LoginCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LoginCall create() => LoginCall._();
  LoginCall createEmptyInstance() => create();
  static $pb.PbList<LoginCall> createRepeated() => $pb.PbList<LoginCall>();
  @$core.pragma('dart2js:noInline')
  static LoginCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LoginCall>(create);
  static LoginCall _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get username => $_getSZ(0);
  @$pb.TagNumber(1)
  set username($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUsername() => $_has(0);
  @$pb.TagNumber(1)
  void clearUsername() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);
}

class LogoutCall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LogoutCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'auth'), createEmptyInstance: create)
    ..aOM<$0.UserId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId', protoName: 'userId', subBuilder: $0.UserId.create)
    ..hasRequiredFields = false
  ;

  LogoutCall._() : super();
  factory LogoutCall({
    $0.UserId userId,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    return _result;
  }
  factory LogoutCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LogoutCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LogoutCall clone() => LogoutCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LogoutCall copyWith(void Function(LogoutCall) updates) => super.copyWith((message) => updates(message as LogoutCall)) as LogoutCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LogoutCall create() => LogoutCall._();
  LogoutCall createEmptyInstance() => create();
  static $pb.PbList<LogoutCall> createRepeated() => $pb.PbList<LogoutCall>();
  @$core.pragma('dart2js:noInline')
  static LogoutCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LogoutCall>(create);
  static LogoutCall _defaultInstance;

  @$pb.TagNumber(1)
  $0.UserId get userId => $_getN(0);
  @$pb.TagNumber(1)
  set userId($0.UserId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);
  @$pb.TagNumber(1)
  $0.UserId ensureUserId() => $_ensure(0);
}

class LoginResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LoginResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'auth'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'token')
    ..aOM<$1.User>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'user', subBuilder: $1.User.create)
    ..hasRequiredFields = false
  ;

  LoginResponse._() : super();
  factory LoginResponse({
    $core.String token,
    $1.User user,
  }) {
    final _result = create();
    if (token != null) {
      _result.token = token;
    }
    if (user != null) {
      _result.user = user;
    }
    return _result;
  }
  factory LoginResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LoginResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LoginResponse clone() => LoginResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LoginResponse copyWith(void Function(LoginResponse) updates) => super.copyWith((message) => updates(message as LoginResponse)) as LoginResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LoginResponse create() => LoginResponse._();
  LoginResponse createEmptyInstance() => create();
  static $pb.PbList<LoginResponse> createRepeated() => $pb.PbList<LoginResponse>();
  @$core.pragma('dart2js:noInline')
  static LoginResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LoginResponse>(create);
  static LoginResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get token => $_getSZ(0);
  @$pb.TagNumber(1)
  set token($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasToken() => $_has(0);
  @$pb.TagNumber(1)
  void clearToken() => clearField(1);

  @$pb.TagNumber(2)
  $1.User get user => $_getN(1);
  @$pb.TagNumber(2)
  set user($1.User v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasUser() => $_has(1);
  @$pb.TagNumber(2)
  void clearUser() => clearField(2);
  @$pb.TagNumber(2)
  $1.User ensureUser() => $_ensure(1);
}

class LogoutResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LogoutResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'auth'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'successful')
    ..hasRequiredFields = false
  ;

  LogoutResponse._() : super();
  factory LogoutResponse({
    $core.bool successful,
  }) {
    final _result = create();
    if (successful != null) {
      _result.successful = successful;
    }
    return _result;
  }
  factory LogoutResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LogoutResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LogoutResponse clone() => LogoutResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LogoutResponse copyWith(void Function(LogoutResponse) updates) => super.copyWith((message) => updates(message as LogoutResponse)) as LogoutResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LogoutResponse create() => LogoutResponse._();
  LogoutResponse createEmptyInstance() => create();
  static $pb.PbList<LogoutResponse> createRepeated() => $pb.PbList<LogoutResponse>();
  @$core.pragma('dart2js:noInline')
  static LogoutResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LogoutResponse>(create);
  static LogoutResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get successful => $_getBF(0);
  @$pb.TagNumber(1)
  set successful($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSuccessful() => $_has(0);
  @$pb.TagNumber(1)
  void clearSuccessful() => clearField(1);
}

enum AuthServiceCall_Message {
  loginCall, 
  logoutCall, 
  notSet
}

class AuthServiceCall extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, AuthServiceCall_Message> _AuthServiceCall_MessageByTag = {
    1 : AuthServiceCall_Message.loginCall,
    2 : AuthServiceCall_Message.logoutCall,
    0 : AuthServiceCall_Message.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AuthServiceCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'auth'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOM<LoginCall>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'loginCall', subBuilder: LoginCall.create)
    ..aOM<LogoutCall>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'logoutCall', subBuilder: LogoutCall.create)
    ..hasRequiredFields = false
  ;

  AuthServiceCall._() : super();
  factory AuthServiceCall({
    LoginCall loginCall,
    LogoutCall logoutCall,
  }) {
    final _result = create();
    if (loginCall != null) {
      _result.loginCall = loginCall;
    }
    if (logoutCall != null) {
      _result.logoutCall = logoutCall;
    }
    return _result;
  }
  factory AuthServiceCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AuthServiceCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AuthServiceCall clone() => AuthServiceCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AuthServiceCall copyWith(void Function(AuthServiceCall) updates) => super.copyWith((message) => updates(message as AuthServiceCall)) as AuthServiceCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthServiceCall create() => AuthServiceCall._();
  AuthServiceCall createEmptyInstance() => create();
  static $pb.PbList<AuthServiceCall> createRepeated() => $pb.PbList<AuthServiceCall>();
  @$core.pragma('dart2js:noInline')
  static AuthServiceCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AuthServiceCall>(create);
  static AuthServiceCall _defaultInstance;

  AuthServiceCall_Message whichMessage() => _AuthServiceCall_MessageByTag[$_whichOneof(0)];
  void clearMessage() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  LoginCall get loginCall => $_getN(0);
  @$pb.TagNumber(1)
  set loginCall(LoginCall v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasLoginCall() => $_has(0);
  @$pb.TagNumber(1)
  void clearLoginCall() => clearField(1);
  @$pb.TagNumber(1)
  LoginCall ensureLoginCall() => $_ensure(0);

  @$pb.TagNumber(2)
  LogoutCall get logoutCall => $_getN(1);
  @$pb.TagNumber(2)
  set logoutCall(LogoutCall v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasLogoutCall() => $_has(1);
  @$pb.TagNumber(2)
  void clearLogoutCall() => clearField(2);
  @$pb.TagNumber(2)
  LogoutCall ensureLogoutCall() => $_ensure(1);
}

enum AuthServiceResponse_Message {
  loginResponse, 
  logoutRespone, 
  notSet
}

class AuthServiceResponse extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, AuthServiceResponse_Message> _AuthServiceResponse_MessageByTag = {
    1 : AuthServiceResponse_Message.loginResponse,
    2 : AuthServiceResponse_Message.logoutRespone,
    0 : AuthServiceResponse_Message.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AuthServiceResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'auth'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOM<LoginResponse>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'loginResponse', subBuilder: LoginResponse.create)
    ..aOM<LogoutResponse>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'logoutRespone', subBuilder: LogoutResponse.create)
    ..hasRequiredFields = false
  ;

  AuthServiceResponse._() : super();
  factory AuthServiceResponse({
    LoginResponse loginResponse,
    LogoutResponse logoutRespone,
  }) {
    final _result = create();
    if (loginResponse != null) {
      _result.loginResponse = loginResponse;
    }
    if (logoutRespone != null) {
      _result.logoutRespone = logoutRespone;
    }
    return _result;
  }
  factory AuthServiceResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AuthServiceResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AuthServiceResponse clone() => AuthServiceResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AuthServiceResponse copyWith(void Function(AuthServiceResponse) updates) => super.copyWith((message) => updates(message as AuthServiceResponse)) as AuthServiceResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthServiceResponse create() => AuthServiceResponse._();
  AuthServiceResponse createEmptyInstance() => create();
  static $pb.PbList<AuthServiceResponse> createRepeated() => $pb.PbList<AuthServiceResponse>();
  @$core.pragma('dart2js:noInline')
  static AuthServiceResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AuthServiceResponse>(create);
  static AuthServiceResponse _defaultInstance;

  AuthServiceResponse_Message whichMessage() => _AuthServiceResponse_MessageByTag[$_whichOneof(0)];
  void clearMessage() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  LoginResponse get loginResponse => $_getN(0);
  @$pb.TagNumber(1)
  set loginResponse(LoginResponse v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasLoginResponse() => $_has(0);
  @$pb.TagNumber(1)
  void clearLoginResponse() => clearField(1);
  @$pb.TagNumber(1)
  LoginResponse ensureLoginResponse() => $_ensure(0);

  @$pb.TagNumber(2)
  LogoutResponse get logoutRespone => $_getN(1);
  @$pb.TagNumber(2)
  set logoutRespone(LogoutResponse v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasLogoutRespone() => $_has(1);
  @$pb.TagNumber(2)
  void clearLogoutRespone() => clearField(2);
  @$pb.TagNumber(2)
  LogoutResponse ensureLogoutRespone() => $_ensure(1);
}

