///
//  Generated code. Do not modify.
//  source: monster.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'monster.pb.dart' as $3;
import 'common.pb.dart' as $0;
export 'monster.pb.dart';

class MonsterServicesClient extends $grpc.Client {
  static final _$addMon = $grpc.ClientMethod<$3.Monster, $0.MonId>(
      '/monster.MonsterServices/AddMon',
      ($3.Monster value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.MonId.fromBuffer(value));
  static final _$addTestMon = $grpc.ClientMethod<$0.UserId, $3.Monster>(
      '/monster.MonsterServices/AddTestMon',
      ($0.UserId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.Monster.fromBuffer(value));
  static final _$lockMonster = $grpc.ClientMethod<$3.LockRequest, $3.Monster>(
      '/monster.MonsterServices/LockMonster',
      ($3.LockRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.Monster.fromBuffer(value));
  static final _$releaseMonster =
      $grpc.ClientMethod<$3.ReleaseRequest, $0.ResponseStatus>(
          '/monster.MonsterServices/ReleaseMonster',
          ($3.ReleaseRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));
  static final _$getMonByID = $grpc.ClientMethod<$0.MonId, $3.Monster>(
      '/monster.MonsterServices/GetMonByID',
      ($0.MonId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.Monster.fromBuffer(value));
  static final _$getMonsByUserID =
      $grpc.ClientMethod<$0.UserId, $3.MonsterList>(
          '/monster.MonsterServices/GetMonsByUserID',
          ($0.UserId value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $3.MonsterList.fromBuffer(value));
  static final _$updateMon = $grpc.ClientMethod<$3.Monster, $3.Monster>(
      '/monster.MonsterServices/UpdateMon',
      ($3.Monster value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.Monster.fromBuffer(value));
  static final _$deleteMon = $grpc.ClientMethod<$0.MonId, $0.ResponseStatus>(
      '/monster.MonsterServices/DeleteMon',
      ($0.MonId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));

  MonsterServicesClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options,
      $core.Iterable<$grpc.ClientInterceptor> interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.MonId> addMon($3.Monster request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$addMon, request, options: options);
  }

  $grpc.ResponseFuture<$3.Monster> addTestMon($0.UserId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$addTestMon, request, options: options);
  }

  $grpc.ResponseFuture<$3.Monster> lockMonster($3.LockRequest request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$lockMonster, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResponseStatus> releaseMonster(
      $3.ReleaseRequest request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$releaseMonster, request, options: options);
  }

  $grpc.ResponseFuture<$3.Monster> getMonByID($0.MonId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getMonByID, request, options: options);
  }

  $grpc.ResponseFuture<$3.MonsterList> getMonsByUserID($0.UserId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getMonsByUserID, request, options: options);
  }

  $grpc.ResponseFuture<$3.Monster> updateMon($3.Monster request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$updateMon, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResponseStatus> deleteMon($0.MonId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$deleteMon, request, options: options);
  }
}

abstract class MonsterServicesServiceBase extends $grpc.Service {
  $core.String get $name => 'monster.MonsterServices';

  MonsterServicesServiceBase() {
    $addMethod($grpc.ServiceMethod<$3.Monster, $0.MonId>(
        'AddMon',
        addMon_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.Monster.fromBuffer(value),
        ($0.MonId value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserId, $3.Monster>(
        'AddTestMon',
        addTestMon_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserId.fromBuffer(value),
        ($3.Monster value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.LockRequest, $3.Monster>(
        'LockMonster',
        lockMonster_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.LockRequest.fromBuffer(value),
        ($3.Monster value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.ReleaseRequest, $0.ResponseStatus>(
        'ReleaseMonster',
        releaseMonster_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.ReleaseRequest.fromBuffer(value),
        ($0.ResponseStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MonId, $3.Monster>(
        'GetMonByID',
        getMonByID_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.MonId.fromBuffer(value),
        ($3.Monster value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserId, $3.MonsterList>(
        'GetMonsByUserID',
        getMonsByUserID_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserId.fromBuffer(value),
        ($3.MonsterList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.Monster, $3.Monster>(
        'UpdateMon',
        updateMon_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.Monster.fromBuffer(value),
        ($3.Monster value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MonId, $0.ResponseStatus>(
        'DeleteMon',
        deleteMon_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.MonId.fromBuffer(value),
        ($0.ResponseStatus value) => value.writeToBuffer()));
  }

  $async.Future<$0.MonId> addMon_Pre(
      $grpc.ServiceCall call, $async.Future<$3.Monster> request) async {
    return addMon(call, await request);
  }

  $async.Future<$3.Monster> addTestMon_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserId> request) async {
    return addTestMon(call, await request);
  }

  $async.Future<$3.Monster> lockMonster_Pre(
      $grpc.ServiceCall call, $async.Future<$3.LockRequest> request) async {
    return lockMonster(call, await request);
  }

  $async.Future<$0.ResponseStatus> releaseMonster_Pre(
      $grpc.ServiceCall call, $async.Future<$3.ReleaseRequest> request) async {
    return releaseMonster(call, await request);
  }

  $async.Future<$3.Monster> getMonByID_Pre(
      $grpc.ServiceCall call, $async.Future<$0.MonId> request) async {
    return getMonByID(call, await request);
  }

  $async.Future<$3.MonsterList> getMonsByUserID_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserId> request) async {
    return getMonsByUserID(call, await request);
  }

  $async.Future<$3.Monster> updateMon_Pre(
      $grpc.ServiceCall call, $async.Future<$3.Monster> request) async {
    return updateMon(call, await request);
  }

  $async.Future<$0.ResponseStatus> deleteMon_Pre(
      $grpc.ServiceCall call, $async.Future<$0.MonId> request) async {
    return deleteMon(call, await request);
  }

  $async.Future<$0.MonId> addMon($grpc.ServiceCall call, $3.Monster request);
  $async.Future<$3.Monster> addTestMon(
      $grpc.ServiceCall call, $0.UserId request);
  $async.Future<$3.Monster> lockMonster(
      $grpc.ServiceCall call, $3.LockRequest request);
  $async.Future<$0.ResponseStatus> releaseMonster(
      $grpc.ServiceCall call, $3.ReleaseRequest request);
  $async.Future<$3.Monster> getMonByID(
      $grpc.ServiceCall call, $0.MonId request);
  $async.Future<$3.MonsterList> getMonsByUserID(
      $grpc.ServiceCall call, $0.UserId request);
  $async.Future<$3.Monster> updateMon(
      $grpc.ServiceCall call, $3.Monster request);
  $async.Future<$0.ResponseStatus> deleteMon(
      $grpc.ServiceCall call, $0.MonId request);
}
