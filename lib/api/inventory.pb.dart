///
//  Generated code. Do not modify.
//  source: inventory.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'common.pb.dart' as $0;

class Inventory extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Inventory', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOM<$0.UserId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', subBuilder: $0.UserId.create)
    ..pc<Item>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: Item.create)
    ..hasRequiredFields = false
  ;

  Inventory._() : super();
  factory Inventory({
    $0.UserId id,
    $core.Iterable<Item> items,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory Inventory.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Inventory.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Inventory clone() => Inventory()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Inventory copyWith(void Function(Inventory) updates) => super.copyWith((message) => updates(message as Inventory)) as Inventory; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Inventory create() => Inventory._();
  Inventory createEmptyInstance() => create();
  static $pb.PbList<Inventory> createRepeated() => $pb.PbList<Inventory>();
  @$core.pragma('dart2js:noInline')
  static Inventory getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Inventory>(create);
  static Inventory _defaultInstance;

  @$pb.TagNumber(1)
  $0.UserId get id => $_getN(0);
  @$pb.TagNumber(1)
  set id($0.UserId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
  @$pb.TagNumber(1)
  $0.UserId ensureId() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<Item> get items => $_getList(1);
}

class ItemList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..pc<Item>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: Item.create)
    ..hasRequiredFields = false
  ;

  ItemList._() : super();
  factory ItemList({
    $core.Iterable<Item> items,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory ItemList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemList clone() => ItemList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemList copyWith(void Function(ItemList) updates) => super.copyWith((message) => updates(message as ItemList)) as ItemList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemList create() => ItemList._();
  ItemList createEmptyInstance() => create();
  static $pb.PbList<ItemList> createRepeated() => $pb.PbList<ItemList>();
  @$core.pragma('dart2js:noInline')
  static ItemList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemList>(create);
  static ItemList _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Item> get items => $_getList(0);
}

class ItemDefList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemDefList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..pc<ItemDefinition>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemDefs', $pb.PbFieldType.PM, protoName: 'itemDefs', subBuilder: ItemDefinition.create)
    ..hasRequiredFields = false
  ;

  ItemDefList._() : super();
  factory ItemDefList({
    $core.Iterable<ItemDefinition> itemDefs,
  }) {
    final _result = create();
    if (itemDefs != null) {
      _result.itemDefs.addAll(itemDefs);
    }
    return _result;
  }
  factory ItemDefList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemDefList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemDefList clone() => ItemDefList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemDefList copyWith(void Function(ItemDefList) updates) => super.copyWith((message) => updates(message as ItemDefList)) as ItemDefList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemDefList create() => ItemDefList._();
  ItemDefList createEmptyInstance() => create();
  static $pb.PbList<ItemDefList> createRepeated() => $pb.PbList<ItemDefList>();
  @$core.pragma('dart2js:noInline')
  static ItemDefList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemDefList>(create);
  static ItemDefList _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<ItemDefinition> get itemDefs => $_getList(0);
}

class ItemIdList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemIdList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..pc<$0.ItemId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemIds', $pb.PbFieldType.PM, protoName: 'itemIds', subBuilder: $0.ItemId.create)
    ..hasRequiredFields = false
  ;

  ItemIdList._() : super();
  factory ItemIdList({
    $core.Iterable<$0.ItemId> itemIds,
  }) {
    final _result = create();
    if (itemIds != null) {
      _result.itemIds.addAll(itemIds);
    }
    return _result;
  }
  factory ItemIdList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemIdList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemIdList clone() => ItemIdList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemIdList copyWith(void Function(ItemIdList) updates) => super.copyWith((message) => updates(message as ItemIdList)) as ItemIdList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemIdList create() => ItemIdList._();
  ItemIdList createEmptyInstance() => create();
  static $pb.PbList<ItemIdList> createRepeated() => $pb.PbList<ItemIdList>();
  @$core.pragma('dart2js:noInline')
  static ItemIdList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemIdList>(create);
  static ItemIdList _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$0.ItemId> get itemIds => $_getList(0);
}

enum ItemDefinition_ItemContent {
  food, 
  notSet
}

class ItemDefinition extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, ItemDefinition_ItemContent> _ItemDefinition_ItemContentByTag = {
    4 : ItemDefinition_ItemContent.food,
    0 : ItemDefinition_ItemContent.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemDefinition', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..oo(0, [4])
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type')
    ..pPS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tags')
    ..aOM<Food>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'food', subBuilder: Food.create)
    ..hasRequiredFields = false
  ;

  ItemDefinition._() : super();
  factory ItemDefinition({
    $core.String type,
    $core.Iterable<$core.String> tags,
    Food food,
  }) {
    final _result = create();
    if (type != null) {
      _result.type = type;
    }
    if (tags != null) {
      _result.tags.addAll(tags);
    }
    if (food != null) {
      _result.food = food;
    }
    return _result;
  }
  factory ItemDefinition.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemDefinition.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemDefinition clone() => ItemDefinition()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemDefinition copyWith(void Function(ItemDefinition) updates) => super.copyWith((message) => updates(message as ItemDefinition)) as ItemDefinition; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemDefinition create() => ItemDefinition._();
  ItemDefinition createEmptyInstance() => create();
  static $pb.PbList<ItemDefinition> createRepeated() => $pb.PbList<ItemDefinition>();
  @$core.pragma('dart2js:noInline')
  static ItemDefinition getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemDefinition>(create);
  static ItemDefinition _defaultInstance;

  ItemDefinition_ItemContent whichItemContent() => _ItemDefinition_ItemContentByTag[$_whichOneof(0)];
  void clearItemContent() => clearField($_whichOneof(0));

  @$pb.TagNumber(2)
  $core.String get type => $_getSZ(0);
  @$pb.TagNumber(2)
  set type($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(2)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(2)
  void clearType() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.String> get tags => $_getList(1);

  @$pb.TagNumber(4)
  Food get food => $_getN(2);
  @$pb.TagNumber(4)
  set food(Food v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasFood() => $_has(2);
  @$pb.TagNumber(4)
  void clearFood() => clearField(4);
  @$pb.TagNumber(4)
  Food ensureFood() => $_ensure(2);
}

class Item extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Item', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', $pb.PbFieldType.O3)
    ..aOM<ItemDefinition>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemDef', protoName: 'itemDef', subBuilder: ItemDefinition.create)
    ..hasRequiredFields = false
  ;

  Item._() : super();
  factory Item({
    $core.int id,
    ItemDefinition itemDef,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (itemDef != null) {
      _result.itemDef = itemDef;
    }
    return _result;
  }
  factory Item.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Item.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Item clone() => Item()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Item copyWith(void Function(Item) updates) => super.copyWith((message) => updates(message as Item)) as Item; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Item create() => Item._();
  Item createEmptyInstance() => create();
  static $pb.PbList<Item> createRepeated() => $pb.PbList<Item>();
  @$core.pragma('dart2js:noInline')
  static Item getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Item>(create);
  static Item _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get id => $_getIZ(0);
  @$pb.TagNumber(1)
  set id($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  ItemDefinition get itemDef => $_getN(1);
  @$pb.TagNumber(2)
  set itemDef(ItemDefinition v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasItemDef() => $_has(1);
  @$pb.TagNumber(2)
  void clearItemDef() => clearField(2);
  @$pb.TagNumber(2)
  ItemDefinition ensureItemDef() => $_ensure(1);
}

enum Effect_Effect {
  buff, 
  rec, 
  notSet
}

class Effect extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Effect_Effect> _Effect_EffectByTag = {
    1 : Effect_Effect.buff,
    2 : Effect_Effect.rec,
    0 : Effect_Effect.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Effect', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOM<Buff>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buff', subBuilder: Buff.create)
    ..aOM<Recovery>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rec', subBuilder: Recovery.create)
    ..hasRequiredFields = false
  ;

  Effect._() : super();
  factory Effect({
    Buff buff,
    Recovery rec,
  }) {
    final _result = create();
    if (buff != null) {
      _result.buff = buff;
    }
    if (rec != null) {
      _result.rec = rec;
    }
    return _result;
  }
  factory Effect.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Effect.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Effect clone() => Effect()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Effect copyWith(void Function(Effect) updates) => super.copyWith((message) => updates(message as Effect)) as Effect; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Effect create() => Effect._();
  Effect createEmptyInstance() => create();
  static $pb.PbList<Effect> createRepeated() => $pb.PbList<Effect>();
  @$core.pragma('dart2js:noInline')
  static Effect getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Effect>(create);
  static Effect _defaultInstance;

  Effect_Effect whichEffect() => _Effect_EffectByTag[$_whichOneof(0)];
  void clearEffect() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  Buff get buff => $_getN(0);
  @$pb.TagNumber(1)
  set buff(Buff v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasBuff() => $_has(0);
  @$pb.TagNumber(1)
  void clearBuff() => clearField(1);
  @$pb.TagNumber(1)
  Buff ensureBuff() => $_ensure(0);

  @$pb.TagNumber(2)
  Recovery get rec => $_getN(1);
  @$pb.TagNumber(2)
  set rec(Recovery v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasRec() => $_has(1);
  @$pb.TagNumber(2)
  void clearRec() => clearField(2);
  @$pb.TagNumber(2)
  Recovery ensureRec() => $_ensure(1);
}

class Buff extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Buff', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  Buff._() : super();
  factory Buff({
    $core.String type,
    $core.double value,
  }) {
    final _result = create();
    if (type != null) {
      _result.type = type;
    }
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory Buff.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Buff.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Buff clone() => Buff()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Buff copyWith(void Function(Buff) updates) => super.copyWith((message) => updates(message as Buff)) as Buff; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Buff create() => Buff._();
  Buff createEmptyInstance() => create();
  static $pb.PbList<Buff> createRepeated() => $pb.PbList<Buff>();
  @$core.pragma('dart2js:noInline')
  static Buff getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Buff>(create);
  static Buff _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get type => $_getSZ(0);
  @$pb.TagNumber(1)
  set type($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(1)
  void clearType() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get value => $_getN(1);
  @$pb.TagNumber(2)
  set value($core.double v) { $_setFloat(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearValue() => clearField(2);
}

class Recovery extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Recovery', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'value', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  Recovery._() : super();
  factory Recovery({
    $core.String type,
    $core.double value,
  }) {
    final _result = create();
    if (type != null) {
      _result.type = type;
    }
    if (value != null) {
      _result.value = value;
    }
    return _result;
  }
  factory Recovery.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Recovery.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Recovery clone() => Recovery()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Recovery copyWith(void Function(Recovery) updates) => super.copyWith((message) => updates(message as Recovery)) as Recovery; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Recovery create() => Recovery._();
  Recovery createEmptyInstance() => create();
  static $pb.PbList<Recovery> createRepeated() => $pb.PbList<Recovery>();
  @$core.pragma('dart2js:noInline')
  static Recovery getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Recovery>(create);
  static Recovery _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get type => $_getSZ(0);
  @$pb.TagNumber(1)
  set type($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(1)
  void clearType() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get value => $_getN(1);
  @$pb.TagNumber(2)
  set value($core.double v) { $_setFloat(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearValue() => clearField(2);
}

class Page extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Page', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'size', $pb.PbFieldType.O3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'count', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Page._() : super();
  factory Page({
    $core.int size,
    $core.int count,
  }) {
    final _result = create();
    if (size != null) {
      _result.size = size;
    }
    if (count != null) {
      _result.count = count;
    }
    return _result;
  }
  factory Page.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Page.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Page clone() => Page()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Page copyWith(void Function(Page) updates) => super.copyWith((message) => updates(message as Page)) as Page; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Page create() => Page._();
  Page createEmptyInstance() => create();
  static $pb.PbList<Page> createRepeated() => $pb.PbList<Page>();
  @$core.pragma('dart2js:noInline')
  static Page getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Page>(create);
  static Page _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get size => $_getIZ(0);
  @$pb.TagNumber(1)
  set size($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSize() => $_has(0);
  @$pb.TagNumber(1)
  void clearSize() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get count => $_getIZ(1);
  @$pb.TagNumber(2)
  set count($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCount() => $_has(1);
  @$pb.TagNumber(2)
  void clearCount() => clearField(2);
}

class Food extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Food', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..pc<Effect>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'effects', $pb.PbFieldType.PM, subBuilder: Effect.create)
    ..hasRequiredFields = false
  ;

  Food._() : super();
  factory Food({
    $core.Iterable<Effect> effects,
  }) {
    final _result = create();
    if (effects != null) {
      _result.effects.addAll(effects);
    }
    return _result;
  }
  factory Food.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Food.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Food clone() => Food()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Food copyWith(void Function(Food) updates) => super.copyWith((message) => updates(message as Food)) as Food; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Food create() => Food._();
  Food createEmptyInstance() => create();
  static $pb.PbList<Food> createRepeated() => $pb.PbList<Food>();
  @$core.pragma('dart2js:noInline')
  static Food getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Food>(create);
  static Food _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Effect> get effects => $_getList(0);
}

class ItemType extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemType', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type')
    ..hasRequiredFields = false
  ;

  ItemType._() : super();
  factory ItemType({
    $core.String type,
  }) {
    final _result = create();
    if (type != null) {
      _result.type = type;
    }
    return _result;
  }
  factory ItemType.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemType.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemType clone() => ItemType()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemType copyWith(void Function(ItemType) updates) => super.copyWith((message) => updates(message as ItemType)) as ItemType; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemType create() => ItemType._();
  ItemType createEmptyInstance() => create();
  static $pb.PbList<ItemType> createRepeated() => $pb.PbList<ItemType>();
  @$core.pragma('dart2js:noInline')
  static ItemType getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemType>(create);
  static ItemType _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get type => $_getSZ(0);
  @$pb.TagNumber(1)
  set type($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(1)
  void clearType() => clearField(1);
}

class AddItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AddItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOM<Item>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'item', subBuilder: Item.create)
    ..hasRequiredFields = false
  ;

  AddItem._() : super();
  factory AddItem({
    Item item,
  }) {
    final _result = create();
    if (item != null) {
      _result.item = item;
    }
    return _result;
  }
  factory AddItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AddItem clone() => AddItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AddItem copyWith(void Function(AddItem) updates) => super.copyWith((message) => updates(message as AddItem)) as AddItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddItem create() => AddItem._();
  AddItem createEmptyInstance() => create();
  static $pb.PbList<AddItem> createRepeated() => $pb.PbList<AddItem>();
  @$core.pragma('dart2js:noInline')
  static AddItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddItem>(create);
  static AddItem _defaultInstance;

  @$pb.TagNumber(1)
  Item get item => $_getN(0);
  @$pb.TagNumber(1)
  set item(Item v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasItem() => $_has(0);
  @$pb.TagNumber(1)
  void clearItem() => clearField(1);
  @$pb.TagNumber(1)
  Item ensureItem() => $_ensure(0);
}

class GetItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOM<$0.ItemId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', subBuilder: $0.ItemId.create)
    ..hasRequiredFields = false
  ;

  GetItem._() : super();
  factory GetItem({
    $0.ItemId id,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    return _result;
  }
  factory GetItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetItem clone() => GetItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetItem copyWith(void Function(GetItem) updates) => super.copyWith((message) => updates(message as GetItem)) as GetItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetItem create() => GetItem._();
  GetItem createEmptyInstance() => create();
  static $pb.PbList<GetItem> createRepeated() => $pb.PbList<GetItem>();
  @$core.pragma('dart2js:noInline')
  static GetItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetItem>(create);
  static GetItem _defaultInstance;

  @$pb.TagNumber(1)
  $0.ItemId get id => $_getN(0);
  @$pb.TagNumber(1)
  set id($0.ItemId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
  @$pb.TagNumber(1)
  $0.ItemId ensureId() => $_ensure(0);
}

class GetInventory extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetInventory', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOM<$0.UserId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId', protoName: 'userId', subBuilder: $0.UserId.create)
    ..hasRequiredFields = false
  ;

  GetInventory._() : super();
  factory GetInventory({
    $0.UserId userId,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    return _result;
  }
  factory GetInventory.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetInventory.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetInventory clone() => GetInventory()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetInventory copyWith(void Function(GetInventory) updates) => super.copyWith((message) => updates(message as GetInventory)) as GetInventory; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetInventory create() => GetInventory._();
  GetInventory createEmptyInstance() => create();
  static $pb.PbList<GetInventory> createRepeated() => $pb.PbList<GetInventory>();
  @$core.pragma('dart2js:noInline')
  static GetInventory getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetInventory>(create);
  static GetInventory _defaultInstance;

  @$pb.TagNumber(1)
  $0.UserId get userId => $_getN(0);
  @$pb.TagNumber(1)
  set userId($0.UserId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);
  @$pb.TagNumber(1)
  $0.UserId ensureUserId() => $_ensure(0);
}

class AddItemToInventoryCall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AddItemToInventoryCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOM<$0.UserId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId', protoName: 'userId', subBuilder: $0.UserId.create)
    ..aOM<$0.ItemId>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemId', protoName: 'itemId', subBuilder: $0.ItemId.create)
    ..hasRequiredFields = false
  ;

  AddItemToInventoryCall._() : super();
  factory AddItemToInventoryCall({
    $0.UserId userId,
    $0.ItemId itemId,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (itemId != null) {
      _result.itemId = itemId;
    }
    return _result;
  }
  factory AddItemToInventoryCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddItemToInventoryCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AddItemToInventoryCall clone() => AddItemToInventoryCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AddItemToInventoryCall copyWith(void Function(AddItemToInventoryCall) updates) => super.copyWith((message) => updates(message as AddItemToInventoryCall)) as AddItemToInventoryCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddItemToInventoryCall create() => AddItemToInventoryCall._();
  AddItemToInventoryCall createEmptyInstance() => create();
  static $pb.PbList<AddItemToInventoryCall> createRepeated() => $pb.PbList<AddItemToInventoryCall>();
  @$core.pragma('dart2js:noInline')
  static AddItemToInventoryCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddItemToInventoryCall>(create);
  static AddItemToInventoryCall _defaultInstance;

  @$pb.TagNumber(1)
  $0.UserId get userId => $_getN(0);
  @$pb.TagNumber(1)
  set userId($0.UserId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);
  @$pb.TagNumber(1)
  $0.UserId ensureUserId() => $_ensure(0);

  @$pb.TagNumber(2)
  $0.ItemId get itemId => $_getN(1);
  @$pb.TagNumber(2)
  set itemId($0.ItemId v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasItemId() => $_has(1);
  @$pb.TagNumber(2)
  void clearItemId() => clearField(2);
  @$pb.TagNumber(2)
  $0.ItemId ensureItemId() => $_ensure(1);
}

class UpdateItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpdateItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOM<Item>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'item', subBuilder: Item.create)
    ..hasRequiredFields = false
  ;

  UpdateItem._() : super();
  factory UpdateItem({
    Item item,
  }) {
    final _result = create();
    if (item != null) {
      _result.item = item;
    }
    return _result;
  }
  factory UpdateItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpdateItem clone() => UpdateItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpdateItem copyWith(void Function(UpdateItem) updates) => super.copyWith((message) => updates(message as UpdateItem)) as UpdateItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateItem create() => UpdateItem._();
  UpdateItem createEmptyInstance() => create();
  static $pb.PbList<UpdateItem> createRepeated() => $pb.PbList<UpdateItem>();
  @$core.pragma('dart2js:noInline')
  static UpdateItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateItem>(create);
  static UpdateItem _defaultInstance;

  @$pb.TagNumber(1)
  Item get item => $_getN(0);
  @$pb.TagNumber(1)
  set item(Item v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasItem() => $_has(0);
  @$pb.TagNumber(1)
  void clearItem() => clearField(1);
  @$pb.TagNumber(1)
  Item ensureItem() => $_ensure(0);
}

class DeleteItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DeleteItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOM<$0.ItemId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', subBuilder: $0.ItemId.create)
    ..hasRequiredFields = false
  ;

  DeleteItem._() : super();
  factory DeleteItem({
    $0.ItemId id,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    return _result;
  }
  factory DeleteItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeleteItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DeleteItem clone() => DeleteItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DeleteItem copyWith(void Function(DeleteItem) updates) => super.copyWith((message) => updates(message as DeleteItem)) as DeleteItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeleteItem create() => DeleteItem._();
  DeleteItem createEmptyInstance() => create();
  static $pb.PbList<DeleteItem> createRepeated() => $pb.PbList<DeleteItem>();
  @$core.pragma('dart2js:noInline')
  static DeleteItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeleteItem>(create);
  static DeleteItem _defaultInstance;

  @$pb.TagNumber(1)
  $0.ItemId get id => $_getN(0);
  @$pb.TagNumber(1)
  set id($0.ItemId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
  @$pb.TagNumber(1)
  $0.ItemId ensureId() => $_ensure(0);
}

class ItemDeletedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemDeletedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOM<$0.ItemId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemId', protoName: 'itemId', subBuilder: $0.ItemId.create)
    ..hasRequiredFields = false
  ;

  ItemDeletedEvent._() : super();
  factory ItemDeletedEvent({
    $0.ItemId itemId,
  }) {
    final _result = create();
    if (itemId != null) {
      _result.itemId = itemId;
    }
    return _result;
  }
  factory ItemDeletedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemDeletedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemDeletedEvent clone() => ItemDeletedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemDeletedEvent copyWith(void Function(ItemDeletedEvent) updates) => super.copyWith((message) => updates(message as ItemDeletedEvent)) as ItemDeletedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemDeletedEvent create() => ItemDeletedEvent._();
  ItemDeletedEvent createEmptyInstance() => create();
  static $pb.PbList<ItemDeletedEvent> createRepeated() => $pb.PbList<ItemDeletedEvent>();
  @$core.pragma('dart2js:noInline')
  static ItemDeletedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemDeletedEvent>(create);
  static ItemDeletedEvent _defaultInstance;

  @$pb.TagNumber(1)
  $0.ItemId get itemId => $_getN(0);
  @$pb.TagNumber(1)
  set itemId($0.ItemId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasItemId() => $_has(0);
  @$pb.TagNumber(1)
  void clearItemId() => clearField(1);
  @$pb.TagNumber(1)
  $0.ItemId ensureItemId() => $_ensure(0);
}

class ItemAddedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemAddedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOM<Item>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'item', subBuilder: Item.create)
    ..hasRequiredFields = false
  ;

  ItemAddedEvent._() : super();
  factory ItemAddedEvent({
    Item item,
  }) {
    final _result = create();
    if (item != null) {
      _result.item = item;
    }
    return _result;
  }
  factory ItemAddedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemAddedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemAddedEvent clone() => ItemAddedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemAddedEvent copyWith(void Function(ItemAddedEvent) updates) => super.copyWith((message) => updates(message as ItemAddedEvent)) as ItemAddedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemAddedEvent create() => ItemAddedEvent._();
  ItemAddedEvent createEmptyInstance() => create();
  static $pb.PbList<ItemAddedEvent> createRepeated() => $pb.PbList<ItemAddedEvent>();
  @$core.pragma('dart2js:noInline')
  static ItemAddedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemAddedEvent>(create);
  static ItemAddedEvent _defaultInstance;

  @$pb.TagNumber(1)
  Item get item => $_getN(0);
  @$pb.TagNumber(1)
  set item(Item v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasItem() => $_has(0);
  @$pb.TagNumber(1)
  void clearItem() => clearField(1);
  @$pb.TagNumber(1)
  Item ensureItem() => $_ensure(0);
}

class ItemUpdatedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemUpdatedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..aOM<Item>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'item', subBuilder: Item.create)
    ..hasRequiredFields = false
  ;

  ItemUpdatedEvent._() : super();
  factory ItemUpdatedEvent({
    Item item,
  }) {
    final _result = create();
    if (item != null) {
      _result.item = item;
    }
    return _result;
  }
  factory ItemUpdatedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemUpdatedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemUpdatedEvent clone() => ItemUpdatedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemUpdatedEvent copyWith(void Function(ItemUpdatedEvent) updates) => super.copyWith((message) => updates(message as ItemUpdatedEvent)) as ItemUpdatedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemUpdatedEvent create() => ItemUpdatedEvent._();
  ItemUpdatedEvent createEmptyInstance() => create();
  static $pb.PbList<ItemUpdatedEvent> createRepeated() => $pb.PbList<ItemUpdatedEvent>();
  @$core.pragma('dart2js:noInline')
  static ItemUpdatedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemUpdatedEvent>(create);
  static ItemUpdatedEvent _defaultInstance;

  @$pb.TagNumber(1)
  Item get item => $_getN(0);
  @$pb.TagNumber(1)
  set item(Item v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasItem() => $_has(0);
  @$pb.TagNumber(1)
  void clearItem() => clearField(1);
  @$pb.TagNumber(1)
  Item ensureItem() => $_ensure(0);
}

enum ItemEvent_Event {
  addedEvent, 
  deleteEvent, 
  updateEvent, 
  notSet
}

class ItemEvent extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, ItemEvent_Event> _ItemEvent_EventByTag = {
    1 : ItemEvent_Event.addedEvent,
    2 : ItemEvent_Event.deleteEvent,
    3 : ItemEvent_Event.updateEvent,
    0 : ItemEvent_Event.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3])
    ..aOM<ItemAddedEvent>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'addedEvent', protoName: 'addedEvent', subBuilder: ItemAddedEvent.create)
    ..aOM<ItemDeletedEvent>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deleteEvent', protoName: 'deleteEvent', subBuilder: ItemDeletedEvent.create)
    ..aOM<ItemUpdatedEvent>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updateEvent', protoName: 'updateEvent', subBuilder: ItemUpdatedEvent.create)
    ..hasRequiredFields = false
  ;

  ItemEvent._() : super();
  factory ItemEvent({
    ItemAddedEvent addedEvent,
    ItemDeletedEvent deleteEvent,
    ItemUpdatedEvent updateEvent,
  }) {
    final _result = create();
    if (addedEvent != null) {
      _result.addedEvent = addedEvent;
    }
    if (deleteEvent != null) {
      _result.deleteEvent = deleteEvent;
    }
    if (updateEvent != null) {
      _result.updateEvent = updateEvent;
    }
    return _result;
  }
  factory ItemEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemEvent clone() => ItemEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemEvent copyWith(void Function(ItemEvent) updates) => super.copyWith((message) => updates(message as ItemEvent)) as ItemEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemEvent create() => ItemEvent._();
  ItemEvent createEmptyInstance() => create();
  static $pb.PbList<ItemEvent> createRepeated() => $pb.PbList<ItemEvent>();
  @$core.pragma('dart2js:noInline')
  static ItemEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemEvent>(create);
  static ItemEvent _defaultInstance;

  ItemEvent_Event whichEvent() => _ItemEvent_EventByTag[$_whichOneof(0)];
  void clearEvent() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  ItemAddedEvent get addedEvent => $_getN(0);
  @$pb.TagNumber(1)
  set addedEvent(ItemAddedEvent v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasAddedEvent() => $_has(0);
  @$pb.TagNumber(1)
  void clearAddedEvent() => clearField(1);
  @$pb.TagNumber(1)
  ItemAddedEvent ensureAddedEvent() => $_ensure(0);

  @$pb.TagNumber(2)
  ItemDeletedEvent get deleteEvent => $_getN(1);
  @$pb.TagNumber(2)
  set deleteEvent(ItemDeletedEvent v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasDeleteEvent() => $_has(1);
  @$pb.TagNumber(2)
  void clearDeleteEvent() => clearField(2);
  @$pb.TagNumber(2)
  ItemDeletedEvent ensureDeleteEvent() => $_ensure(1);

  @$pb.TagNumber(3)
  ItemUpdatedEvent get updateEvent => $_getN(2);
  @$pb.TagNumber(3)
  set updateEvent(ItemUpdatedEvent v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasUpdateEvent() => $_has(2);
  @$pb.TagNumber(3)
  void clearUpdateEvent() => clearField(3);
  @$pb.TagNumber(3)
  ItemUpdatedEvent ensureUpdateEvent() => $_ensure(2);
}

enum InventoryServiceCall_Method {
  addItem, 
  getItem, 
  getInventory, 
  updateItem, 
  deleteItem, 
  notSet
}

class InventoryServiceCall extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, InventoryServiceCall_Method> _InventoryServiceCall_MethodByTag = {
    1 : InventoryServiceCall_Method.addItem,
    2 : InventoryServiceCall_Method.getItem,
    3 : InventoryServiceCall_Method.getInventory,
    4 : InventoryServiceCall_Method.updateItem,
    5 : InventoryServiceCall_Method.deleteItem,
    0 : InventoryServiceCall_Method.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InventoryServiceCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4, 5])
    ..aOM<AddItem>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'addItem', protoName: 'addItem', subBuilder: AddItem.create)
    ..aOM<GetItem>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'getItem', protoName: 'getItem', subBuilder: GetItem.create)
    ..aOM<GetInventory>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'getInventory', protoName: 'getInventory', subBuilder: GetInventory.create)
    ..aOM<UpdateItem>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'updateItem', protoName: 'updateItem', subBuilder: UpdateItem.create)
    ..aOM<DeleteItem>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deleteItem', protoName: 'deleteItem', subBuilder: DeleteItem.create)
    ..aOM<$0.MetaData>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'metaData', protoName: 'metaData', subBuilder: $0.MetaData.create)
    ..hasRequiredFields = false
  ;

  InventoryServiceCall._() : super();
  factory InventoryServiceCall({
    AddItem addItem,
    GetItem getItem,
    GetInventory getInventory,
    UpdateItem updateItem,
    DeleteItem deleteItem,
    $0.MetaData metaData,
  }) {
    final _result = create();
    if (addItem != null) {
      _result.addItem = addItem;
    }
    if (getItem != null) {
      _result.getItem = getItem;
    }
    if (getInventory != null) {
      _result.getInventory = getInventory;
    }
    if (updateItem != null) {
      _result.updateItem = updateItem;
    }
    if (deleteItem != null) {
      _result.deleteItem = deleteItem;
    }
    if (metaData != null) {
      _result.metaData = metaData;
    }
    return _result;
  }
  factory InventoryServiceCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InventoryServiceCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InventoryServiceCall clone() => InventoryServiceCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InventoryServiceCall copyWith(void Function(InventoryServiceCall) updates) => super.copyWith((message) => updates(message as InventoryServiceCall)) as InventoryServiceCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InventoryServiceCall create() => InventoryServiceCall._();
  InventoryServiceCall createEmptyInstance() => create();
  static $pb.PbList<InventoryServiceCall> createRepeated() => $pb.PbList<InventoryServiceCall>();
  @$core.pragma('dart2js:noInline')
  static InventoryServiceCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InventoryServiceCall>(create);
  static InventoryServiceCall _defaultInstance;

  InventoryServiceCall_Method whichMethod() => _InventoryServiceCall_MethodByTag[$_whichOneof(0)];
  void clearMethod() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  AddItem get addItem => $_getN(0);
  @$pb.TagNumber(1)
  set addItem(AddItem v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasAddItem() => $_has(0);
  @$pb.TagNumber(1)
  void clearAddItem() => clearField(1);
  @$pb.TagNumber(1)
  AddItem ensureAddItem() => $_ensure(0);

  @$pb.TagNumber(2)
  GetItem get getItem => $_getN(1);
  @$pb.TagNumber(2)
  set getItem(GetItem v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasGetItem() => $_has(1);
  @$pb.TagNumber(2)
  void clearGetItem() => clearField(2);
  @$pb.TagNumber(2)
  GetItem ensureGetItem() => $_ensure(1);

  @$pb.TagNumber(3)
  GetInventory get getInventory => $_getN(2);
  @$pb.TagNumber(3)
  set getInventory(GetInventory v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasGetInventory() => $_has(2);
  @$pb.TagNumber(3)
  void clearGetInventory() => clearField(3);
  @$pb.TagNumber(3)
  GetInventory ensureGetInventory() => $_ensure(2);

  @$pb.TagNumber(4)
  UpdateItem get updateItem => $_getN(3);
  @$pb.TagNumber(4)
  set updateItem(UpdateItem v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasUpdateItem() => $_has(3);
  @$pb.TagNumber(4)
  void clearUpdateItem() => clearField(4);
  @$pb.TagNumber(4)
  UpdateItem ensureUpdateItem() => $_ensure(3);

  @$pb.TagNumber(5)
  DeleteItem get deleteItem => $_getN(4);
  @$pb.TagNumber(5)
  set deleteItem(DeleteItem v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasDeleteItem() => $_has(4);
  @$pb.TagNumber(5)
  void clearDeleteItem() => clearField(5);
  @$pb.TagNumber(5)
  DeleteItem ensureDeleteItem() => $_ensure(4);

  @$pb.TagNumber(6)
  $0.MetaData get metaData => $_getN(5);
  @$pb.TagNumber(6)
  set metaData($0.MetaData v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasMetaData() => $_has(5);
  @$pb.TagNumber(6)
  void clearMetaData() => clearField(6);
  @$pb.TagNumber(6)
  $0.MetaData ensureMetaData() => $_ensure(5);
}

enum InventoryServiceResponse_Data {
  itemId, 
  item, 
  status, 
  inv, 
  notSet
}

class InventoryServiceResponse extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, InventoryServiceResponse_Data> _InventoryServiceResponse_DataByTag = {
    1 : InventoryServiceResponse_Data.itemId,
    2 : InventoryServiceResponse_Data.item,
    3 : InventoryServiceResponse_Data.status,
    4 : InventoryServiceResponse_Data.inv,
    0 : InventoryServiceResponse_Data.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InventoryServiceResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'item'), createEmptyInstance: create)
    ..oo(0, [1, 2, 3, 4])
    ..aOM<$0.ItemId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemId', protoName: 'itemId', subBuilder: $0.ItemId.create)
    ..aOM<Item>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'item', subBuilder: Item.create)
    ..aOM<$0.ResponseStatus>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', subBuilder: $0.ResponseStatus.create)
    ..aOM<Inventory>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'inv', subBuilder: Inventory.create)
    ..aOM<$0.MetaData>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'metaData', protoName: 'metaData', subBuilder: $0.MetaData.create)
    ..hasRequiredFields = false
  ;

  InventoryServiceResponse._() : super();
  factory InventoryServiceResponse({
    $0.ItemId itemId,
    Item item,
    $0.ResponseStatus status,
    Inventory inv,
    $0.MetaData metaData,
  }) {
    final _result = create();
    if (itemId != null) {
      _result.itemId = itemId;
    }
    if (item != null) {
      _result.item = item;
    }
    if (status != null) {
      _result.status = status;
    }
    if (inv != null) {
      _result.inv = inv;
    }
    if (metaData != null) {
      _result.metaData = metaData;
    }
    return _result;
  }
  factory InventoryServiceResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InventoryServiceResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InventoryServiceResponse clone() => InventoryServiceResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InventoryServiceResponse copyWith(void Function(InventoryServiceResponse) updates) => super.copyWith((message) => updates(message as InventoryServiceResponse)) as InventoryServiceResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InventoryServiceResponse create() => InventoryServiceResponse._();
  InventoryServiceResponse createEmptyInstance() => create();
  static $pb.PbList<InventoryServiceResponse> createRepeated() => $pb.PbList<InventoryServiceResponse>();
  @$core.pragma('dart2js:noInline')
  static InventoryServiceResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InventoryServiceResponse>(create);
  static InventoryServiceResponse _defaultInstance;

  InventoryServiceResponse_Data whichData() => _InventoryServiceResponse_DataByTag[$_whichOneof(0)];
  void clearData() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $0.ItemId get itemId => $_getN(0);
  @$pb.TagNumber(1)
  set itemId($0.ItemId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasItemId() => $_has(0);
  @$pb.TagNumber(1)
  void clearItemId() => clearField(1);
  @$pb.TagNumber(1)
  $0.ItemId ensureItemId() => $_ensure(0);

  @$pb.TagNumber(2)
  Item get item => $_getN(1);
  @$pb.TagNumber(2)
  set item(Item v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasItem() => $_has(1);
  @$pb.TagNumber(2)
  void clearItem() => clearField(2);
  @$pb.TagNumber(2)
  Item ensureItem() => $_ensure(1);

  @$pb.TagNumber(3)
  $0.ResponseStatus get status => $_getN(2);
  @$pb.TagNumber(3)
  set status($0.ResponseStatus v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStatus() => $_has(2);
  @$pb.TagNumber(3)
  void clearStatus() => clearField(3);
  @$pb.TagNumber(3)
  $0.ResponseStatus ensureStatus() => $_ensure(2);

  @$pb.TagNumber(4)
  Inventory get inv => $_getN(3);
  @$pb.TagNumber(4)
  set inv(Inventory v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasInv() => $_has(3);
  @$pb.TagNumber(4)
  void clearInv() => clearField(4);
  @$pb.TagNumber(4)
  Inventory ensureInv() => $_ensure(3);

  @$pb.TagNumber(5)
  $0.MetaData get metaData => $_getN(4);
  @$pb.TagNumber(5)
  set metaData($0.MetaData v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasMetaData() => $_has(4);
  @$pb.TagNumber(5)
  void clearMetaData() => clearField(5);
  @$pb.TagNumber(5)
  $0.MetaData ensureMetaData() => $_ensure(4);
}

