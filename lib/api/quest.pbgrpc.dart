///
//  Generated code. Do not modify.
//  source: quest.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'quest.pb.dart' as $8;
import 'common.pb.dart' as $0;
export 'quest.pb.dart';

class QuestServicesClient extends $grpc.Client {
  static final _$addQuest = $grpc.ClientMethod<$8.Quest, $0.QuestId>(
      '/quest.QuestServices/AddQuest',
      ($8.Quest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.QuestId.fromBuffer(value));
  static final _$getQuestByID = $grpc.ClientMethod<$0.QuestId, $8.Quest>(
      '/quest.QuestServices/GetQuestByID',
      ($0.QuestId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $8.Quest.fromBuffer(value));
  static final _$getQuestByUserID = $grpc.ClientMethod<$0.UserId, $8.QuestList>(
      '/quest.QuestServices/GetQuestByUserID',
      ($0.UserId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $8.QuestList.fromBuffer(value));
  static final _$startQuest =
      $grpc.ClientMethod<$8.StartQuestRequest, $0.ResponseStatus>(
          '/quest.QuestServices/StartQuest',
          ($8.StartQuestRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));

  QuestServicesClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options,
      $core.Iterable<$grpc.ClientInterceptor> interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.QuestId> addQuest($8.Quest request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$addQuest, request, options: options);
  }

  $grpc.ResponseFuture<$8.Quest> getQuestByID($0.QuestId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getQuestByID, request, options: options);
  }

  $grpc.ResponseFuture<$8.QuestList> getQuestByUserID($0.UserId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getQuestByUserID, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResponseStatus> startQuest(
      $8.StartQuestRequest request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$startQuest, request, options: options);
  }
}

abstract class QuestServicesServiceBase extends $grpc.Service {
  $core.String get $name => 'quest.QuestServices';

  QuestServicesServiceBase() {
    $addMethod($grpc.ServiceMethod<$8.Quest, $0.QuestId>(
        'AddQuest',
        addQuest_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $8.Quest.fromBuffer(value),
        ($0.QuestId value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.QuestId, $8.Quest>(
        'GetQuestByID',
        getQuestByID_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.QuestId.fromBuffer(value),
        ($8.Quest value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserId, $8.QuestList>(
        'GetQuestByUserID',
        getQuestByUserID_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserId.fromBuffer(value),
        ($8.QuestList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$8.StartQuestRequest, $0.ResponseStatus>(
        'StartQuest',
        startQuest_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $8.StartQuestRequest.fromBuffer(value),
        ($0.ResponseStatus value) => value.writeToBuffer()));
  }

  $async.Future<$0.QuestId> addQuest_Pre(
      $grpc.ServiceCall call, $async.Future<$8.Quest> request) async {
    return addQuest(call, await request);
  }

  $async.Future<$8.Quest> getQuestByID_Pre(
      $grpc.ServiceCall call, $async.Future<$0.QuestId> request) async {
    return getQuestByID(call, await request);
  }

  $async.Future<$8.QuestList> getQuestByUserID_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserId> request) async {
    return getQuestByUserID(call, await request);
  }

  $async.Future<$0.ResponseStatus> startQuest_Pre($grpc.ServiceCall call,
      $async.Future<$8.StartQuestRequest> request) async {
    return startQuest(call, await request);
  }

  $async.Future<$0.QuestId> addQuest($grpc.ServiceCall call, $8.Quest request);
  $async.Future<$8.Quest> getQuestByID(
      $grpc.ServiceCall call, $0.QuestId request);
  $async.Future<$8.QuestList> getQuestByUserID(
      $grpc.ServiceCall call, $0.UserId request);
  $async.Future<$0.ResponseStatus> startQuest(
      $grpc.ServiceCall call, $8.StartQuestRequest request);
}
