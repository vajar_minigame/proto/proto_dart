///
//  Generated code. Do not modify.
//  source: interactions.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'interactions.pb.dart' as $7;
import 'common.pb.dart' as $0;
export 'interactions.pb.dart';

class InteractionServiceClient extends $grpc.Client {
  static final _$monsterConsumeItem =
      $grpc.ClientMethod<$7.MonConsumeItemRequest, $0.ResponseStatus>(
          '/interaction.InteractionService/MonsterConsumeItem',
          ($7.MonConsumeItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));
  static final _$userConsumeItem =
      $grpc.ClientMethod<$7.UserConsumeItemRequest, $0.ResponseStatus>(
          '/interaction.InteractionService/UserConsumeItem',
          ($7.UserConsumeItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));

  InteractionServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options,
      $core.Iterable<$grpc.ClientInterceptor> interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.ResponseStatus> monsterConsumeItem(
      $7.MonConsumeItemRequest request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$monsterConsumeItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResponseStatus> userConsumeItem(
      $7.UserConsumeItemRequest request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$userConsumeItem, request, options: options);
  }
}

abstract class InteractionServiceBase extends $grpc.Service {
  $core.String get $name => 'interaction.InteractionService';

  InteractionServiceBase() {
    $addMethod($grpc.ServiceMethod<$7.MonConsumeItemRequest, $0.ResponseStatus>(
        'MonsterConsumeItem',
        monsterConsumeItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $7.MonConsumeItemRequest.fromBuffer(value),
        ($0.ResponseStatus value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$7.UserConsumeItemRequest, $0.ResponseStatus>(
            'UserConsumeItem',
            userConsumeItem_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $7.UserConsumeItemRequest.fromBuffer(value),
            ($0.ResponseStatus value) => value.writeToBuffer()));
  }

  $async.Future<$0.ResponseStatus> monsterConsumeItem_Pre(
      $grpc.ServiceCall call,
      $async.Future<$7.MonConsumeItemRequest> request) async {
    return monsterConsumeItem(call, await request);
  }

  $async.Future<$0.ResponseStatus> userConsumeItem_Pre($grpc.ServiceCall call,
      $async.Future<$7.UserConsumeItemRequest> request) async {
    return userConsumeItem(call, await request);
  }

  $async.Future<$0.ResponseStatus> monsterConsumeItem(
      $grpc.ServiceCall call, $7.MonConsumeItemRequest request);
  $async.Future<$0.ResponseStatus> userConsumeItem(
      $grpc.ServiceCall call, $7.UserConsumeItemRequest request);
}
