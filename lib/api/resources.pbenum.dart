///
//  Generated code. Do not modify.
//  source: resources.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class ResourceType extends $pb.ProtobufEnum {
  static const ResourceType FOREST = ResourceType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'FOREST');
  static const ResourceType WOOD = ResourceType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'WOOD');
  static const ResourceType GOLD = ResourceType._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'GOLD');
  static const ResourceType PLANK = ResourceType._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PLANK');

  static const $core.List<ResourceType> values = <ResourceType> [
    FOREST,
    WOOD,
    GOLD,
    PLANK,
  ];

  static final $core.Map<$core.int, ResourceType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static ResourceType valueOf($core.int value) => _byValue[value];

  const ResourceType._($core.int v, $core.String n) : super(v, n);
}

