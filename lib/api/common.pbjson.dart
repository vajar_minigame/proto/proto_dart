///
//  Generated code. Do not modify.
//  source: common.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use questIdDescriptor instead')
const QuestId$json = const {
  '1': 'QuestId',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
  ],
};

/// Descriptor for `QuestId`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List questIdDescriptor = $convert.base64Decode('CgdRdWVzdElkEg4KAmlkGAEgASgFUgJpZA==');
@$core.Deprecated('Use monIdDescriptor instead')
const MonId$json = const {
  '1': 'MonId',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
  ],
};

/// Descriptor for `MonId`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List monIdDescriptor = $convert.base64Decode('CgVNb25JZBIOCgJpZBgBIAEoBVICaWQ=');
@$core.Deprecated('Use userIdDescriptor instead')
const UserId$json = const {
  '1': 'UserId',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
  ],
};

/// Descriptor for `UserId`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userIdDescriptor = $convert.base64Decode('CgZVc2VySWQSDgoCaWQYASABKAVSAmlk');
@$core.Deprecated('Use itemIdDescriptor instead')
const ItemId$json = const {
  '1': 'ItemId',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
  ],
};

/// Descriptor for `ItemId`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemIdDescriptor = $convert.base64Decode('CgZJdGVtSWQSDgoCaWQYASABKAVSAmlk');
@$core.Deprecated('Use messageIdDescriptor instead')
const MessageId$json = const {
  '1': 'MessageId',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
  ],
};

/// Descriptor for `MessageId`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageIdDescriptor = $convert.base64Decode('CglNZXNzYWdlSWQSDgoCaWQYASABKAVSAmlk');
@$core.Deprecated('Use battleIdDescriptor instead')
const BattleId$json = const {
  '1': 'BattleId',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
  ],
};

/// Descriptor for `BattleId`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleIdDescriptor = $convert.base64Decode('CghCYXR0bGVJZBIOCgJpZBgBIAEoBVICaWQ=');
@$core.Deprecated('Use rewardIdDescriptor instead')
const RewardId$json = const {
  '1': 'RewardId',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
  ],
};

/// Descriptor for `RewardId`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List rewardIdDescriptor = $convert.base64Decode('CghSZXdhcmRJZBIOCgJpZBgBIAEoBVICaWQ=');
@$core.Deprecated('Use settlementIdDescriptor instead')
const SettlementId$json = const {
  '1': 'SettlementId',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 3, '10': 'id'},
  ],
};

/// Descriptor for `SettlementId`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List settlementIdDescriptor = $convert.base64Decode('CgxTZXR0bGVtZW50SWQSDgoCaWQYASABKANSAmlk');
@$core.Deprecated('Use buildingIdDescriptor instead')
const BuildingId$json = const {
  '1': 'BuildingId',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 3, '10': 'id'},
  ],
};

/// Descriptor for `BuildingId`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List buildingIdDescriptor = $convert.base64Decode('CgpCdWlsZGluZ0lkEg4KAmlkGAEgASgDUgJpZA==');
@$core.Deprecated('Use responseStatusDescriptor instead')
const ResponseStatus$json = const {
  '1': 'ResponseStatus',
  '2': const [
    const {'1': 'success', '3': 1, '4': 1, '5': 8, '10': 'success'},
  ],
};

/// Descriptor for `ResponseStatus`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List responseStatusDescriptor = $convert.base64Decode('Cg5SZXNwb25zZVN0YXR1cxIYCgdzdWNjZXNzGAEgASgIUgdzdWNjZXNz');
@$core.Deprecated('Use metaDataDescriptor instead')
const MetaData$json = const {
  '1': 'MetaData',
  '2': const [
    const {'1': 'MessageID', '3': 1, '4': 1, '5': 5, '10': 'MessageID'},
    const {'1': 'CorrID', '3': 2, '4': 1, '5': 5, '10': 'CorrID'},
    const {'1': 'jwt', '3': 3, '4': 1, '5': 9, '10': 'jwt'},
  ],
};

/// Descriptor for `MetaData`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List metaDataDescriptor = $convert.base64Decode('CghNZXRhRGF0YRIcCglNZXNzYWdlSUQYASABKAVSCU1lc3NhZ2VJRBIWCgZDb3JySUQYAiABKAVSBkNvcnJJRBIQCgNqd3QYAyABKAlSA2p3dA==');
