///
//  Generated code. Do not modify.
//  source: resources.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use resourceTypeDescriptor instead')
const ResourceType$json = const {
  '1': 'ResourceType',
  '2': const [
    const {'1': 'FOREST', '2': 0},
    const {'1': 'WOOD', '2': 1},
    const {'1': 'GOLD', '2': 2},
    const {'1': 'PLANK', '2': 3},
  ],
};

/// Descriptor for `ResourceType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List resourceTypeDescriptor = $convert.base64Decode('CgxSZXNvdXJjZVR5cGUSCgoGRk9SRVNUEAASCAoEV09PRBABEggKBEdPTEQQAhIJCgVQTEFOSxAD');
