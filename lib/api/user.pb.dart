///
//  Generated code. Do not modify.
//  source: user.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'google/protobuf/timestamp.pb.dart' as $12;
import 'common.pb.dart' as $0;

class User extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'User', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'user'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', $pb.PbFieldType.O3)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'username')
    ..aOM<$12.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastUpdate', subBuilder: $12.Timestamp.create)
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isTest')
    ..hasRequiredFields = false
  ;

  User._() : super();
  factory User({
    $core.int id,
    $core.String username,
    $12.Timestamp lastUpdate,
    $core.bool isTest,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (username != null) {
      _result.username = username;
    }
    if (lastUpdate != null) {
      _result.lastUpdate = lastUpdate;
    }
    if (isTest != null) {
      _result.isTest = isTest;
    }
    return _result;
  }
  factory User.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory User.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  User clone() => User()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  User copyWith(void Function(User) updates) => super.copyWith((message) => updates(message as User)) as User; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static User create() => User._();
  User createEmptyInstance() => create();
  static $pb.PbList<User> createRepeated() => $pb.PbList<User>();
  @$core.pragma('dart2js:noInline')
  static User getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<User>(create);
  static User _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get id => $_getIZ(0);
  @$pb.TagNumber(1)
  set id($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get username => $_getSZ(1);
  @$pb.TagNumber(2)
  set username($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasUsername() => $_has(1);
  @$pb.TagNumber(2)
  void clearUsername() => clearField(2);

  @$pb.TagNumber(3)
  $12.Timestamp get lastUpdate => $_getN(2);
  @$pb.TagNumber(3)
  set lastUpdate($12.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasLastUpdate() => $_has(2);
  @$pb.TagNumber(3)
  void clearLastUpdate() => clearField(3);
  @$pb.TagNumber(3)
  $12.Timestamp ensureLastUpdate() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.bool get isTest => $_getBF(3);
  @$pb.TagNumber(4)
  set isTest($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasIsTest() => $_has(3);
  @$pb.TagNumber(4)
  void clearIsTest() => clearField(4);
}

class GetUserCall extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetUserCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'user'), createEmptyInstance: create)
    ..aOM<$0.UserId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'user', subBuilder: $0.UserId.create)
    ..hasRequiredFields = false
  ;

  GetUserCall._() : super();
  factory GetUserCall({
    $0.UserId user,
  }) {
    final _result = create();
    if (user != null) {
      _result.user = user;
    }
    return _result;
  }
  factory GetUserCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetUserCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetUserCall clone() => GetUserCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetUserCall copyWith(void Function(GetUserCall) updates) => super.copyWith((message) => updates(message as GetUserCall)) as GetUserCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetUserCall create() => GetUserCall._();
  GetUserCall createEmptyInstance() => create();
  static $pb.PbList<GetUserCall> createRepeated() => $pb.PbList<GetUserCall>();
  @$core.pragma('dart2js:noInline')
  static GetUserCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetUserCall>(create);
  static GetUserCall _defaultInstance;

  @$pb.TagNumber(1)
  $0.UserId get user => $_getN(0);
  @$pb.TagNumber(1)
  set user($0.UserId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);
  @$pb.TagNumber(1)
  $0.UserId ensureUser() => $_ensure(0);
}

class GetUserResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetUserResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'user'), createEmptyInstance: create)
    ..aOM<User>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'user', subBuilder: User.create)
    ..hasRequiredFields = false
  ;

  GetUserResponse._() : super();
  factory GetUserResponse({
    User user,
  }) {
    final _result = create();
    if (user != null) {
      _result.user = user;
    }
    return _result;
  }
  factory GetUserResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetUserResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetUserResponse clone() => GetUserResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetUserResponse copyWith(void Function(GetUserResponse) updates) => super.copyWith((message) => updates(message as GetUserResponse)) as GetUserResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetUserResponse create() => GetUserResponse._();
  GetUserResponse createEmptyInstance() => create();
  static $pb.PbList<GetUserResponse> createRepeated() => $pb.PbList<GetUserResponse>();
  @$core.pragma('dart2js:noInline')
  static GetUserResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetUserResponse>(create);
  static GetUserResponse _defaultInstance;

  @$pb.TagNumber(1)
  User get user => $_getN(0);
  @$pb.TagNumber(1)
  set user(User v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);
  @$pb.TagNumber(1)
  User ensureUser() => $_ensure(0);
}

enum UserServiceCall_Message {
  getUserCall, 
  notSet
}

class UserServiceCall extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, UserServiceCall_Message> _UserServiceCall_MessageByTag = {
    1 : UserServiceCall_Message.getUserCall,
    0 : UserServiceCall_Message.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UserServiceCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'user'), createEmptyInstance: create)
    ..oo(0, [1])
    ..aOM<GetUserCall>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'getUserCall', subBuilder: GetUserCall.create)
    ..hasRequiredFields = false
  ;

  UserServiceCall._() : super();
  factory UserServiceCall({
    GetUserCall getUserCall,
  }) {
    final _result = create();
    if (getUserCall != null) {
      _result.getUserCall = getUserCall;
    }
    return _result;
  }
  factory UserServiceCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UserServiceCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UserServiceCall clone() => UserServiceCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UserServiceCall copyWith(void Function(UserServiceCall) updates) => super.copyWith((message) => updates(message as UserServiceCall)) as UserServiceCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UserServiceCall create() => UserServiceCall._();
  UserServiceCall createEmptyInstance() => create();
  static $pb.PbList<UserServiceCall> createRepeated() => $pb.PbList<UserServiceCall>();
  @$core.pragma('dart2js:noInline')
  static UserServiceCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UserServiceCall>(create);
  static UserServiceCall _defaultInstance;

  UserServiceCall_Message whichMessage() => _UserServiceCall_MessageByTag[$_whichOneof(0)];
  void clearMessage() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  GetUserCall get getUserCall => $_getN(0);
  @$pb.TagNumber(1)
  set getUserCall(GetUserCall v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasGetUserCall() => $_has(0);
  @$pb.TagNumber(1)
  void clearGetUserCall() => clearField(1);
  @$pb.TagNumber(1)
  GetUserCall ensureGetUserCall() => $_ensure(0);
}

enum UserServiceResponse_Message {
  getUserResponse, 
  notSet
}

class UserServiceResponse extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, UserServiceResponse_Message> _UserServiceResponse_MessageByTag = {
    1 : UserServiceResponse_Message.getUserResponse,
    0 : UserServiceResponse_Message.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UserServiceResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'user'), createEmptyInstance: create)
    ..oo(0, [1])
    ..aOM<GetUserResponse>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'getUserResponse', subBuilder: GetUserResponse.create)
    ..hasRequiredFields = false
  ;

  UserServiceResponse._() : super();
  factory UserServiceResponse({
    GetUserResponse getUserResponse,
  }) {
    final _result = create();
    if (getUserResponse != null) {
      _result.getUserResponse = getUserResponse;
    }
    return _result;
  }
  factory UserServiceResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UserServiceResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UserServiceResponse clone() => UserServiceResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UserServiceResponse copyWith(void Function(UserServiceResponse) updates) => super.copyWith((message) => updates(message as UserServiceResponse)) as UserServiceResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UserServiceResponse create() => UserServiceResponse._();
  UserServiceResponse createEmptyInstance() => create();
  static $pb.PbList<UserServiceResponse> createRepeated() => $pb.PbList<UserServiceResponse>();
  @$core.pragma('dart2js:noInline')
  static UserServiceResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UserServiceResponse>(create);
  static UserServiceResponse _defaultInstance;

  UserServiceResponse_Message whichMessage() => _UserServiceResponse_MessageByTag[$_whichOneof(0)];
  void clearMessage() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  GetUserResponse get getUserResponse => $_getN(0);
  @$pb.TagNumber(1)
  set getUserResponse(GetUserResponse v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasGetUserResponse() => $_has(0);
  @$pb.TagNumber(1)
  void clearGetUserResponse() => clearField(1);
  @$pb.TagNumber(1)
  GetUserResponse ensureGetUserResponse() => $_ensure(0);
}

