///
//  Generated code. Do not modify.
//  source: eventStream.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'common.pb.dart' as $0;
import 'controller.pb.dart' as $9;
export 'eventStream.pb.dart';

class EventStreamingServiceClient extends $grpc.Client {
  static final _$eventStream = $grpc.ClientMethod<$0.UserId, $9.Event>(
      '/streaming.EventStreamingService/EventStream',
      ($0.UserId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $9.Event.fromBuffer(value));

  EventStreamingServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options,
      $core.Iterable<$grpc.ClientInterceptor> interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseStream<$9.Event> eventStream($0.UserId request,
      {$grpc.CallOptions options}) {
    return $createStreamingCall(
        _$eventStream, $async.Stream.fromIterable([request]),
        options: options);
  }
}

abstract class EventStreamingServiceBase extends $grpc.Service {
  $core.String get $name => 'streaming.EventStreamingService';

  EventStreamingServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.UserId, $9.Event>(
        'EventStream',
        eventStream_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.UserId.fromBuffer(value),
        ($9.Event value) => value.writeToBuffer()));
  }

  $async.Stream<$9.Event> eventStream_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserId> request) async* {
    yield* eventStream(call, await request);
  }

  $async.Stream<$9.Event> eventStream(
      $grpc.ServiceCall call, $0.UserId request);
}
