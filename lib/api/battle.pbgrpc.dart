///
//  Generated code. Do not modify.
//  source: battle.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'battle.pb.dart' as $4;
import 'common.pb.dart' as $0;
export 'battle.pb.dart';

class BattleServiceClient extends $grpc.Client {
  static final _$addBattle = $grpc.ClientMethod<$4.Battle, $0.BattleId>(
      '/battle.BattleService/AddBattle',
      ($4.Battle value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.BattleId.fromBuffer(value));
  static final _$getBattleByID = $grpc.ClientMethod<$0.BattleId, $4.Battle>(
      '/battle.BattleService/getBattleByID',
      ($0.BattleId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $4.Battle.fromBuffer(value));
  static final _$getBattleByUserID =
      $grpc.ClientMethod<$0.UserId, $4.BattleList>(
          '/battle.BattleService/GetBattleByUserID',
          ($0.UserId value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $4.BattleList.fromBuffer(value));
  static final _$startBattle =
      $grpc.ClientMethod<$4.BattleRequest, $4.ResponseStatus>(
          '/battle.BattleService/StartBattle',
          ($4.BattleRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $4.ResponseStatus.fromBuffer(value));
  static final _$addTestBattle = $grpc.ClientMethod<$4.Battle, $0.BattleId>(
      '/battle.BattleService/AddTestBattle',
      ($4.Battle value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.BattleId.fromBuffer(value));
  static final _$commandRequest =
      $grpc.ClientMethod<$4.Command, $4.ResponseStatus>(
          '/battle.BattleService/CommandRequest',
          ($4.Command value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $4.ResponseStatus.fromBuffer(value));

  BattleServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options,
      $core.Iterable<$grpc.ClientInterceptor> interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.BattleId> addBattle($4.Battle request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$addBattle, request, options: options);
  }

  $grpc.ResponseFuture<$4.Battle> getBattleByID($0.BattleId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getBattleByID, request, options: options);
  }

  $grpc.ResponseFuture<$4.BattleList> getBattleByUserID($0.UserId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getBattleByUserID, request, options: options);
  }

  $grpc.ResponseFuture<$4.ResponseStatus> startBattle($4.BattleRequest request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$startBattle, request, options: options);
  }

  $grpc.ResponseFuture<$0.BattleId> addTestBattle($4.Battle request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$addTestBattle, request, options: options);
  }

  $grpc.ResponseFuture<$4.ResponseStatus> commandRequest($4.Command request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$commandRequest, request, options: options);
  }
}

abstract class BattleServiceBase extends $grpc.Service {
  $core.String get $name => 'battle.BattleService';

  BattleServiceBase() {
    $addMethod($grpc.ServiceMethod<$4.Battle, $0.BattleId>(
        'AddBattle',
        addBattle_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $4.Battle.fromBuffer(value),
        ($0.BattleId value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.BattleId, $4.Battle>(
        'getBattleByID',
        getBattleByID_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.BattleId.fromBuffer(value),
        ($4.Battle value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserId, $4.BattleList>(
        'GetBattleByUserID',
        getBattleByUserID_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserId.fromBuffer(value),
        ($4.BattleList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$4.BattleRequest, $4.ResponseStatus>(
        'StartBattle',
        startBattle_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $4.BattleRequest.fromBuffer(value),
        ($4.ResponseStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$4.Battle, $0.BattleId>(
        'AddTestBattle',
        addTestBattle_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $4.Battle.fromBuffer(value),
        ($0.BattleId value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$4.Command, $4.ResponseStatus>(
        'CommandRequest',
        commandRequest_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $4.Command.fromBuffer(value),
        ($4.ResponseStatus value) => value.writeToBuffer()));
  }

  $async.Future<$0.BattleId> addBattle_Pre(
      $grpc.ServiceCall call, $async.Future<$4.Battle> request) async {
    return addBattle(call, await request);
  }

  $async.Future<$4.Battle> getBattleByID_Pre(
      $grpc.ServiceCall call, $async.Future<$0.BattleId> request) async {
    return getBattleByID(call, await request);
  }

  $async.Future<$4.BattleList> getBattleByUserID_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserId> request) async {
    return getBattleByUserID(call, await request);
  }

  $async.Future<$4.ResponseStatus> startBattle_Pre(
      $grpc.ServiceCall call, $async.Future<$4.BattleRequest> request) async {
    return startBattle(call, await request);
  }

  $async.Future<$0.BattleId> addTestBattle_Pre(
      $grpc.ServiceCall call, $async.Future<$4.Battle> request) async {
    return addTestBattle(call, await request);
  }

  $async.Future<$4.ResponseStatus> commandRequest_Pre(
      $grpc.ServiceCall call, $async.Future<$4.Command> request) async {
    return commandRequest(call, await request);
  }

  $async.Future<$0.BattleId> addBattle(
      $grpc.ServiceCall call, $4.Battle request);
  $async.Future<$4.Battle> getBattleByID(
      $grpc.ServiceCall call, $0.BattleId request);
  $async.Future<$4.BattleList> getBattleByUserID(
      $grpc.ServiceCall call, $0.UserId request);
  $async.Future<$4.ResponseStatus> startBattle(
      $grpc.ServiceCall call, $4.BattleRequest request);
  $async.Future<$0.BattleId> addTestBattle(
      $grpc.ServiceCall call, $4.Battle request);
  $async.Future<$4.ResponseStatus> commandRequest(
      $grpc.ServiceCall call, $4.Command request);
}
