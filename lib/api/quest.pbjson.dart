///
//  Generated code. Do not modify.
//  source: quest.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use questDescriptor instead')
const Quest$json = const {
  '1': 'Quest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.common.QuestId', '10': 'id'},
    const {'1': 'difficulty', '3': 2, '4': 1, '5': 5, '10': 'difficulty'},
    const {'1': 'recommendedStrength', '3': 3, '4': 1, '5': 5, '10': 'recommendedStrength'},
    const {'1': 'playerID', '3': 4, '4': 1, '5': 5, '10': 'playerID'},
    const {'1': 'duration', '3': 5, '4': 1, '5': 5, '10': 'duration'},
    const {'1': 'activeQuest', '3': 6, '4': 1, '5': 11, '6': '.quest.ActiveQuest', '10': 'activeQuest'},
  ],
};

/// Descriptor for `Quest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List questDescriptor = $convert.base64Decode('CgVRdWVzdBIfCgJpZBgBIAEoCzIPLmNvbW1vbi5RdWVzdElkUgJpZBIeCgpkaWZmaWN1bHR5GAIgASgFUgpkaWZmaWN1bHR5EjAKE3JlY29tbWVuZGVkU3RyZW5ndGgYAyABKAVSE3JlY29tbWVuZGVkU3RyZW5ndGgSGgoIcGxheWVySUQYBCABKAVSCHBsYXllcklEEhoKCGR1cmF0aW9uGAUgASgFUghkdXJhdGlvbhI0CgthY3RpdmVRdWVzdBgGIAEoCzISLnF1ZXN0LkFjdGl2ZVF1ZXN0UgthY3RpdmVRdWVzdA==');
@$core.Deprecated('Use activeQuestDescriptor instead')
const ActiveQuest$json = const {
  '1': 'ActiveQuest',
  '2': const [
    const {'1': 'start_time', '3': 1, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startTime'},
    const {'1': 'monsterIds', '3': 2, '4': 3, '5': 11, '6': '.common.MonId', '10': 'monsterIds'},
  ],
};

/// Descriptor for `ActiveQuest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List activeQuestDescriptor = $convert.base64Decode('CgtBY3RpdmVRdWVzdBI5CgpzdGFydF90aW1lGAEgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIJc3RhcnRUaW1lEi0KCm1vbnN0ZXJJZHMYAiADKAsyDS5jb21tb24uTW9uSWRSCm1vbnN0ZXJJZHM=');
@$core.Deprecated('Use startQuestRequestDescriptor instead')
const StartQuestRequest$json = const {
  '1': 'StartQuestRequest',
  '2': const [
    const {'1': 'questId', '3': 1, '4': 1, '5': 11, '6': '.common.QuestId', '10': 'questId'},
    const {'1': 'monsterIds', '3': 2, '4': 3, '5': 11, '6': '.common.MonId', '10': 'monsterIds'},
  ],
};

/// Descriptor for `StartQuestRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List startQuestRequestDescriptor = $convert.base64Decode('ChFTdGFydFF1ZXN0UmVxdWVzdBIpCgdxdWVzdElkGAEgASgLMg8uY29tbW9uLlF1ZXN0SWRSB3F1ZXN0SWQSLQoKbW9uc3RlcklkcxgCIAMoCzINLmNvbW1vbi5Nb25JZFIKbW9uc3Rlcklkcw==');
@$core.Deprecated('Use getQuestsByUserDescriptor instead')
const GetQuestsByUser$json = const {
  '1': 'GetQuestsByUser',
  '2': const [
    const {'1': 'userId', '3': 1, '4': 1, '5': 11, '6': '.common.UserId', '10': 'userId'},
  ],
};

/// Descriptor for `GetQuestsByUser`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getQuestsByUserDescriptor = $convert.base64Decode('Cg9HZXRRdWVzdHNCeVVzZXISJgoGdXNlcklkGAEgASgLMg4uY29tbW9uLlVzZXJJZFIGdXNlcklk');
@$core.Deprecated('Use getQuestDescriptor instead')
const GetQuest$json = const {
  '1': 'GetQuest',
  '2': const [
    const {'1': 'questId', '3': 1, '4': 1, '5': 11, '6': '.common.QuestId', '10': 'questId'},
  ],
};

/// Descriptor for `GetQuest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getQuestDescriptor = $convert.base64Decode('CghHZXRRdWVzdBIpCgdxdWVzdElkGAEgASgLMg8uY29tbW9uLlF1ZXN0SWRSB3F1ZXN0SWQ=');
@$core.Deprecated('Use startQuestResponseDescriptor instead')
const StartQuestResponse$json = const {
  '1': 'StartQuestResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 11, '6': '.common.ResponseStatus', '10': 'status'},
  ],
};

/// Descriptor for `StartQuestResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List startQuestResponseDescriptor = $convert.base64Decode('ChJTdGFydFF1ZXN0UmVzcG9uc2USLgoGc3RhdHVzGAEgASgLMhYuY29tbW9uLlJlc3BvbnNlU3RhdHVzUgZzdGF0dXM=');
@$core.Deprecated('Use getQuestResponseDescriptor instead')
const GetQuestResponse$json = const {
  '1': 'GetQuestResponse',
  '2': const [
    const {'1': 'quest', '3': 1, '4': 1, '5': 11, '6': '.quest.Quest', '10': 'quest'},
  ],
};

/// Descriptor for `GetQuestResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getQuestResponseDescriptor = $convert.base64Decode('ChBHZXRRdWVzdFJlc3BvbnNlEiIKBXF1ZXN0GAEgASgLMgwucXVlc3QuUXVlc3RSBXF1ZXN0');
@$core.Deprecated('Use getQuestByUserResponseDescriptor instead')
const GetQuestByUserResponse$json = const {
  '1': 'GetQuestByUserResponse',
  '2': const [
    const {'1': 'quests', '3': 1, '4': 3, '5': 11, '6': '.quest.Quest', '10': 'quests'},
  ],
};

/// Descriptor for `GetQuestByUserResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getQuestByUserResponseDescriptor = $convert.base64Decode('ChZHZXRRdWVzdEJ5VXNlclJlc3BvbnNlEiQKBnF1ZXN0cxgBIAMoCzIMLnF1ZXN0LlF1ZXN0UgZxdWVzdHM=');
@$core.Deprecated('Use questListDescriptor instead')
const QuestList$json = const {
  '1': 'QuestList',
  '2': const [
    const {'1': 'quests', '3': 1, '4': 3, '5': 11, '6': '.quest.Quest', '10': 'quests'},
  ],
};

/// Descriptor for `QuestList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List questListDescriptor = $convert.base64Decode('CglRdWVzdExpc3QSJAoGcXVlc3RzGAEgAygLMgwucXVlc3QuUXVlc3RSBnF1ZXN0cw==');
@$core.Deprecated('Use questCompletedEventDescriptor instead')
const QuestCompletedEvent$json = const {
  '1': 'QuestCompletedEvent',
  '2': const [
    const {'1': 'questId', '3': 1, '4': 1, '5': 11, '6': '.common.QuestId', '10': 'questId'},
  ],
};

/// Descriptor for `QuestCompletedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List questCompletedEventDescriptor = $convert.base64Decode('ChNRdWVzdENvbXBsZXRlZEV2ZW50EikKB3F1ZXN0SWQYASABKAsyDy5jb21tb24uUXVlc3RJZFIHcXVlc3RJZA==');
@$core.Deprecated('Use questUpdatedEventDescriptor instead')
const QuestUpdatedEvent$json = const {
  '1': 'QuestUpdatedEvent',
  '2': const [
    const {'1': 'quest', '3': 1, '4': 1, '5': 11, '6': '.quest.Quest', '10': 'quest'},
  ],
};

/// Descriptor for `QuestUpdatedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List questUpdatedEventDescriptor = $convert.base64Decode('ChFRdWVzdFVwZGF0ZWRFdmVudBIiCgVxdWVzdBgBIAEoCzIMLnF1ZXN0LlF1ZXN0UgVxdWVzdA==');
@$core.Deprecated('Use questDeletedEventDescriptor instead')
const QuestDeletedEvent$json = const {
  '1': 'QuestDeletedEvent',
  '2': const [
    const {'1': 'questId', '3': 1, '4': 1, '5': 11, '6': '.common.QuestId', '10': 'questId'},
  ],
};

/// Descriptor for `QuestDeletedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List questDeletedEventDescriptor = $convert.base64Decode('ChFRdWVzdERlbGV0ZWRFdmVudBIpCgdxdWVzdElkGAEgASgLMg8uY29tbW9uLlF1ZXN0SWRSB3F1ZXN0SWQ=');
@$core.Deprecated('Use questStartedEventDescriptor instead')
const QuestStartedEvent$json = const {
  '1': 'QuestStartedEvent',
  '2': const [
    const {'1': 'quest', '3': 1, '4': 1, '5': 11, '6': '.quest.Quest', '10': 'quest'},
  ],
};

/// Descriptor for `QuestStartedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List questStartedEventDescriptor = $convert.base64Decode('ChFRdWVzdFN0YXJ0ZWRFdmVudBIiCgVxdWVzdBgBIAEoCzIMLnF1ZXN0LlF1ZXN0UgVxdWVzdA==');
@$core.Deprecated('Use questEventDescriptor instead')
const QuestEvent$json = const {
  '1': 'QuestEvent',
  '2': const [
    const {'1': 'completedEvent', '3': 1, '4': 1, '5': 11, '6': '.quest.QuestCompletedEvent', '9': 0, '10': 'completedEvent'},
    const {'1': 'updatedEvent', '3': 2, '4': 1, '5': 11, '6': '.quest.QuestUpdatedEvent', '9': 0, '10': 'updatedEvent'},
    const {'1': 'deletedEvent', '3': 3, '4': 1, '5': 11, '6': '.quest.QuestDeletedEvent', '9': 0, '10': 'deletedEvent'},
    const {'1': 'startedEvent', '3': 4, '4': 1, '5': 11, '6': '.quest.QuestStartedEvent', '9': 0, '10': 'startedEvent'},
  ],
  '8': const [
    const {'1': 'event'},
  ],
};

/// Descriptor for `QuestEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List questEventDescriptor = $convert.base64Decode('CgpRdWVzdEV2ZW50EkQKDmNvbXBsZXRlZEV2ZW50GAEgASgLMhoucXVlc3QuUXVlc3RDb21wbGV0ZWRFdmVudEgAUg5jb21wbGV0ZWRFdmVudBI+Cgx1cGRhdGVkRXZlbnQYAiABKAsyGC5xdWVzdC5RdWVzdFVwZGF0ZWRFdmVudEgAUgx1cGRhdGVkRXZlbnQSPgoMZGVsZXRlZEV2ZW50GAMgASgLMhgucXVlc3QuUXVlc3REZWxldGVkRXZlbnRIAFIMZGVsZXRlZEV2ZW50Ej4KDHN0YXJ0ZWRFdmVudBgEIAEoCzIYLnF1ZXN0LlF1ZXN0U3RhcnRlZEV2ZW50SABSDHN0YXJ0ZWRFdmVudEIHCgVldmVudA==');
@$core.Deprecated('Use questServiceCallDescriptor instead')
const QuestServiceCall$json = const {
  '1': 'QuestServiceCall',
  '2': const [
    const {'1': 'startQuest', '3': 1, '4': 1, '5': 11, '6': '.quest.StartQuestRequest', '9': 0, '10': 'startQuest'},
    const {'1': 'getQuest', '3': 2, '4': 1, '5': 11, '6': '.quest.GetQuest', '9': 0, '10': 'getQuest'},
    const {'1': 'questQuestByUser', '3': 3, '4': 1, '5': 11, '6': '.quest.GetQuestsByUser', '9': 0, '10': 'questQuestByUser'},
    const {'1': 'metaData', '3': 4, '4': 1, '5': 11, '6': '.common.MetaData', '10': 'metaData'},
  ],
  '8': const [
    const {'1': 'method'},
  ],
};

/// Descriptor for `QuestServiceCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List questServiceCallDescriptor = $convert.base64Decode('ChBRdWVzdFNlcnZpY2VDYWxsEjoKCnN0YXJ0UXVlc3QYASABKAsyGC5xdWVzdC5TdGFydFF1ZXN0UmVxdWVzdEgAUgpzdGFydFF1ZXN0Ei0KCGdldFF1ZXN0GAIgASgLMg8ucXVlc3QuR2V0UXVlc3RIAFIIZ2V0UXVlc3QSRAoQcXVlc3RRdWVzdEJ5VXNlchgDIAEoCzIWLnF1ZXN0LkdldFF1ZXN0c0J5VXNlckgAUhBxdWVzdFF1ZXN0QnlVc2VyEiwKCG1ldGFEYXRhGAQgASgLMhAuY29tbW9uLk1ldGFEYXRhUghtZXRhRGF0YUIICgZtZXRob2Q=');
@$core.Deprecated('Use questServiceResponseDescriptor instead')
const QuestServiceResponse$json = const {
  '1': 'QuestServiceResponse',
  '2': const [
    const {'1': 'startQuestResponse', '3': 1, '4': 1, '5': 11, '6': '.quest.StartQuestResponse', '9': 0, '10': 'startQuestResponse'},
    const {'1': 'getQuestResponse', '3': 2, '4': 1, '5': 11, '6': '.quest.GetQuestResponse', '9': 0, '10': 'getQuestResponse'},
    const {'1': 'getQuestByUserResponse', '3': 3, '4': 1, '5': 11, '6': '.quest.GetQuestByUserResponse', '9': 0, '10': 'getQuestByUserResponse'},
    const {'1': 'metaData', '3': 4, '4': 1, '5': 11, '6': '.common.MetaData', '10': 'metaData'},
  ],
  '8': const [
    const {'1': 'data'},
  ],
};

/// Descriptor for `QuestServiceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List questServiceResponseDescriptor = $convert.base64Decode('ChRRdWVzdFNlcnZpY2VSZXNwb25zZRJLChJzdGFydFF1ZXN0UmVzcG9uc2UYASABKAsyGS5xdWVzdC5TdGFydFF1ZXN0UmVzcG9uc2VIAFISc3RhcnRRdWVzdFJlc3BvbnNlEkUKEGdldFF1ZXN0UmVzcG9uc2UYAiABKAsyFy5xdWVzdC5HZXRRdWVzdFJlc3BvbnNlSABSEGdldFF1ZXN0UmVzcG9uc2USVwoWZ2V0UXVlc3RCeVVzZXJSZXNwb25zZRgDIAEoCzIdLnF1ZXN0LkdldFF1ZXN0QnlVc2VyUmVzcG9uc2VIAFIWZ2V0UXVlc3RCeVVzZXJSZXNwb25zZRIsCghtZXRhRGF0YRgEIAEoCzIQLmNvbW1vbi5NZXRhRGF0YVIIbWV0YURhdGFCBgoEZGF0YQ==');
