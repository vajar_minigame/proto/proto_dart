///
//  Generated code. Do not modify.
//  source: interactions.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'common.pb.dart' as $0;

class MonConsumeItemRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MonConsumeItemRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'interaction'), createEmptyInstance: create)
    ..aOM<$0.ItemId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemId', protoName: 'itemId', subBuilder: $0.ItemId.create)
    ..aOM<$0.MonId>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'monId', protoName: 'monId', subBuilder: $0.MonId.create)
    ..hasRequiredFields = false
  ;

  MonConsumeItemRequest._() : super();
  factory MonConsumeItemRequest({
    $0.ItemId itemId,
    $0.MonId monId,
  }) {
    final _result = create();
    if (itemId != null) {
      _result.itemId = itemId;
    }
    if (monId != null) {
      _result.monId = monId;
    }
    return _result;
  }
  factory MonConsumeItemRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MonConsumeItemRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MonConsumeItemRequest clone() => MonConsumeItemRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MonConsumeItemRequest copyWith(void Function(MonConsumeItemRequest) updates) => super.copyWith((message) => updates(message as MonConsumeItemRequest)) as MonConsumeItemRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MonConsumeItemRequest create() => MonConsumeItemRequest._();
  MonConsumeItemRequest createEmptyInstance() => create();
  static $pb.PbList<MonConsumeItemRequest> createRepeated() => $pb.PbList<MonConsumeItemRequest>();
  @$core.pragma('dart2js:noInline')
  static MonConsumeItemRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MonConsumeItemRequest>(create);
  static MonConsumeItemRequest _defaultInstance;

  @$pb.TagNumber(1)
  $0.ItemId get itemId => $_getN(0);
  @$pb.TagNumber(1)
  set itemId($0.ItemId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasItemId() => $_has(0);
  @$pb.TagNumber(1)
  void clearItemId() => clearField(1);
  @$pb.TagNumber(1)
  $0.ItemId ensureItemId() => $_ensure(0);

  @$pb.TagNumber(2)
  $0.MonId get monId => $_getN(1);
  @$pb.TagNumber(2)
  set monId($0.MonId v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasMonId() => $_has(1);
  @$pb.TagNumber(2)
  void clearMonId() => clearField(2);
  @$pb.TagNumber(2)
  $0.MonId ensureMonId() => $_ensure(1);
}

class UserConsumeItemRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UserConsumeItemRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'interaction'), createEmptyInstance: create)
    ..aOM<$0.ItemId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemId', protoName: 'itemId', subBuilder: $0.ItemId.create)
    ..aOM<$0.UserId>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId', protoName: 'userId', subBuilder: $0.UserId.create)
    ..hasRequiredFields = false
  ;

  UserConsumeItemRequest._() : super();
  factory UserConsumeItemRequest({
    $0.ItemId itemId,
    $0.UserId userId,
  }) {
    final _result = create();
    if (itemId != null) {
      _result.itemId = itemId;
    }
    if (userId != null) {
      _result.userId = userId;
    }
    return _result;
  }
  factory UserConsumeItemRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UserConsumeItemRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UserConsumeItemRequest clone() => UserConsumeItemRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UserConsumeItemRequest copyWith(void Function(UserConsumeItemRequest) updates) => super.copyWith((message) => updates(message as UserConsumeItemRequest)) as UserConsumeItemRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UserConsumeItemRequest create() => UserConsumeItemRequest._();
  UserConsumeItemRequest createEmptyInstance() => create();
  static $pb.PbList<UserConsumeItemRequest> createRepeated() => $pb.PbList<UserConsumeItemRequest>();
  @$core.pragma('dart2js:noInline')
  static UserConsumeItemRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UserConsumeItemRequest>(create);
  static UserConsumeItemRequest _defaultInstance;

  @$pb.TagNumber(1)
  $0.ItemId get itemId => $_getN(0);
  @$pb.TagNumber(1)
  set itemId($0.ItemId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasItemId() => $_has(0);
  @$pb.TagNumber(1)
  void clearItemId() => clearField(1);
  @$pb.TagNumber(1)
  $0.ItemId ensureItemId() => $_ensure(0);

  @$pb.TagNumber(2)
  $0.UserId get userId => $_getN(1);
  @$pb.TagNumber(2)
  set userId($0.UserId v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasUserId() => $_has(1);
  @$pb.TagNumber(2)
  void clearUserId() => clearField(2);
  @$pb.TagNumber(2)
  $0.UserId ensureUserId() => $_ensure(1);
}

enum InteractionServiceCall_Method {
  consumeItem, 
  notSet
}

class InteractionServiceCall extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, InteractionServiceCall_Method> _InteractionServiceCall_MethodByTag = {
    2 : InteractionServiceCall_Method.consumeItem,
    0 : InteractionServiceCall_Method.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InteractionServiceCall', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'interaction'), createEmptyInstance: create)
    ..oo(0, [2])
    ..aOM<$0.MetaData>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'metaData', protoName: 'metaData', subBuilder: $0.MetaData.create)
    ..aOM<MonConsumeItemRequest>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'consumeItem', protoName: 'consumeItem', subBuilder: MonConsumeItemRequest.create)
    ..hasRequiredFields = false
  ;

  InteractionServiceCall._() : super();
  factory InteractionServiceCall({
    $0.MetaData metaData,
    MonConsumeItemRequest consumeItem,
  }) {
    final _result = create();
    if (metaData != null) {
      _result.metaData = metaData;
    }
    if (consumeItem != null) {
      _result.consumeItem = consumeItem;
    }
    return _result;
  }
  factory InteractionServiceCall.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InteractionServiceCall.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InteractionServiceCall clone() => InteractionServiceCall()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InteractionServiceCall copyWith(void Function(InteractionServiceCall) updates) => super.copyWith((message) => updates(message as InteractionServiceCall)) as InteractionServiceCall; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InteractionServiceCall create() => InteractionServiceCall._();
  InteractionServiceCall createEmptyInstance() => create();
  static $pb.PbList<InteractionServiceCall> createRepeated() => $pb.PbList<InteractionServiceCall>();
  @$core.pragma('dart2js:noInline')
  static InteractionServiceCall getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InteractionServiceCall>(create);
  static InteractionServiceCall _defaultInstance;

  InteractionServiceCall_Method whichMethod() => _InteractionServiceCall_MethodByTag[$_whichOneof(0)];
  void clearMethod() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $0.MetaData get metaData => $_getN(0);
  @$pb.TagNumber(1)
  set metaData($0.MetaData v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMetaData() => $_has(0);
  @$pb.TagNumber(1)
  void clearMetaData() => clearField(1);
  @$pb.TagNumber(1)
  $0.MetaData ensureMetaData() => $_ensure(0);

  @$pb.TagNumber(2)
  MonConsumeItemRequest get consumeItem => $_getN(1);
  @$pb.TagNumber(2)
  set consumeItem(MonConsumeItemRequest v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasConsumeItem() => $_has(1);
  @$pb.TagNumber(2)
  void clearConsumeItem() => clearField(2);
  @$pb.TagNumber(2)
  MonConsumeItemRequest ensureConsumeItem() => $_ensure(1);
}

class FeedMonResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'FeedMonResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'interaction'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  FeedMonResponse._() : super();
  factory FeedMonResponse() => create();
  factory FeedMonResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory FeedMonResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  FeedMonResponse clone() => FeedMonResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  FeedMonResponse copyWith(void Function(FeedMonResponse) updates) => super.copyWith((message) => updates(message as FeedMonResponse)) as FeedMonResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static FeedMonResponse create() => FeedMonResponse._();
  FeedMonResponse createEmptyInstance() => create();
  static $pb.PbList<FeedMonResponse> createRepeated() => $pb.PbList<FeedMonResponse>();
  @$core.pragma('dart2js:noInline')
  static FeedMonResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<FeedMonResponse>(create);
  static FeedMonResponse _defaultInstance;
}

enum InteractionServiceResponse_Data {
  status, 
  notSet
}

class InteractionServiceResponse extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, InteractionServiceResponse_Data> _InteractionServiceResponse_DataByTag = {
    2 : InteractionServiceResponse_Data.status,
    0 : InteractionServiceResponse_Data.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'InteractionServiceResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'interaction'), createEmptyInstance: create)
    ..oo(0, [2])
    ..aOM<$0.MetaData>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'metaData', protoName: 'metaData', subBuilder: $0.MetaData.create)
    ..aOM<$0.ResponseStatus>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', subBuilder: $0.ResponseStatus.create)
    ..hasRequiredFields = false
  ;

  InteractionServiceResponse._() : super();
  factory InteractionServiceResponse({
    $0.MetaData metaData,
    $0.ResponseStatus status,
  }) {
    final _result = create();
    if (metaData != null) {
      _result.metaData = metaData;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory InteractionServiceResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory InteractionServiceResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  InteractionServiceResponse clone() => InteractionServiceResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  InteractionServiceResponse copyWith(void Function(InteractionServiceResponse) updates) => super.copyWith((message) => updates(message as InteractionServiceResponse)) as InteractionServiceResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static InteractionServiceResponse create() => InteractionServiceResponse._();
  InteractionServiceResponse createEmptyInstance() => create();
  static $pb.PbList<InteractionServiceResponse> createRepeated() => $pb.PbList<InteractionServiceResponse>();
  @$core.pragma('dart2js:noInline')
  static InteractionServiceResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<InteractionServiceResponse>(create);
  static InteractionServiceResponse _defaultInstance;

  InteractionServiceResponse_Data whichData() => _InteractionServiceResponse_DataByTag[$_whichOneof(0)];
  void clearData() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $0.MetaData get metaData => $_getN(0);
  @$pb.TagNumber(1)
  set metaData($0.MetaData v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMetaData() => $_has(0);
  @$pb.TagNumber(1)
  void clearMetaData() => clearField(1);
  @$pb.TagNumber(1)
  $0.MetaData ensureMetaData() => $_ensure(0);

  @$pb.TagNumber(2)
  $0.ResponseStatus get status => $_getN(1);
  @$pb.TagNumber(2)
  set status($0.ResponseStatus v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
  @$pb.TagNumber(2)
  $0.ResponseStatus ensureStatus() => $_ensure(1);
}

