///
//  Generated code. Do not modify.
//  source: chat.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use messageDescriptor instead')
const Message$json = const {
  '1': 'Message',
  '2': const [
    const {'1': 'Sender', '3': 1, '4': 1, '5': 11, '6': '.common.UserId', '10': 'Sender'},
    const {'1': 'roomId', '3': 2, '4': 1, '5': 11, '6': '.chat.RoomId', '10': 'roomId'},
    const {'1': 'body', '3': 3, '4': 1, '5': 9, '10': 'body'},
    const {'1': 'sent_time', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'sentTime'},
  ],
};

/// Descriptor for `Message`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageDescriptor = $convert.base64Decode('CgdNZXNzYWdlEiYKBlNlbmRlchgBIAEoCzIOLmNvbW1vbi5Vc2VySWRSBlNlbmRlchIkCgZyb29tSWQYAiABKAsyDC5jaGF0LlJvb21JZFIGcm9vbUlkEhIKBGJvZHkYAyABKAlSBGJvZHkSNwoJc2VudF90aW1lGAQgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIIc2VudFRpbWU=');
@$core.Deprecated('Use roomIdDescriptor instead')
const RoomId$json = const {
  '1': 'RoomId',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
  ],
};

/// Descriptor for `RoomId`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List roomIdDescriptor = $convert.base64Decode('CgZSb29tSWQSDgoCaWQYASABKAVSAmlk');
@$core.Deprecated('Use sendResponseDescriptor instead')
const SendResponse$json = const {
  '1': 'SendResponse',
};

/// Descriptor for `SendResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendResponseDescriptor = $convert.base64Decode('CgxTZW5kUmVzcG9uc2U=');
@$core.Deprecated('Use leaveResponseDescriptor instead')
const LeaveResponse$json = const {
  '1': 'LeaveResponse',
};

/// Descriptor for `LeaveResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List leaveResponseDescriptor = $convert.base64Decode('Cg1MZWF2ZVJlc3BvbnNl');
@$core.Deprecated('Use joinResponseDescriptor instead')
const JoinResponse$json = const {
  '1': 'JoinResponse',
};

/// Descriptor for `JoinResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List joinResponseDescriptor = $convert.base64Decode('CgxKb2luUmVzcG9uc2U=');
@$core.Deprecated('Use joinEventDescriptor instead')
const JoinEvent$json = const {
  '1': 'JoinEvent',
  '2': const [
    const {'1': 'roomId', '3': 1, '4': 1, '5': 11, '6': '.chat.RoomId', '10': 'roomId'},
    const {'1': 'userId', '3': 2, '4': 1, '5': 11, '6': '.common.UserId', '10': 'userId'},
  ],
};

/// Descriptor for `JoinEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List joinEventDescriptor = $convert.base64Decode('CglKb2luRXZlbnQSJAoGcm9vbUlkGAEgASgLMgwuY2hhdC5Sb29tSWRSBnJvb21JZBImCgZ1c2VySWQYAiABKAsyDi5jb21tb24uVXNlcklkUgZ1c2VySWQ=');
@$core.Deprecated('Use leaveEventDescriptor instead')
const LeaveEvent$json = const {
  '1': 'LeaveEvent',
  '2': const [
    const {'1': 'roomId', '3': 1, '4': 1, '5': 11, '6': '.chat.RoomId', '10': 'roomId'},
    const {'1': 'userId', '3': 2, '4': 1, '5': 11, '6': '.common.UserId', '10': 'userId'},
  ],
};

/// Descriptor for `LeaveEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List leaveEventDescriptor = $convert.base64Decode('CgpMZWF2ZUV2ZW50EiQKBnJvb21JZBgBIAEoCzIMLmNoYXQuUm9vbUlkUgZyb29tSWQSJgoGdXNlcklkGAIgASgLMg4uY29tbW9uLlVzZXJJZFIGdXNlcklk');
@$core.Deprecated('Use joinRequestDescriptor instead')
const JoinRequest$json = const {
  '1': 'JoinRequest',
  '2': const [
    const {'1': 'roomId', '3': 1, '4': 1, '5': 11, '6': '.chat.RoomId', '10': 'roomId'},
    const {'1': 'userId', '3': 2, '4': 1, '5': 11, '6': '.common.UserId', '10': 'userId'},
  ],
};

/// Descriptor for `JoinRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List joinRequestDescriptor = $convert.base64Decode('CgtKb2luUmVxdWVzdBIkCgZyb29tSWQYASABKAsyDC5jaGF0LlJvb21JZFIGcm9vbUlkEiYKBnVzZXJJZBgCIAEoCzIOLmNvbW1vbi5Vc2VySWRSBnVzZXJJZA==');
@$core.Deprecated('Use leaveRequestDescriptor instead')
const LeaveRequest$json = const {
  '1': 'LeaveRequest',
  '2': const [
    const {'1': 'roomId', '3': 1, '4': 1, '5': 11, '6': '.chat.RoomId', '10': 'roomId'},
    const {'1': 'userId', '3': 2, '4': 1, '5': 11, '6': '.common.UserId', '10': 'userId'},
  ],
};

/// Descriptor for `LeaveRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List leaveRequestDescriptor = $convert.base64Decode('CgxMZWF2ZVJlcXVlc3QSJAoGcm9vbUlkGAEgASgLMgwuY2hhdC5Sb29tSWRSBnJvb21JZBImCgZ1c2VySWQYAiABKAsyDi5jb21tb24uVXNlcklkUgZ1c2VySWQ=');
@$core.Deprecated('Use lastMessagesRequestDescriptor instead')
const LastMessagesRequest$json = const {
  '1': 'LastMessagesRequest',
  '2': const [
    const {'1': 'roomId', '3': 1, '4': 1, '5': 11, '6': '.chat.RoomId', '10': 'roomId'},
    const {'1': 'count', '3': 2, '4': 1, '5': 5, '10': 'count'},
  ],
};

/// Descriptor for `LastMessagesRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List lastMessagesRequestDescriptor = $convert.base64Decode('ChNMYXN0TWVzc2FnZXNSZXF1ZXN0EiQKBnJvb21JZBgBIAEoCzIMLmNoYXQuUm9vbUlkUgZyb29tSWQSFAoFY291bnQYAiABKAVSBWNvdW50');
@$core.Deprecated('Use roomsDescriptor instead')
const Rooms$json = const {
  '1': 'Rooms',
  '2': const [
    const {'1': 'rooms', '3': 1, '4': 3, '5': 11, '6': '.chat.RoomId', '10': 'rooms'},
  ],
};

/// Descriptor for `Rooms`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List roomsDescriptor = $convert.base64Decode('CgVSb29tcxIiCgVyb29tcxgBIAMoCzIMLmNoYXQuUm9vbUlkUgVyb29tcw==');
@$core.Deprecated('Use eventDescriptor instead')
const Event$json = const {
  '1': 'Event',
  '2': const [
    const {'1': 'login', '3': 1, '4': 1, '5': 11, '6': '.chat.JoinEvent', '9': 0, '10': 'login'},
    const {'1': 'logout', '3': 2, '4': 1, '5': 11, '6': '.chat.LeaveEvent', '9': 0, '10': 'logout'},
    const {'1': 'msg', '3': 3, '4': 1, '5': 11, '6': '.chat.Message', '9': 0, '10': 'msg'},
  ],
  '8': const [
    const {'1': 'event'},
  ],
};

/// Descriptor for `Event`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List eventDescriptor = $convert.base64Decode('CgVFdmVudBInCgVsb2dpbhgBIAEoCzIPLmNoYXQuSm9pbkV2ZW50SABSBWxvZ2luEioKBmxvZ291dBgCIAEoCzIQLmNoYXQuTGVhdmVFdmVudEgAUgZsb2dvdXQSIQoDbXNnGAMgASgLMg0uY2hhdC5NZXNzYWdlSABSA21zZ0IHCgVldmVudA==');
@$core.Deprecated('Use roomRequestDescriptor instead')
const RoomRequest$json = const {
  '1': 'RoomRequest',
};

/// Descriptor for `RoomRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List roomRequestDescriptor = $convert.base64Decode('CgtSb29tUmVxdWVzdA==');
@$core.Deprecated('Use messagesDescriptor instead')
const Messages$json = const {
  '1': 'Messages',
  '2': const [
    const {'1': 'messages', '3': 1, '4': 3, '5': 11, '6': '.chat.Message', '10': 'messages'},
  ],
};

/// Descriptor for `Messages`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messagesDescriptor = $convert.base64Decode('CghNZXNzYWdlcxIpCghtZXNzYWdlcxgBIAMoCzINLmNoYXQuTWVzc2FnZVIIbWVzc2FnZXM=');
