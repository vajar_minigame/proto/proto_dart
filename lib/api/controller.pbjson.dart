///
//  Generated code. Do not modify.
//  source: controller.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use eventDescriptor instead')
const Event$json = const {
  '1': 'Event',
  '2': const [
    const {'1': 'monEvent', '3': 1, '4': 1, '5': 11, '6': '.monster.MonsterEvent', '9': 0, '10': 'monEvent'},
    const {'1': 'itemEvent', '3': 2, '4': 1, '5': 11, '6': '.item.ItemEvent', '9': 0, '10': 'itemEvent'},
    const {'1': 'questEvent', '3': 3, '4': 1, '5': 11, '6': '.quest.QuestEvent', '9': 0, '10': 'questEvent'},
    const {'1': 'battleEvent', '3': 4, '4': 1, '5': 11, '6': '.battle.BattleEvent', '9': 0, '10': 'battleEvent'},
  ],
  '8': const [
    const {'1': 'event'},
  ],
};

/// Descriptor for `Event`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List eventDescriptor = $convert.base64Decode('CgVFdmVudBIzCghtb25FdmVudBgBIAEoCzIVLm1vbnN0ZXIuTW9uc3RlckV2ZW50SABSCG1vbkV2ZW50Ei8KCWl0ZW1FdmVudBgCIAEoCzIPLml0ZW0uSXRlbUV2ZW50SABSCWl0ZW1FdmVudBIzCgpxdWVzdEV2ZW50GAMgASgLMhEucXVlc3QuUXVlc3RFdmVudEgAUgpxdWVzdEV2ZW50EjcKC2JhdHRsZUV2ZW50GAQgASgLMhMuYmF0dGxlLkJhdHRsZUV2ZW50SABSC2JhdHRsZUV2ZW50QgcKBWV2ZW50');
@$core.Deprecated('Use funcCallDescriptor instead')
const FuncCall$json = const {
  '1': 'FuncCall',
  '2': const [
    const {'1': 'auth_service_call', '3': 1, '4': 1, '5': 11, '6': '.auth.AuthServiceCall', '9': 0, '10': 'authServiceCall'},
    const {'1': 'mon_service_call', '3': 2, '4': 1, '5': 11, '6': '.monster.MonServiceCall', '9': 0, '10': 'monServiceCall'},
    const {'1': 'user_service_call', '3': 3, '4': 1, '5': 11, '6': '.user.UserServiceCall', '9': 0, '10': 'userServiceCall'},
    const {'1': 'inv_service_call', '3': 4, '4': 1, '5': 11, '6': '.item.InventoryServiceCall', '9': 0, '10': 'invServiceCall'},
    const {'1': 'interact_service_call', '3': 5, '4': 1, '5': 11, '6': '.interaction.InteractionServiceCall', '9': 0, '10': 'interactServiceCall'},
    const {'1': 'quest_service_call', '3': 6, '4': 1, '5': 11, '6': '.quest.QuestServiceCall', '9': 0, '10': 'questServiceCall'},
  ],
  '8': const [
    const {'1': 'message'},
  ],
};

/// Descriptor for `FuncCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List funcCallDescriptor = $convert.base64Decode('CghGdW5jQ2FsbBJDChFhdXRoX3NlcnZpY2VfY2FsbBgBIAEoCzIVLmF1dGguQXV0aFNlcnZpY2VDYWxsSABSD2F1dGhTZXJ2aWNlQ2FsbBJDChBtb25fc2VydmljZV9jYWxsGAIgASgLMhcubW9uc3Rlci5Nb25TZXJ2aWNlQ2FsbEgAUg5tb25TZXJ2aWNlQ2FsbBJDChF1c2VyX3NlcnZpY2VfY2FsbBgDIAEoCzIVLnVzZXIuVXNlclNlcnZpY2VDYWxsSABSD3VzZXJTZXJ2aWNlQ2FsbBJGChBpbnZfc2VydmljZV9jYWxsGAQgASgLMhouaXRlbS5JbnZlbnRvcnlTZXJ2aWNlQ2FsbEgAUg5pbnZTZXJ2aWNlQ2FsbBJZChVpbnRlcmFjdF9zZXJ2aWNlX2NhbGwYBSABKAsyIy5pbnRlcmFjdGlvbi5JbnRlcmFjdGlvblNlcnZpY2VDYWxsSABSE2ludGVyYWN0U2VydmljZUNhbGwSRwoScXVlc3Rfc2VydmljZV9jYWxsGAYgASgLMhcucXVlc3QuUXVlc3RTZXJ2aWNlQ2FsbEgAUhBxdWVzdFNlcnZpY2VDYWxsQgkKB21lc3NhZ2U=');
@$core.Deprecated('Use messageDescriptor instead')
const Message$json = const {
  '1': 'Message',
  '2': const [
    const {'1': 'call', '3': 1, '4': 1, '5': 11, '6': '.ctrlmsg.FuncCall', '9': 0, '10': 'call'},
    const {'1': 'response', '3': 2, '4': 1, '5': 11, '6': '.ctrlmsg.FuncResponse', '9': 0, '10': 'response'},
    const {'1': 'event', '3': 3, '4': 1, '5': 11, '6': '.ctrlmsg.Event', '9': 0, '10': 'event'},
  ],
  '8': const [
    const {'1': 'msg'},
  ],
};

/// Descriptor for `Message`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageDescriptor = $convert.base64Decode('CgdNZXNzYWdlEicKBGNhbGwYASABKAsyES5jdHJsbXNnLkZ1bmNDYWxsSABSBGNhbGwSMwoIcmVzcG9uc2UYAiABKAsyFS5jdHJsbXNnLkZ1bmNSZXNwb25zZUgAUghyZXNwb25zZRImCgVldmVudBgDIAEoCzIOLmN0cmxtc2cuRXZlbnRIAFIFZXZlbnRCBQoDbXNn');
@$core.Deprecated('Use funcResponseDescriptor instead')
const FuncResponse$json = const {
  '1': 'FuncResponse',
  '2': const [
    const {'1': 'mon_service_res', '3': 1, '4': 1, '5': 11, '6': '.monster.MonServiceResponse', '9': 0, '10': 'monServiceRes'},
    const {'1': 'auth_service_res', '3': 2, '4': 1, '5': 11, '6': '.auth.AuthServiceResponse', '9': 0, '10': 'authServiceRes'},
    const {'1': 'user_service_res', '3': 3, '4': 1, '5': 11, '6': '.user.UserServiceResponse', '9': 0, '10': 'userServiceRes'},
    const {'1': 'inv_service_res', '3': 4, '4': 1, '5': 11, '6': '.item.InventoryServiceResponse', '9': 0, '10': 'invServiceRes'},
    const {'1': 'interact_service_res', '3': 5, '4': 1, '5': 11, '6': '.interaction.InteractionServiceResponse', '9': 0, '10': 'interactServiceRes'},
    const {'1': 'quest_service_res', '3': 6, '4': 1, '5': 11, '6': '.quest.QuestServiceResponse', '9': 0, '10': 'questServiceRes'},
  ],
  '8': const [
    const {'1': 'message'},
  ],
};

/// Descriptor for `FuncResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List funcResponseDescriptor = $convert.base64Decode('CgxGdW5jUmVzcG9uc2USRQoPbW9uX3NlcnZpY2VfcmVzGAEgASgLMhsubW9uc3Rlci5Nb25TZXJ2aWNlUmVzcG9uc2VIAFINbW9uU2VydmljZVJlcxJFChBhdXRoX3NlcnZpY2VfcmVzGAIgASgLMhkuYXV0aC5BdXRoU2VydmljZVJlc3BvbnNlSABSDmF1dGhTZXJ2aWNlUmVzEkUKEHVzZXJfc2VydmljZV9yZXMYAyABKAsyGS51c2VyLlVzZXJTZXJ2aWNlUmVzcG9uc2VIAFIOdXNlclNlcnZpY2VSZXMSSAoPaW52X3NlcnZpY2VfcmVzGAQgASgLMh4uaXRlbS5JbnZlbnRvcnlTZXJ2aWNlUmVzcG9uc2VIAFINaW52U2VydmljZVJlcxJbChRpbnRlcmFjdF9zZXJ2aWNlX3JlcxgFIAEoCzInLmludGVyYWN0aW9uLkludGVyYWN0aW9uU2VydmljZVJlc3BvbnNlSABSEmludGVyYWN0U2VydmljZVJlcxJJChFxdWVzdF9zZXJ2aWNlX3JlcxgGIAEoCzIbLnF1ZXN0LlF1ZXN0U2VydmljZVJlc3BvbnNlSABSD3F1ZXN0U2VydmljZVJlc0IJCgdtZXNzYWdl');
