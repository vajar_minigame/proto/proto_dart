///
//  Generated code. Do not modify.
//  source: inventory.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'inventory.pb.dart' as $6;
import 'common.pb.dart' as $0;
export 'inventory.pb.dart';

class ItemServiceClient extends $grpc.Client {
  static final _$createItemByType = $grpc.ClientMethod<$6.ItemType, $0.ItemId>(
      '/item.ItemService/CreateItemByType',
      ($6.ItemType value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ItemId.fromBuffer(value));
  static final _$addItem = $grpc.ClientMethod<$6.Item, $0.ItemId>(
      '/item.ItemService/AddItem',
      ($6.Item value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ItemId.fromBuffer(value));
  static final _$addItemToInventory =
      $grpc.ClientMethod<$6.AddItemToInventoryCall, $0.ResponseStatus>(
          '/item.ItemService/AddItemToInventory',
          ($6.AddItemToInventoryCall value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));
  static final _$getItem = $grpc.ClientMethod<$0.ItemId, $6.Item>(
      '/item.ItemService/GetItem',
      ($0.ItemId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $6.Item.fromBuffer(value));
  static final _$getInventory = $grpc.ClientMethod<$0.UserId, $6.Inventory>(
      '/item.ItemService/GetInventory',
      ($0.UserId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $6.Inventory.fromBuffer(value));
  static final _$updateItem = $grpc.ClientMethod<$6.Item, $0.ResponseStatus>(
      '/item.ItemService/UpdateItem',
      ($6.Item value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));
  static final _$deleteItem = $grpc.ClientMethod<$0.ItemId, $0.ResponseStatus>(
      '/item.ItemService/DeleteItem',
      ($0.ItemId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));
  static final _$getItemDefinitions =
      $grpc.ClientMethod<$6.Page, $6.ItemDefList>(
          '/item.ItemService/GetItemDefinitions',
          ($6.Page value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $6.ItemDefList.fromBuffer(value));

  ItemServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options,
      $core.Iterable<$grpc.ClientInterceptor> interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.ItemId> createItemByType($6.ItemType request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$createItemByType, request, options: options);
  }

  $grpc.ResponseFuture<$0.ItemId> addItem($6.Item request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$addItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResponseStatus> addItemToInventory(
      $6.AddItemToInventoryCall request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$addItemToInventory, request, options: options);
  }

  $grpc.ResponseFuture<$6.Item> getItem($0.ItemId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getItem, request, options: options);
  }

  $grpc.ResponseFuture<$6.Inventory> getInventory($0.UserId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getInventory, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResponseStatus> updateItem($6.Item request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$updateItem, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResponseStatus> deleteItem($0.ItemId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$deleteItem, request, options: options);
  }

  $grpc.ResponseFuture<$6.ItemDefList> getItemDefinitions($6.Page request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getItemDefinitions, request, options: options);
  }
}

abstract class ItemServiceBase extends $grpc.Service {
  $core.String get $name => 'item.ItemService';

  ItemServiceBase() {
    $addMethod($grpc.ServiceMethod<$6.ItemType, $0.ItemId>(
        'CreateItemByType',
        createItemByType_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $6.ItemType.fromBuffer(value),
        ($0.ItemId value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$6.Item, $0.ItemId>(
        'AddItem',
        addItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $6.Item.fromBuffer(value),
        ($0.ItemId value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$6.AddItemToInventoryCall, $0.ResponseStatus>(
            'AddItemToInventory',
            addItemToInventory_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $6.AddItemToInventoryCall.fromBuffer(value),
            ($0.ResponseStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemId, $6.Item>(
        'GetItem',
        getItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ItemId.fromBuffer(value),
        ($6.Item value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserId, $6.Inventory>(
        'GetInventory',
        getInventory_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserId.fromBuffer(value),
        ($6.Inventory value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$6.Item, $0.ResponseStatus>(
        'UpdateItem',
        updateItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $6.Item.fromBuffer(value),
        ($0.ResponseStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ItemId, $0.ResponseStatus>(
        'DeleteItem',
        deleteItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ItemId.fromBuffer(value),
        ($0.ResponseStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$6.Page, $6.ItemDefList>(
        'GetItemDefinitions',
        getItemDefinitions_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $6.Page.fromBuffer(value),
        ($6.ItemDefList value) => value.writeToBuffer()));
  }

  $async.Future<$0.ItemId> createItemByType_Pre(
      $grpc.ServiceCall call, $async.Future<$6.ItemType> request) async {
    return createItemByType(call, await request);
  }

  $async.Future<$0.ItemId> addItem_Pre(
      $grpc.ServiceCall call, $async.Future<$6.Item> request) async {
    return addItem(call, await request);
  }

  $async.Future<$0.ResponseStatus> addItemToInventory_Pre(
      $grpc.ServiceCall call,
      $async.Future<$6.AddItemToInventoryCall> request) async {
    return addItemToInventory(call, await request);
  }

  $async.Future<$6.Item> getItem_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ItemId> request) async {
    return getItem(call, await request);
  }

  $async.Future<$6.Inventory> getInventory_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserId> request) async {
    return getInventory(call, await request);
  }

  $async.Future<$0.ResponseStatus> updateItem_Pre(
      $grpc.ServiceCall call, $async.Future<$6.Item> request) async {
    return updateItem(call, await request);
  }

  $async.Future<$0.ResponseStatus> deleteItem_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ItemId> request) async {
    return deleteItem(call, await request);
  }

  $async.Future<$6.ItemDefList> getItemDefinitions_Pre(
      $grpc.ServiceCall call, $async.Future<$6.Page> request) async {
    return getItemDefinitions(call, await request);
  }

  $async.Future<$0.ItemId> createItemByType(
      $grpc.ServiceCall call, $6.ItemType request);
  $async.Future<$0.ItemId> addItem($grpc.ServiceCall call, $6.Item request);
  $async.Future<$0.ResponseStatus> addItemToInventory(
      $grpc.ServiceCall call, $6.AddItemToInventoryCall request);
  $async.Future<$6.Item> getItem($grpc.ServiceCall call, $0.ItemId request);
  $async.Future<$6.Inventory> getInventory(
      $grpc.ServiceCall call, $0.UserId request);
  $async.Future<$0.ResponseStatus> updateItem(
      $grpc.ServiceCall call, $6.Item request);
  $async.Future<$0.ResponseStatus> deleteItem(
      $grpc.ServiceCall call, $0.ItemId request);
  $async.Future<$6.ItemDefList> getItemDefinitions(
      $grpc.ServiceCall call, $6.Page request);
}
