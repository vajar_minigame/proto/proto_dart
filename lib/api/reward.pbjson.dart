///
//  Generated code. Do not modify.
//  source: reward.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use rewardDescriptor instead')
const Reward$json = const {
  '1': 'Reward',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
    const {'1': 'activity', '3': 2, '4': 1, '5': 11, '6': '.monster.Activity', '10': 'activity'},
    const {'1': 'itemList', '3': 3, '4': 1, '5': 11, '6': '.item.ItemIdList', '10': 'itemList'},
    const {'1': 'owner', '3': 4, '4': 1, '5': 11, '6': '.common.UserId', '10': 'owner'},
    const {'1': 'collected', '3': 5, '4': 1, '5': 8, '10': 'collected'},
  ],
};

/// Descriptor for `Reward`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List rewardDescriptor = $convert.base64Decode('CgZSZXdhcmQSDgoCaWQYASABKAVSAmlkEi0KCGFjdGl2aXR5GAIgASgLMhEubW9uc3Rlci5BY3Rpdml0eVIIYWN0aXZpdHkSLAoIaXRlbUxpc3QYAyABKAsyEC5pdGVtLkl0ZW1JZExpc3RSCGl0ZW1MaXN0EiQKBW93bmVyGAQgASgLMg4uY29tbW9uLlVzZXJJZFIFb3duZXISHAoJY29sbGVjdGVkGAUgASgIUgljb2xsZWN0ZWQ=');
@$core.Deprecated('Use rewardListDescriptor instead')
const RewardList$json = const {
  '1': 'RewardList',
  '2': const [
    const {'1': 'reward', '3': 1, '4': 3, '5': 11, '6': '.reward.Reward', '10': 'reward'},
  ],
};

/// Descriptor for `RewardList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List rewardListDescriptor = $convert.base64Decode('CgpSZXdhcmRMaXN0EiYKBnJld2FyZBgBIAMoCzIOLnJld2FyZC5SZXdhcmRSBnJld2FyZA==');
