///
//  Generated code. Do not modify.
//  source: auth.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'auth.pb.dart' as $2;
export 'auth.pb.dart';

class AuthClient extends $grpc.Client {
  static final _$login = $grpc.ClientMethod<$2.LoginCall, $2.LoginResponse>(
      '/auth.Auth/Login',
      ($2.LoginCall value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.LoginResponse.fromBuffer(value));
  static final _$logout = $grpc.ClientMethod<$2.LogoutCall, $2.LogoutResponse>(
      '/auth.Auth/Logout',
      ($2.LogoutCall value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.LogoutResponse.fromBuffer(value));

  AuthClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options,
      $core.Iterable<$grpc.ClientInterceptor> interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$2.LoginResponse> login($2.LoginCall request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$login, request, options: options);
  }

  $grpc.ResponseFuture<$2.LogoutResponse> logout($2.LogoutCall request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$logout, request, options: options);
  }
}

abstract class AuthServiceBase extends $grpc.Service {
  $core.String get $name => 'auth.Auth';

  AuthServiceBase() {
    $addMethod($grpc.ServiceMethod<$2.LoginCall, $2.LoginResponse>(
        'Login',
        login_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.LoginCall.fromBuffer(value),
        ($2.LoginResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.LogoutCall, $2.LogoutResponse>(
        'Logout',
        logout_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.LogoutCall.fromBuffer(value),
        ($2.LogoutResponse value) => value.writeToBuffer()));
  }

  $async.Future<$2.LoginResponse> login_Pre(
      $grpc.ServiceCall call, $async.Future<$2.LoginCall> request) async {
    return login(call, await request);
  }

  $async.Future<$2.LogoutResponse> logout_Pre(
      $grpc.ServiceCall call, $async.Future<$2.LogoutCall> request) async {
    return logout(call, await request);
  }

  $async.Future<$2.LoginResponse> login(
      $grpc.ServiceCall call, $2.LoginCall request);
  $async.Future<$2.LogoutResponse> logout(
      $grpc.ServiceCall call, $2.LogoutCall request);
}
