///
//  Generated code. Do not modify.
//  source: interactions.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use monConsumeItemRequestDescriptor instead')
const MonConsumeItemRequest$json = const {
  '1': 'MonConsumeItemRequest',
  '2': const [
    const {'1': 'itemId', '3': 1, '4': 1, '5': 11, '6': '.common.ItemId', '10': 'itemId'},
    const {'1': 'monId', '3': 2, '4': 1, '5': 11, '6': '.common.MonId', '10': 'monId'},
  ],
};

/// Descriptor for `MonConsumeItemRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List monConsumeItemRequestDescriptor = $convert.base64Decode('ChVNb25Db25zdW1lSXRlbVJlcXVlc3QSJgoGaXRlbUlkGAEgASgLMg4uY29tbW9uLkl0ZW1JZFIGaXRlbUlkEiMKBW1vbklkGAIgASgLMg0uY29tbW9uLk1vbklkUgVtb25JZA==');
@$core.Deprecated('Use userConsumeItemRequestDescriptor instead')
const UserConsumeItemRequest$json = const {
  '1': 'UserConsumeItemRequest',
  '2': const [
    const {'1': 'itemId', '3': 1, '4': 1, '5': 11, '6': '.common.ItemId', '10': 'itemId'},
    const {'1': 'userId', '3': 2, '4': 1, '5': 11, '6': '.common.UserId', '10': 'userId'},
  ],
};

/// Descriptor for `UserConsumeItemRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userConsumeItemRequestDescriptor = $convert.base64Decode('ChZVc2VyQ29uc3VtZUl0ZW1SZXF1ZXN0EiYKBml0ZW1JZBgBIAEoCzIOLmNvbW1vbi5JdGVtSWRSBml0ZW1JZBImCgZ1c2VySWQYAiABKAsyDi5jb21tb24uVXNlcklkUgZ1c2VySWQ=');
@$core.Deprecated('Use interactionServiceCallDescriptor instead')
const InteractionServiceCall$json = const {
  '1': 'InteractionServiceCall',
  '2': const [
    const {'1': 'metaData', '3': 1, '4': 1, '5': 11, '6': '.common.MetaData', '10': 'metaData'},
    const {'1': 'consumeItem', '3': 2, '4': 1, '5': 11, '6': '.interaction.MonConsumeItemRequest', '9': 0, '10': 'consumeItem'},
  ],
  '8': const [
    const {'1': 'method'},
  ],
};

/// Descriptor for `InteractionServiceCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List interactionServiceCallDescriptor = $convert.base64Decode('ChZJbnRlcmFjdGlvblNlcnZpY2VDYWxsEiwKCG1ldGFEYXRhGAEgASgLMhAuY29tbW9uLk1ldGFEYXRhUghtZXRhRGF0YRJGCgtjb25zdW1lSXRlbRgCIAEoCzIiLmludGVyYWN0aW9uLk1vbkNvbnN1bWVJdGVtUmVxdWVzdEgAUgtjb25zdW1lSXRlbUIICgZtZXRob2Q=');
@$core.Deprecated('Use feedMonResponseDescriptor instead')
const FeedMonResponse$json = const {
  '1': 'FeedMonResponse',
};

/// Descriptor for `FeedMonResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List feedMonResponseDescriptor = $convert.base64Decode('Cg9GZWVkTW9uUmVzcG9uc2U=');
@$core.Deprecated('Use interactionServiceResponseDescriptor instead')
const InteractionServiceResponse$json = const {
  '1': 'InteractionServiceResponse',
  '2': const [
    const {'1': 'metaData', '3': 1, '4': 1, '5': 11, '6': '.common.MetaData', '10': 'metaData'},
    const {'1': 'status', '3': 2, '4': 1, '5': 11, '6': '.common.ResponseStatus', '9': 0, '10': 'status'},
  ],
  '8': const [
    const {'1': 'data'},
  ],
};

/// Descriptor for `InteractionServiceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List interactionServiceResponseDescriptor = $convert.base64Decode('ChpJbnRlcmFjdGlvblNlcnZpY2VSZXNwb25zZRIsCghtZXRhRGF0YRgBIAEoCzIQLmNvbW1vbi5NZXRhRGF0YVIIbWV0YURhdGESMAoGc3RhdHVzGAIgASgLMhYuY29tbW9uLlJlc3BvbnNlU3RhdHVzSABSBnN0YXR1c0IGCgRkYXRh');
