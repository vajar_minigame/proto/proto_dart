///
//  Generated code. Do not modify.
//  source: battle.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'common.pb.dart' as $0;
import 'google/protobuf/timestamp.pb.dart' as $12;
import 'monster.pb.dart' as $3;

class Team extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Team', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..pc<$0.UserId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'users', $pb.PbFieldType.PM, subBuilder: $0.UserId.create)
    ..pc<$0.MonId>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'mons', $pb.PbFieldType.PM, subBuilder: $0.MonId.create)
    ..hasRequiredFields = false
  ;

  Team._() : super();
  factory Team({
    $core.Iterable<$0.UserId> users,
    $core.Iterable<$0.MonId> mons,
  }) {
    final _result = create();
    if (users != null) {
      _result.users.addAll(users);
    }
    if (mons != null) {
      _result.mons.addAll(mons);
    }
    return _result;
  }
  factory Team.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Team.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Team clone() => Team()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Team copyWith(void Function(Team) updates) => super.copyWith((message) => updates(message as Team)) as Team; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Team create() => Team._();
  Team createEmptyInstance() => create();
  static $pb.PbList<Team> createRepeated() => $pb.PbList<Team>();
  @$core.pragma('dart2js:noInline')
  static Team getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Team>(create);
  static Team _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$0.UserId> get users => $_getList(0);

  @$pb.TagNumber(2)
  $core.List<$0.MonId> get mons => $_getList(1);
}

enum BattleState_State {
  activeBattle, 
  end, 
  notSet
}

class BattleState extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, BattleState_State> _BattleState_StateByTag = {
    1 : BattleState_State.activeBattle,
    2 : BattleState_State.end,
    0 : BattleState_State.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BattleState', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOM<ActiveBattle>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'activeBattle', protoName: 'activeBattle', subBuilder: ActiveBattle.create)
    ..aOM<BattleEnded>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'end', subBuilder: BattleEnded.create)
    ..hasRequiredFields = false
  ;

  BattleState._() : super();
  factory BattleState({
    ActiveBattle activeBattle,
    BattleEnded end,
  }) {
    final _result = create();
    if (activeBattle != null) {
      _result.activeBattle = activeBattle;
    }
    if (end != null) {
      _result.end = end;
    }
    return _result;
  }
  factory BattleState.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BattleState.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BattleState clone() => BattleState()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BattleState copyWith(void Function(BattleState) updates) => super.copyWith((message) => updates(message as BattleState)) as BattleState; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BattleState create() => BattleState._();
  BattleState createEmptyInstance() => create();
  static $pb.PbList<BattleState> createRepeated() => $pb.PbList<BattleState>();
  @$core.pragma('dart2js:noInline')
  static BattleState getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BattleState>(create);
  static BattleState _defaultInstance;

  BattleState_State whichState() => _BattleState_StateByTag[$_whichOneof(0)];
  void clearState() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  ActiveBattle get activeBattle => $_getN(0);
  @$pb.TagNumber(1)
  set activeBattle(ActiveBattle v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasActiveBattle() => $_has(0);
  @$pb.TagNumber(1)
  void clearActiveBattle() => clearField(1);
  @$pb.TagNumber(1)
  ActiveBattle ensureActiveBattle() => $_ensure(0);

  @$pb.TagNumber(2)
  BattleEnded get end => $_getN(1);
  @$pb.TagNumber(2)
  set end(BattleEnded v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasEnd() => $_has(1);
  @$pb.TagNumber(2)
  void clearEnd() => clearField(2);
  @$pb.TagNumber(2)
  BattleEnded ensureEnd() => $_ensure(1);
}

class Battle extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Battle', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Id', $pb.PbFieldType.O3, protoName: 'Id')
    ..pc<Team>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'teams', $pb.PbFieldType.PM, subBuilder: Team.create)
    ..aOM<BattleState>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'state', subBuilder: BattleState.create)
    ..aOM<$12.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastUpdate', subBuilder: $12.Timestamp.create)
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isTest')
    ..hasRequiredFields = false
  ;

  Battle._() : super();
  factory Battle({
    $core.int id,
    $core.Iterable<Team> teams,
    BattleState state,
    $12.Timestamp lastUpdate,
    $core.bool isTest,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (teams != null) {
      _result.teams.addAll(teams);
    }
    if (state != null) {
      _result.state = state;
    }
    if (lastUpdate != null) {
      _result.lastUpdate = lastUpdate;
    }
    if (isTest != null) {
      _result.isTest = isTest;
    }
    return _result;
  }
  factory Battle.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Battle.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Battle clone() => Battle()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Battle copyWith(void Function(Battle) updates) => super.copyWith((message) => updates(message as Battle)) as Battle; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Battle create() => Battle._();
  Battle createEmptyInstance() => create();
  static $pb.PbList<Battle> createRepeated() => $pb.PbList<Battle>();
  @$core.pragma('dart2js:noInline')
  static Battle getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Battle>(create);
  static Battle _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get id => $_getIZ(0);
  @$pb.TagNumber(1)
  set id($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Team> get teams => $_getList(1);

  @$pb.TagNumber(3)
  BattleState get state => $_getN(2);
  @$pb.TagNumber(3)
  set state(BattleState v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasState() => $_has(2);
  @$pb.TagNumber(3)
  void clearState() => clearField(3);
  @$pb.TagNumber(3)
  BattleState ensureState() => $_ensure(2);

  @$pb.TagNumber(4)
  $12.Timestamp get lastUpdate => $_getN(3);
  @$pb.TagNumber(4)
  set lastUpdate($12.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasLastUpdate() => $_has(3);
  @$pb.TagNumber(4)
  void clearLastUpdate() => clearField(4);
  @$pb.TagNumber(4)
  $12.Timestamp ensureLastUpdate() => $_ensure(3);

  @$pb.TagNumber(5)
  $core.bool get isTest => $_getBF(4);
  @$pb.TagNumber(5)
  set isTest($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasIsTest() => $_has(4);
  @$pb.TagNumber(5)
  void clearIsTest() => clearField(5);
}

class ActiveBattle extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ActiveBattle', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..aOM<$12.Timestamp>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startTime', subBuilder: $12.Timestamp.create)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'turnCount', $pb.PbFieldType.O3, protoName: 'turnCount')
    ..pc<$0.MonId>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'turnQueue', $pb.PbFieldType.PM, protoName: 'turnQueue', subBuilder: $0.MonId.create)
    ..aOM<$12.Timestamp>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastAction', subBuilder: $12.Timestamp.create)
    ..hasRequiredFields = false
  ;

  ActiveBattle._() : super();
  factory ActiveBattle({
    $12.Timestamp startTime,
    $core.int turnCount,
    $core.Iterable<$0.MonId> turnQueue,
    $12.Timestamp lastAction,
  }) {
    final _result = create();
    if (startTime != null) {
      _result.startTime = startTime;
    }
    if (turnCount != null) {
      _result.turnCount = turnCount;
    }
    if (turnQueue != null) {
      _result.turnQueue.addAll(turnQueue);
    }
    if (lastAction != null) {
      _result.lastAction = lastAction;
    }
    return _result;
  }
  factory ActiveBattle.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ActiveBattle.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ActiveBattle clone() => ActiveBattle()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ActiveBattle copyWith(void Function(ActiveBattle) updates) => super.copyWith((message) => updates(message as ActiveBattle)) as ActiveBattle; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ActiveBattle create() => ActiveBattle._();
  ActiveBattle createEmptyInstance() => create();
  static $pb.PbList<ActiveBattle> createRepeated() => $pb.PbList<ActiveBattle>();
  @$core.pragma('dart2js:noInline')
  static ActiveBattle getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ActiveBattle>(create);
  static ActiveBattle _defaultInstance;

  @$pb.TagNumber(1)
  $12.Timestamp get startTime => $_getN(0);
  @$pb.TagNumber(1)
  set startTime($12.Timestamp v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStartTime() => $_has(0);
  @$pb.TagNumber(1)
  void clearStartTime() => clearField(1);
  @$pb.TagNumber(1)
  $12.Timestamp ensureStartTime() => $_ensure(0);

  @$pb.TagNumber(3)
  $core.int get turnCount => $_getIZ(1);
  @$pb.TagNumber(3)
  set turnCount($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(3)
  $core.bool hasTurnCount() => $_has(1);
  @$pb.TagNumber(3)
  void clearTurnCount() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$0.MonId> get turnQueue => $_getList(2);

  @$pb.TagNumber(5)
  $12.Timestamp get lastAction => $_getN(3);
  @$pb.TagNumber(5)
  set lastAction($12.Timestamp v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasLastAction() => $_has(3);
  @$pb.TagNumber(5)
  void clearLastAction() => clearField(5);
  @$pb.TagNumber(5)
  $12.Timestamp ensureLastAction() => $_ensure(3);
}

class Report extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Report', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..pPS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'report')
    ..hasRequiredFields = false
  ;

  Report._() : super();
  factory Report({
    $core.Iterable<$core.String> report,
  }) {
    final _result = create();
    if (report != null) {
      _result.report.addAll(report);
    }
    return _result;
  }
  factory Report.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Report.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Report clone() => Report()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Report copyWith(void Function(Report) updates) => super.copyWith((message) => updates(message as Report)) as Report; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Report create() => Report._();
  Report createEmptyInstance() => create();
  static $pb.PbList<Report> createRepeated() => $pb.PbList<Report>();
  @$core.pragma('dart2js:noInline')
  static Report getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Report>(create);
  static Report _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.String> get report => $_getList(0);
}

class TurnQueueEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TurnQueueEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..aOM<Battle>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'b', subBuilder: Battle.create)
    ..hasRequiredFields = false
  ;

  TurnQueueEvent._() : super();
  factory TurnQueueEvent({
    Battle b,
  }) {
    final _result = create();
    if (b != null) {
      _result.b = b;
    }
    return _result;
  }
  factory TurnQueueEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TurnQueueEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TurnQueueEvent clone() => TurnQueueEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TurnQueueEvent copyWith(void Function(TurnQueueEvent) updates) => super.copyWith((message) => updates(message as TurnQueueEvent)) as TurnQueueEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TurnQueueEvent create() => TurnQueueEvent._();
  TurnQueueEvent createEmptyInstance() => create();
  static $pb.PbList<TurnQueueEvent> createRepeated() => $pb.PbList<TurnQueueEvent>();
  @$core.pragma('dart2js:noInline')
  static TurnQueueEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TurnQueueEvent>(create);
  static TurnQueueEvent _defaultInstance;

  @$pb.TagNumber(1)
  Battle get b => $_getN(0);
  @$pb.TagNumber(1)
  set b(Battle v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasB() => $_has(0);
  @$pb.TagNumber(1)
  void clearB() => clearField(1);
  @$pb.TagNumber(1)
  Battle ensureB() => $_ensure(0);
}

class BattleStartedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BattleStartedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..aOM<Battle>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'b', subBuilder: Battle.create)
    ..hasRequiredFields = false
  ;

  BattleStartedEvent._() : super();
  factory BattleStartedEvent({
    Battle b,
  }) {
    final _result = create();
    if (b != null) {
      _result.b = b;
    }
    return _result;
  }
  factory BattleStartedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BattleStartedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BattleStartedEvent clone() => BattleStartedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BattleStartedEvent copyWith(void Function(BattleStartedEvent) updates) => super.copyWith((message) => updates(message as BattleStartedEvent)) as BattleStartedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BattleStartedEvent create() => BattleStartedEvent._();
  BattleStartedEvent createEmptyInstance() => create();
  static $pb.PbList<BattleStartedEvent> createRepeated() => $pb.PbList<BattleStartedEvent>();
  @$core.pragma('dart2js:noInline')
  static BattleStartedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BattleStartedEvent>(create);
  static BattleStartedEvent _defaultInstance;

  @$pb.TagNumber(1)
  Battle get b => $_getN(0);
  @$pb.TagNumber(1)
  set b(Battle v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasB() => $_has(0);
  @$pb.TagNumber(1)
  void clearB() => clearField(1);
  @$pb.TagNumber(1)
  Battle ensureB() => $_ensure(0);
}

class BattleEndedEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BattleEndedEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..aOM<Report>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ev', subBuilder: Report.create)
    ..aOM<Team>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'winningTeam', protoName: 'winningTeam', subBuilder: Team.create)
    ..hasRequiredFields = false
  ;

  BattleEndedEvent._() : super();
  factory BattleEndedEvent({
    Report ev,
    Team winningTeam,
  }) {
    final _result = create();
    if (ev != null) {
      _result.ev = ev;
    }
    if (winningTeam != null) {
      _result.winningTeam = winningTeam;
    }
    return _result;
  }
  factory BattleEndedEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BattleEndedEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BattleEndedEvent clone() => BattleEndedEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BattleEndedEvent copyWith(void Function(BattleEndedEvent) updates) => super.copyWith((message) => updates(message as BattleEndedEvent)) as BattleEndedEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BattleEndedEvent create() => BattleEndedEvent._();
  BattleEndedEvent createEmptyInstance() => create();
  static $pb.PbList<BattleEndedEvent> createRepeated() => $pb.PbList<BattleEndedEvent>();
  @$core.pragma('dart2js:noInline')
  static BattleEndedEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BattleEndedEvent>(create);
  static BattleEndedEvent _defaultInstance;

  @$pb.TagNumber(1)
  Report get ev => $_getN(0);
  @$pb.TagNumber(1)
  set ev(Report v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasEv() => $_has(0);
  @$pb.TagNumber(1)
  void clearEv() => clearField(1);
  @$pb.TagNumber(1)
  Report ensureEv() => $_ensure(0);

  @$pb.TagNumber(2)
  Team get winningTeam => $_getN(1);
  @$pb.TagNumber(2)
  set winningTeam(Team v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasWinningTeam() => $_has(1);
  @$pb.TagNumber(2)
  void clearWinningTeam() => clearField(2);
  @$pb.TagNumber(2)
  Team ensureWinningTeam() => $_ensure(1);
}

enum BattleEvent_Event {
  turnUpdate, 
  action, 
  battleStarted, 
  battleEnded, 
  battleAdded, 
  notSet
}

class BattleEvent extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, BattleEvent_Event> _BattleEvent_EventByTag = {
    2 : BattleEvent_Event.turnUpdate,
    3 : BattleEvent_Event.action,
    4 : BattleEvent_Event.battleStarted,
    5 : BattleEvent_Event.battleEnded,
    6 : BattleEvent_Event.battleAdded,
    0 : BattleEvent_Event.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BattleEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..oo(0, [2, 3, 4, 5, 6])
    ..aOM<$0.BattleId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', subBuilder: $0.BattleId.create)
    ..aOM<TurnQueueEvent>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'turnUpdate', protoName: 'turnUpdate', subBuilder: TurnQueueEvent.create)
    ..aOM<ActionEvent>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'action', subBuilder: ActionEvent.create)
    ..aOM<BattleStartedEvent>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'battleStarted', protoName: 'battleStarted', subBuilder: BattleStartedEvent.create)
    ..aOM<BattleEndedEvent>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'battleEnded', protoName: 'battleEnded', subBuilder: BattleEndedEvent.create)
    ..aOM<BattleAdded>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'battleAdded', protoName: 'battleAdded', subBuilder: BattleAdded.create)
    ..hasRequiredFields = false
  ;

  BattleEvent._() : super();
  factory BattleEvent({
    $0.BattleId id,
    TurnQueueEvent turnUpdate,
    ActionEvent action,
    BattleStartedEvent battleStarted,
    BattleEndedEvent battleEnded,
    BattleAdded battleAdded,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (turnUpdate != null) {
      _result.turnUpdate = turnUpdate;
    }
    if (action != null) {
      _result.action = action;
    }
    if (battleStarted != null) {
      _result.battleStarted = battleStarted;
    }
    if (battleEnded != null) {
      _result.battleEnded = battleEnded;
    }
    if (battleAdded != null) {
      _result.battleAdded = battleAdded;
    }
    return _result;
  }
  factory BattleEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BattleEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BattleEvent clone() => BattleEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BattleEvent copyWith(void Function(BattleEvent) updates) => super.copyWith((message) => updates(message as BattleEvent)) as BattleEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BattleEvent create() => BattleEvent._();
  BattleEvent createEmptyInstance() => create();
  static $pb.PbList<BattleEvent> createRepeated() => $pb.PbList<BattleEvent>();
  @$core.pragma('dart2js:noInline')
  static BattleEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BattleEvent>(create);
  static BattleEvent _defaultInstance;

  BattleEvent_Event whichEvent() => _BattleEvent_EventByTag[$_whichOneof(0)];
  void clearEvent() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $0.BattleId get id => $_getN(0);
  @$pb.TagNumber(1)
  set id($0.BattleId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
  @$pb.TagNumber(1)
  $0.BattleId ensureId() => $_ensure(0);

  @$pb.TagNumber(2)
  TurnQueueEvent get turnUpdate => $_getN(1);
  @$pb.TagNumber(2)
  set turnUpdate(TurnQueueEvent v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasTurnUpdate() => $_has(1);
  @$pb.TagNumber(2)
  void clearTurnUpdate() => clearField(2);
  @$pb.TagNumber(2)
  TurnQueueEvent ensureTurnUpdate() => $_ensure(1);

  @$pb.TagNumber(3)
  ActionEvent get action => $_getN(2);
  @$pb.TagNumber(3)
  set action(ActionEvent v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasAction() => $_has(2);
  @$pb.TagNumber(3)
  void clearAction() => clearField(3);
  @$pb.TagNumber(3)
  ActionEvent ensureAction() => $_ensure(2);

  @$pb.TagNumber(4)
  BattleStartedEvent get battleStarted => $_getN(3);
  @$pb.TagNumber(4)
  set battleStarted(BattleStartedEvent v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasBattleStarted() => $_has(3);
  @$pb.TagNumber(4)
  void clearBattleStarted() => clearField(4);
  @$pb.TagNumber(4)
  BattleStartedEvent ensureBattleStarted() => $_ensure(3);

  @$pb.TagNumber(5)
  BattleEndedEvent get battleEnded => $_getN(4);
  @$pb.TagNumber(5)
  set battleEnded(BattleEndedEvent v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasBattleEnded() => $_has(4);
  @$pb.TagNumber(5)
  void clearBattleEnded() => clearField(5);
  @$pb.TagNumber(5)
  BattleEndedEvent ensureBattleEnded() => $_ensure(4);

  @$pb.TagNumber(6)
  BattleAdded get battleAdded => $_getN(5);
  @$pb.TagNumber(6)
  set battleAdded(BattleAdded v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasBattleAdded() => $_has(5);
  @$pb.TagNumber(6)
  void clearBattleAdded() => clearField(6);
  @$pb.TagNumber(6)
  BattleAdded ensureBattleAdded() => $_ensure(5);
}

class BattleAdded extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BattleAdded', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..aOM<Battle>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'b', subBuilder: Battle.create)
    ..hasRequiredFields = false
  ;

  BattleAdded._() : super();
  factory BattleAdded({
    Battle b,
  }) {
    final _result = create();
    if (b != null) {
      _result.b = b;
    }
    return _result;
  }
  factory BattleAdded.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BattleAdded.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BattleAdded clone() => BattleAdded()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BattleAdded copyWith(void Function(BattleAdded) updates) => super.copyWith((message) => updates(message as BattleAdded)) as BattleAdded; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BattleAdded create() => BattleAdded._();
  BattleAdded createEmptyInstance() => create();
  static $pb.PbList<BattleAdded> createRepeated() => $pb.PbList<BattleAdded>();
  @$core.pragma('dart2js:noInline')
  static BattleAdded getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BattleAdded>(create);
  static BattleAdded _defaultInstance;

  @$pb.TagNumber(1)
  Battle get b => $_getN(0);
  @$pb.TagNumber(1)
  set b(Battle v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasB() => $_has(0);
  @$pb.TagNumber(1)
  void clearB() => clearField(1);
  @$pb.TagNumber(1)
  Battle ensureB() => $_ensure(0);
}

class BattleEnded extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BattleEnded', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..aOM<Report>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ev', subBuilder: Report.create)
    ..aOM<Team>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'winningTeam', protoName: 'winningTeam', subBuilder: Team.create)
    ..hasRequiredFields = false
  ;

  BattleEnded._() : super();
  factory BattleEnded({
    Report ev,
    Team winningTeam,
  }) {
    final _result = create();
    if (ev != null) {
      _result.ev = ev;
    }
    if (winningTeam != null) {
      _result.winningTeam = winningTeam;
    }
    return _result;
  }
  factory BattleEnded.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BattleEnded.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BattleEnded clone() => BattleEnded()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BattleEnded copyWith(void Function(BattleEnded) updates) => super.copyWith((message) => updates(message as BattleEnded)) as BattleEnded; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BattleEnded create() => BattleEnded._();
  BattleEnded createEmptyInstance() => create();
  static $pb.PbList<BattleEnded> createRepeated() => $pb.PbList<BattleEnded>();
  @$core.pragma('dart2js:noInline')
  static BattleEnded getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BattleEnded>(create);
  static BattleEnded _defaultInstance;

  @$pb.TagNumber(1)
  Report get ev => $_getN(0);
  @$pb.TagNumber(1)
  set ev(Report v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasEv() => $_has(0);
  @$pb.TagNumber(1)
  void clearEv() => clearField(1);
  @$pb.TagNumber(1)
  Report ensureEv() => $_ensure(0);

  @$pb.TagNumber(2)
  Team get winningTeam => $_getN(1);
  @$pb.TagNumber(2)
  set winningTeam(Team v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasWinningTeam() => $_has(1);
  @$pb.TagNumber(2)
  void clearWinningTeam() => clearField(2);
  @$pb.TagNumber(2)
  Team ensureWinningTeam() => $_ensure(1);
}

class ActionEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ActionEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..aOM<$0.MonId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'source', subBuilder: $0.MonId.create)
    ..aOM<$0.MonId>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'target', subBuilder: $0.MonId.create)
    ..aOM<$3.BattleValues>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'effect', subBuilder: $3.BattleValues.create)
    ..hasRequiredFields = false
  ;

  ActionEvent._() : super();
  factory ActionEvent({
    $0.MonId source,
    $0.MonId target,
    $3.BattleValues effect,
  }) {
    final _result = create();
    if (source != null) {
      _result.source = source;
    }
    if (target != null) {
      _result.target = target;
    }
    if (effect != null) {
      _result.effect = effect;
    }
    return _result;
  }
  factory ActionEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ActionEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ActionEvent clone() => ActionEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ActionEvent copyWith(void Function(ActionEvent) updates) => super.copyWith((message) => updates(message as ActionEvent)) as ActionEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ActionEvent create() => ActionEvent._();
  ActionEvent createEmptyInstance() => create();
  static $pb.PbList<ActionEvent> createRepeated() => $pb.PbList<ActionEvent>();
  @$core.pragma('dart2js:noInline')
  static ActionEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ActionEvent>(create);
  static ActionEvent _defaultInstance;

  @$pb.TagNumber(1)
  $0.MonId get source => $_getN(0);
  @$pb.TagNumber(1)
  set source($0.MonId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSource() => $_has(0);
  @$pb.TagNumber(1)
  void clearSource() => clearField(1);
  @$pb.TagNumber(1)
  $0.MonId ensureSource() => $_ensure(0);

  @$pb.TagNumber(2)
  $0.MonId get target => $_getN(1);
  @$pb.TagNumber(2)
  set target($0.MonId v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasTarget() => $_has(1);
  @$pb.TagNumber(2)
  void clearTarget() => clearField(2);
  @$pb.TagNumber(2)
  $0.MonId ensureTarget() => $_ensure(1);

  @$pb.TagNumber(3)
  $3.BattleValues get effect => $_getN(2);
  @$pb.TagNumber(3)
  set effect($3.BattleValues v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasEffect() => $_has(2);
  @$pb.TagNumber(3)
  void clearEffect() => clearField(3);
  @$pb.TagNumber(3)
  $3.BattleValues ensureEffect() => $_ensure(2);
}

enum BattleResponse_Message {
  ev, 
  turnU, 
  notSet
}

class BattleResponse extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, BattleResponse_Message> _BattleResponse_MessageByTag = {
    1 : BattleResponse_Message.ev,
    3 : BattleResponse_Message.turnU,
    0 : BattleResponse_Message.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BattleResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..oo(0, [1, 3])
    ..aOM<Report>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ev', subBuilder: Report.create)
    ..aOM<TurnQueueEvent>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'turnU', protoName: 'turnU', subBuilder: TurnQueueEvent.create)
    ..hasRequiredFields = false
  ;

  BattleResponse._() : super();
  factory BattleResponse({
    Report ev,
    TurnQueueEvent turnU,
  }) {
    final _result = create();
    if (ev != null) {
      _result.ev = ev;
    }
    if (turnU != null) {
      _result.turnU = turnU;
    }
    return _result;
  }
  factory BattleResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BattleResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BattleResponse clone() => BattleResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BattleResponse copyWith(void Function(BattleResponse) updates) => super.copyWith((message) => updates(message as BattleResponse)) as BattleResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BattleResponse create() => BattleResponse._();
  BattleResponse createEmptyInstance() => create();
  static $pb.PbList<BattleResponse> createRepeated() => $pb.PbList<BattleResponse>();
  @$core.pragma('dart2js:noInline')
  static BattleResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BattleResponse>(create);
  static BattleResponse _defaultInstance;

  BattleResponse_Message whichMessage() => _BattleResponse_MessageByTag[$_whichOneof(0)];
  void clearMessage() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  Report get ev => $_getN(0);
  @$pb.TagNumber(1)
  set ev(Report v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasEv() => $_has(0);
  @$pb.TagNumber(1)
  void clearEv() => clearField(1);
  @$pb.TagNumber(1)
  Report ensureEv() => $_ensure(0);

  @$pb.TagNumber(3)
  TurnQueueEvent get turnU => $_getN(1);
  @$pb.TagNumber(3)
  set turnU(TurnQueueEvent v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasTurnU() => $_has(1);
  @$pb.TagNumber(3)
  void clearTurnU() => clearField(3);
  @$pb.TagNumber(3)
  TurnQueueEvent ensureTurnU() => $_ensure(1);
}

enum Command_Command {
  attack, 
  random, 
  notSet
}

class Command extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Command_Command> _Command_CommandByTag = {
    2 : Command_Command.attack,
    3 : Command_Command.random,
    0 : Command_Command.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Command', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..oo(0, [2, 3])
    ..aOM<$0.BattleId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'battleId', protoName: 'battleId', subBuilder: $0.BattleId.create)
    ..aOM<Attack>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'attack', subBuilder: Attack.create)
    ..aOM<RandomCommand>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'random', subBuilder: RandomCommand.create)
    ..hasRequiredFields = false
  ;

  Command._() : super();
  factory Command({
    $0.BattleId battleId,
    Attack attack,
    RandomCommand random,
  }) {
    final _result = create();
    if (battleId != null) {
      _result.battleId = battleId;
    }
    if (attack != null) {
      _result.attack = attack;
    }
    if (random != null) {
      _result.random = random;
    }
    return _result;
  }
  factory Command.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Command.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Command clone() => Command()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Command copyWith(void Function(Command) updates) => super.copyWith((message) => updates(message as Command)) as Command; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Command create() => Command._();
  Command createEmptyInstance() => create();
  static $pb.PbList<Command> createRepeated() => $pb.PbList<Command>();
  @$core.pragma('dart2js:noInline')
  static Command getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Command>(create);
  static Command _defaultInstance;

  Command_Command whichCommand() => _Command_CommandByTag[$_whichOneof(0)];
  void clearCommand() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $0.BattleId get battleId => $_getN(0);
  @$pb.TagNumber(1)
  set battleId($0.BattleId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasBattleId() => $_has(0);
  @$pb.TagNumber(1)
  void clearBattleId() => clearField(1);
  @$pb.TagNumber(1)
  $0.BattleId ensureBattleId() => $_ensure(0);

  @$pb.TagNumber(2)
  Attack get attack => $_getN(1);
  @$pb.TagNumber(2)
  set attack(Attack v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasAttack() => $_has(1);
  @$pb.TagNumber(2)
  void clearAttack() => clearField(2);
  @$pb.TagNumber(2)
  Attack ensureAttack() => $_ensure(1);

  @$pb.TagNumber(3)
  RandomCommand get random => $_getN(2);
  @$pb.TagNumber(3)
  set random(RandomCommand v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasRandom() => $_has(2);
  @$pb.TagNumber(3)
  void clearRandom() => clearField(3);
  @$pb.TagNumber(3)
  RandomCommand ensureRandom() => $_ensure(2);
}

class BattleRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BattleRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..aOM<$0.BattleId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', subBuilder: $0.BattleId.create)
    ..pc<$0.MonId>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'monIds', $pb.PbFieldType.PM, protoName: 'monIds', subBuilder: $0.MonId.create)
    ..aOM<$0.UserId>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId', protoName: 'userId', subBuilder: $0.UserId.create)
    ..hasRequiredFields = false
  ;

  BattleRequest._() : super();
  factory BattleRequest({
    $0.BattleId id,
    $core.Iterable<$0.MonId> monIds,
    $0.UserId userId,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (monIds != null) {
      _result.monIds.addAll(monIds);
    }
    if (userId != null) {
      _result.userId = userId;
    }
    return _result;
  }
  factory BattleRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BattleRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BattleRequest clone() => BattleRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BattleRequest copyWith(void Function(BattleRequest) updates) => super.copyWith((message) => updates(message as BattleRequest)) as BattleRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BattleRequest create() => BattleRequest._();
  BattleRequest createEmptyInstance() => create();
  static $pb.PbList<BattleRequest> createRepeated() => $pb.PbList<BattleRequest>();
  @$core.pragma('dart2js:noInline')
  static BattleRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BattleRequest>(create);
  static BattleRequest _defaultInstance;

  @$pb.TagNumber(1)
  $0.BattleId get id => $_getN(0);
  @$pb.TagNumber(1)
  set id($0.BattleId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
  @$pb.TagNumber(1)
  $0.BattleId ensureId() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<$0.MonId> get monIds => $_getList(1);

  @$pb.TagNumber(3)
  $0.UserId get userId => $_getN(2);
  @$pb.TagNumber(3)
  set userId($0.UserId v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasUserId() => $_has(2);
  @$pb.TagNumber(3)
  void clearUserId() => clearField(3);
  @$pb.TagNumber(3)
  $0.UserId ensureUserId() => $_ensure(2);
}

class BattleList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BattleList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..pc<Battle>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'battles', $pb.PbFieldType.PM, subBuilder: Battle.create)
    ..hasRequiredFields = false
  ;

  BattleList._() : super();
  factory BattleList({
    $core.Iterable<Battle> battles,
  }) {
    final _result = create();
    if (battles != null) {
      _result.battles.addAll(battles);
    }
    return _result;
  }
  factory BattleList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BattleList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BattleList clone() => BattleList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BattleList copyWith(void Function(BattleList) updates) => super.copyWith((message) => updates(message as BattleList)) as BattleList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BattleList create() => BattleList._();
  BattleList createEmptyInstance() => create();
  static $pb.PbList<BattleList> createRepeated() => $pb.PbList<BattleList>();
  @$core.pragma('dart2js:noInline')
  static BattleList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BattleList>(create);
  static BattleList _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Battle> get battles => $_getList(0);
}

class ResponseStatus extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ResponseStatus', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'success')
    ..hasRequiredFields = false
  ;

  ResponseStatus._() : super();
  factory ResponseStatus({
    $core.bool success,
  }) {
    final _result = create();
    if (success != null) {
      _result.success = success;
    }
    return _result;
  }
  factory ResponseStatus.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ResponseStatus.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ResponseStatus clone() => ResponseStatus()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ResponseStatus copyWith(void Function(ResponseStatus) updates) => super.copyWith((message) => updates(message as ResponseStatus)) as ResponseStatus; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ResponseStatus create() => ResponseStatus._();
  ResponseStatus createEmptyInstance() => create();
  static $pb.PbList<ResponseStatus> createRepeated() => $pb.PbList<ResponseStatus>();
  @$core.pragma('dart2js:noInline')
  static ResponseStatus getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ResponseStatus>(create);
  static ResponseStatus _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get success => $_getBF(0);
  @$pb.TagNumber(1)
  set success($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSuccess() => $_has(0);
  @$pb.TagNumber(1)
  void clearSuccess() => clearField(1);
}

class RandomCommand extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RandomCommand', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..aOM<$0.MonId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'source', subBuilder: $0.MonId.create)
    ..hasRequiredFields = false
  ;

  RandomCommand._() : super();
  factory RandomCommand({
    $0.MonId source,
  }) {
    final _result = create();
    if (source != null) {
      _result.source = source;
    }
    return _result;
  }
  factory RandomCommand.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RandomCommand.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RandomCommand clone() => RandomCommand()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RandomCommand copyWith(void Function(RandomCommand) updates) => super.copyWith((message) => updates(message as RandomCommand)) as RandomCommand; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RandomCommand create() => RandomCommand._();
  RandomCommand createEmptyInstance() => create();
  static $pb.PbList<RandomCommand> createRepeated() => $pb.PbList<RandomCommand>();
  @$core.pragma('dart2js:noInline')
  static RandomCommand getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RandomCommand>(create);
  static RandomCommand _defaultInstance;

  @$pb.TagNumber(1)
  $0.MonId get source => $_getN(0);
  @$pb.TagNumber(1)
  set source($0.MonId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSource() => $_has(0);
  @$pb.TagNumber(1)
  void clearSource() => clearField(1);
  @$pb.TagNumber(1)
  $0.MonId ensureSource() => $_ensure(0);
}

class Attack extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Attack', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'battle'), createEmptyInstance: create)
    ..aOM<$0.MonId>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'source', subBuilder: $0.MonId.create)
    ..aOM<$0.MonId>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'target', subBuilder: $0.MonId.create)
    ..hasRequiredFields = false
  ;

  Attack._() : super();
  factory Attack({
    $0.MonId source,
    $0.MonId target,
  }) {
    final _result = create();
    if (source != null) {
      _result.source = source;
    }
    if (target != null) {
      _result.target = target;
    }
    return _result;
  }
  factory Attack.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Attack.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Attack clone() => Attack()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Attack copyWith(void Function(Attack) updates) => super.copyWith((message) => updates(message as Attack)) as Attack; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Attack create() => Attack._();
  Attack createEmptyInstance() => create();
  static $pb.PbList<Attack> createRepeated() => $pb.PbList<Attack>();
  @$core.pragma('dart2js:noInline')
  static Attack getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Attack>(create);
  static Attack _defaultInstance;

  @$pb.TagNumber(1)
  $0.MonId get source => $_getN(0);
  @$pb.TagNumber(1)
  set source($0.MonId v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSource() => $_has(0);
  @$pb.TagNumber(1)
  void clearSource() => clearField(1);
  @$pb.TagNumber(1)
  $0.MonId ensureSource() => $_ensure(0);

  @$pb.TagNumber(2)
  $0.MonId get target => $_getN(1);
  @$pb.TagNumber(2)
  set target($0.MonId v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasTarget() => $_has(1);
  @$pb.TagNumber(2)
  void clearTarget() => clearField(2);
  @$pb.TagNumber(2)
  $0.MonId ensureTarget() => $_ensure(1);
}

