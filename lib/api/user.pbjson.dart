///
//  Generated code. Do not modify.
//  source: user.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use userDescriptor instead')
const User$json = const {
  '1': 'User',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
    const {'1': 'username', '3': 2, '4': 1, '5': 9, '10': 'username'},
    const {'1': 'last_update', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'lastUpdate'},
    const {'1': 'is_test', '3': 4, '4': 1, '5': 8, '10': 'isTest'},
  ],
};

/// Descriptor for `User`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userDescriptor = $convert.base64Decode('CgRVc2VyEg4KAmlkGAEgASgFUgJpZBIaCgh1c2VybmFtZRgCIAEoCVIIdXNlcm5hbWUSOwoLbGFzdF91cGRhdGUYAyABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUgpsYXN0VXBkYXRlEhcKB2lzX3Rlc3QYBCABKAhSBmlzVGVzdA==');
@$core.Deprecated('Use getUserCallDescriptor instead')
const GetUserCall$json = const {
  '1': 'GetUserCall',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 11, '6': '.common.UserId', '10': 'user'},
  ],
};

/// Descriptor for `GetUserCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getUserCallDescriptor = $convert.base64Decode('CgtHZXRVc2VyQ2FsbBIiCgR1c2VyGAEgASgLMg4uY29tbW9uLlVzZXJJZFIEdXNlcg==');
@$core.Deprecated('Use getUserResponseDescriptor instead')
const GetUserResponse$json = const {
  '1': 'GetUserResponse',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 11, '6': '.user.User', '10': 'user'},
  ],
};

/// Descriptor for `GetUserResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getUserResponseDescriptor = $convert.base64Decode('Cg9HZXRVc2VyUmVzcG9uc2USHgoEdXNlchgBIAEoCzIKLnVzZXIuVXNlclIEdXNlcg==');
@$core.Deprecated('Use userServiceCallDescriptor instead')
const UserServiceCall$json = const {
  '1': 'UserServiceCall',
  '2': const [
    const {'1': 'get_user_call', '3': 1, '4': 1, '5': 11, '6': '.user.GetUserCall', '9': 0, '10': 'getUserCall'},
  ],
  '8': const [
    const {'1': 'message'},
  ],
};

/// Descriptor for `UserServiceCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userServiceCallDescriptor = $convert.base64Decode('Cg9Vc2VyU2VydmljZUNhbGwSNwoNZ2V0X3VzZXJfY2FsbBgBIAEoCzIRLnVzZXIuR2V0VXNlckNhbGxIAFILZ2V0VXNlckNhbGxCCQoHbWVzc2FnZQ==');
@$core.Deprecated('Use userServiceResponseDescriptor instead')
const UserServiceResponse$json = const {
  '1': 'UserServiceResponse',
  '2': const [
    const {'1': 'get_user_response', '3': 1, '4': 1, '5': 11, '6': '.user.GetUserResponse', '9': 0, '10': 'getUserResponse'},
  ],
  '8': const [
    const {'1': 'message'},
  ],
};

/// Descriptor for `UserServiceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userServiceResponseDescriptor = $convert.base64Decode('ChNVc2VyU2VydmljZVJlc3BvbnNlEkMKEWdldF91c2VyX3Jlc3BvbnNlGAEgASgLMhUudXNlci5HZXRVc2VyUmVzcG9uc2VIAFIPZ2V0VXNlclJlc3BvbnNlQgkKB21lc3NhZ2U=');
