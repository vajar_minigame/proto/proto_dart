///
//  Generated code. Do not modify.
//  source: reward.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'battle.pb.dart' as $4;
import 'reward.pb.dart' as $10;
import 'common.pb.dart' as $0;
export 'reward.pb.dart';

class RewardServiceClient extends $grpc.Client {
  static final _$createReward = $grpc.ClientMethod<$4.Battle, $10.Reward>(
      '/reward.RewardService/CreateReward',
      ($4.Battle value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $10.Reward.fromBuffer(value));
  static final _$getReward = $grpc.ClientMethod<$0.RewardId, $10.Reward>(
      '/reward.RewardService/getReward',
      ($0.RewardId value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $10.Reward.fromBuffer(value));
  static final _$collectReward =
      $grpc.ClientMethod<$0.RewardId, $0.ResponseStatus>(
          '/reward.RewardService/collectReward',
          ($0.RewardId value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ResponseStatus.fromBuffer(value));
  static final _$getAllRewardsByUser =
      $grpc.ClientMethod<$0.UserId, $10.RewardList>(
          '/reward.RewardService/getAllRewardsByUser',
          ($0.UserId value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $10.RewardList.fromBuffer(value));

  RewardServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options,
      $core.Iterable<$grpc.ClientInterceptor> interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$10.Reward> createReward($4.Battle request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$createReward, request, options: options);
  }

  $grpc.ResponseFuture<$10.Reward> getReward($0.RewardId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getReward, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResponseStatus> collectReward($0.RewardId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$collectReward, request, options: options);
  }

  $grpc.ResponseFuture<$10.RewardList> getAllRewardsByUser($0.UserId request,
      {$grpc.CallOptions options}) {
    return $createUnaryCall(_$getAllRewardsByUser, request, options: options);
  }
}

abstract class RewardServiceBase extends $grpc.Service {
  $core.String get $name => 'reward.RewardService';

  RewardServiceBase() {
    $addMethod($grpc.ServiceMethod<$4.Battle, $10.Reward>(
        'CreateReward',
        createReward_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $4.Battle.fromBuffer(value),
        ($10.Reward value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RewardId, $10.Reward>(
        'getReward',
        getReward_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RewardId.fromBuffer(value),
        ($10.Reward value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RewardId, $0.ResponseStatus>(
        'collectReward',
        collectReward_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RewardId.fromBuffer(value),
        ($0.ResponseStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserId, $10.RewardList>(
        'getAllRewardsByUser',
        getAllRewardsByUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserId.fromBuffer(value),
        ($10.RewardList value) => value.writeToBuffer()));
  }

  $async.Future<$10.Reward> createReward_Pre(
      $grpc.ServiceCall call, $async.Future<$4.Battle> request) async {
    return createReward(call, await request);
  }

  $async.Future<$10.Reward> getReward_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RewardId> request) async {
    return getReward(call, await request);
  }

  $async.Future<$0.ResponseStatus> collectReward_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RewardId> request) async {
    return collectReward(call, await request);
  }

  $async.Future<$10.RewardList> getAllRewardsByUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserId> request) async {
    return getAllRewardsByUser(call, await request);
  }

  $async.Future<$10.Reward> createReward(
      $grpc.ServiceCall call, $4.Battle request);
  $async.Future<$10.Reward> getReward(
      $grpc.ServiceCall call, $0.RewardId request);
  $async.Future<$0.ResponseStatus> collectReward(
      $grpc.ServiceCall call, $0.RewardId request);
  $async.Future<$10.RewardList> getAllRewardsByUser(
      $grpc.ServiceCall call, $0.UserId request);
}
