///
//  Generated code. Do not modify.
//  source: monster.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use monsterDescriptor instead')
const Monster$json = const {
  '1': 'Monster',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'id', '3': 2, '4': 1, '5': 5, '10': 'id'},
    const {'1': 'playerID', '3': 3, '4': 1, '5': 5, '10': 'playerID'},
    const {'1': 'battleValues', '3': 4, '4': 1, '5': 11, '6': '.monster.BattleValues', '10': 'battleValues'},
    const {'1': 'bodyValues', '3': 5, '4': 1, '5': 11, '6': '.monster.BodyValues', '10': 'bodyValues'},
    const {'1': 'last_update', '3': 8, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'lastUpdate'},
    const {'1': 'is_test', '3': 10, '4': 1, '5': 8, '10': 'isTest'},
    const {'1': 'activity', '3': 6, '4': 1, '5': 11, '6': '.monster.Activity', '10': 'activity'},
    const {'1': 'status', '3': 7, '4': 1, '5': 11, '6': '.monster.StatusFlags', '10': 'status'},
  ],
};

/// Descriptor for `Monster`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List monsterDescriptor = $convert.base64Decode('CgdNb25zdGVyEhIKBG5hbWUYASABKAlSBG5hbWUSDgoCaWQYAiABKAVSAmlkEhoKCHBsYXllcklEGAMgASgFUghwbGF5ZXJJRBI5CgxiYXR0bGVWYWx1ZXMYBCABKAsyFS5tb25zdGVyLkJhdHRsZVZhbHVlc1IMYmF0dGxlVmFsdWVzEjMKCmJvZHlWYWx1ZXMYBSABKAsyEy5tb25zdGVyLkJvZHlWYWx1ZXNSCmJvZHlWYWx1ZXMSOwoLbGFzdF91cGRhdGUYCCABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUgpsYXN0VXBkYXRlEhcKB2lzX3Rlc3QYCiABKAhSBmlzVGVzdBItCghhY3Rpdml0eRgGIAEoCzIRLm1vbnN0ZXIuQWN0aXZpdHlSCGFjdGl2aXR5EiwKBnN0YXR1cxgHIAEoCzIULm1vbnN0ZXIuU3RhdHVzRmxhZ3NSBnN0YXR1cw==');
@$core.Deprecated('Use statusFlagsDescriptor instead')
const StatusFlags$json = const {
  '1': 'StatusFlags',
  '2': const [
    const {'1': 'is_alive', '3': 1, '4': 1, '5': 8, '10': 'isAlive'},
  ],
};

/// Descriptor for `StatusFlags`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List statusFlagsDescriptor = $convert.base64Decode('CgtTdGF0dXNGbGFncxIZCghpc19hbGl2ZRgBIAEoCFIHaXNBbGl2ZQ==');
@$core.Deprecated('Use battleValuesDescriptor instead')
const BattleValues$json = const {
  '1': 'BattleValues',
  '2': const [
    const {'1': 'attack', '3': 1, '4': 1, '5': 5, '10': 'attack'},
    const {'1': 'defense', '3': 2, '4': 1, '5': 5, '10': 'defense'},
    const {'1': 'max_hp', '3': 3, '4': 1, '5': 5, '10': 'maxHp'},
    const {'1': 'remaining_hp', '3': 4, '4': 1, '5': 5, '10': 'remainingHp'},
  ],
};

/// Descriptor for `BattleValues`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleValuesDescriptor = $convert.base64Decode('CgxCYXR0bGVWYWx1ZXMSFgoGYXR0YWNrGAEgASgFUgZhdHRhY2sSGAoHZGVmZW5zZRgCIAEoBVIHZGVmZW5zZRIVCgZtYXhfaHAYAyABKAVSBW1heEhwEiEKDHJlbWFpbmluZ19ocBgEIAEoBVILcmVtYWluaW5nSHA=');
@$core.Deprecated('Use bodyValuesDescriptor instead')
const BodyValues$json = const {
  '1': 'BodyValues',
  '2': const [
    const {'1': 'remaining_saturation', '3': 1, '4': 1, '5': 1, '10': 'remainingSaturation'},
    const {'1': 'max_saturation', '3': 2, '4': 1, '5': 1, '10': 'maxSaturation'},
    const {'1': 'mass', '3': 3, '4': 1, '5': 5, '10': 'mass'},
  ],
};

/// Descriptor for `BodyValues`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bodyValuesDescriptor = $convert.base64Decode('CgpCb2R5VmFsdWVzEjEKFHJlbWFpbmluZ19zYXR1cmF0aW9uGAEgASgBUhNyZW1haW5pbmdTYXR1cmF0aW9uEiUKDm1heF9zYXR1cmF0aW9uGAIgASgBUg1tYXhTYXR1cmF0aW9uEhIKBG1hc3MYAyABKAVSBG1hc3M=');
@$core.Deprecated('Use activityDescriptor instead')
const Activity$json = const {
  '1': 'Activity',
  '2': const [
    const {'1': 'questId', '3': 1, '4': 1, '5': 11, '6': '.common.QuestId', '9': 0, '10': 'questId'},
    const {'1': 'battleID', '3': 2, '4': 1, '5': 11, '6': '.common.BattleId', '9': 0, '10': 'battleID'},
  ],
  '8': const [
    const {'1': 'activity'},
  ],
};

/// Descriptor for `Activity`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List activityDescriptor = $convert.base64Decode('CghBY3Rpdml0eRIrCgdxdWVzdElkGAEgASgLMg8uY29tbW9uLlF1ZXN0SWRIAFIHcXVlc3RJZBIuCghiYXR0bGVJRBgCIAEoCzIQLmNvbW1vbi5CYXR0bGVJZEgAUghiYXR0bGVJREIKCghhY3Rpdml0eQ==');
@$core.Deprecated('Use addMonCallDescriptor instead')
const AddMonCall$json = const {
  '1': 'AddMonCall',
  '2': const [
    const {'1': 'mon', '3': 1, '4': 1, '5': 11, '6': '.monster.Monster', '10': 'mon'},
  ],
};

/// Descriptor for `AddMonCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addMonCallDescriptor = $convert.base64Decode('CgpBZGRNb25DYWxsEiIKA21vbhgBIAEoCzIQLm1vbnN0ZXIuTW9uc3RlclIDbW9u');
@$core.Deprecated('Use getMonByIDCallDescriptor instead')
const GetMonByIDCall$json = const {
  '1': 'GetMonByIDCall',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.common.MonId', '10': 'id'},
  ],
};

/// Descriptor for `GetMonByIDCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getMonByIDCallDescriptor = $convert.base64Decode('Cg5HZXRNb25CeUlEQ2FsbBIdCgJpZBgBIAEoCzINLmNvbW1vbi5Nb25JZFICaWQ=');
@$core.Deprecated('Use getMonByUserIDCallDescriptor instead')
const GetMonByUserIDCall$json = const {
  '1': 'GetMonByUserIDCall',
  '2': const [
    const {'1': 'userId', '3': 1, '4': 1, '5': 11, '6': '.common.UserId', '10': 'userId'},
  ],
};

/// Descriptor for `GetMonByUserIDCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getMonByUserIDCallDescriptor = $convert.base64Decode('ChJHZXRNb25CeVVzZXJJRENhbGwSJgoGdXNlcklkGAEgASgLMg4uY29tbW9uLlVzZXJJZFIGdXNlcklk');
@$core.Deprecated('Use updateMonCallDescriptor instead')
const UpdateMonCall$json = const {
  '1': 'UpdateMonCall',
  '2': const [
    const {'1': 'mon', '3': 1, '4': 1, '5': 11, '6': '.monster.Monster', '10': 'mon'},
  ],
};

/// Descriptor for `UpdateMonCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateMonCallDescriptor = $convert.base64Decode('Cg1VcGRhdGVNb25DYWxsEiIKA21vbhgBIAEoCzIQLm1vbnN0ZXIuTW9uc3RlclIDbW9u');
@$core.Deprecated('Use deleteMonCallDescriptor instead')
const DeleteMonCall$json = const {
  '1': 'DeleteMonCall',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.common.MonId', '10': 'id'},
  ],
};

/// Descriptor for `DeleteMonCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteMonCallDescriptor = $convert.base64Decode('Cg1EZWxldGVNb25DYWxsEh0KAmlkGAEgASgLMg0uY29tbW9uLk1vbklkUgJpZA==');
@$core.Deprecated('Use lockRequestDescriptor instead')
const LockRequest$json = const {
  '1': 'LockRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.common.MonId', '10': 'id'},
    const {'1': 'activity', '3': 2, '4': 1, '5': 11, '6': '.monster.Activity', '10': 'activity'},
  ],
};

/// Descriptor for `LockRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List lockRequestDescriptor = $convert.base64Decode('CgtMb2NrUmVxdWVzdBIdCgJpZBgBIAEoCzINLmNvbW1vbi5Nb25JZFICaWQSLQoIYWN0aXZpdHkYAiABKAsyES5tb25zdGVyLkFjdGl2aXR5UghhY3Rpdml0eQ==');
@$core.Deprecated('Use releaseRequestDescriptor instead')
const ReleaseRequest$json = const {
  '1': 'ReleaseRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.common.MonId', '10': 'id'},
  ],
};

/// Descriptor for `ReleaseRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List releaseRequestDescriptor = $convert.base64Decode('Cg5SZWxlYXNlUmVxdWVzdBIdCgJpZBgBIAEoCzINLmNvbW1vbi5Nb25JZFICaWQ=');
@$core.Deprecated('Use monsterDeletedEventDescriptor instead')
const MonsterDeletedEvent$json = const {
  '1': 'MonsterDeletedEvent',
  '2': const [
    const {'1': 'monId', '3': 1, '4': 1, '5': 11, '6': '.common.MonId', '10': 'monId'},
  ],
};

/// Descriptor for `MonsterDeletedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List monsterDeletedEventDescriptor = $convert.base64Decode('ChNNb25zdGVyRGVsZXRlZEV2ZW50EiMKBW1vbklkGAEgASgLMg0uY29tbW9uLk1vbklkUgVtb25JZA==');
@$core.Deprecated('Use monsterAddedEventDescriptor instead')
const MonsterAddedEvent$json = const {
  '1': 'MonsterAddedEvent',
  '2': const [
    const {'1': 'mon', '3': 1, '4': 1, '5': 11, '6': '.monster.Monster', '10': 'mon'},
  ],
};

/// Descriptor for `MonsterAddedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List monsterAddedEventDescriptor = $convert.base64Decode('ChFNb25zdGVyQWRkZWRFdmVudBIiCgNtb24YASABKAsyEC5tb25zdGVyLk1vbnN0ZXJSA21vbg==');
@$core.Deprecated('Use monsterUpdatedEventDescriptor instead')
const MonsterUpdatedEvent$json = const {
  '1': 'MonsterUpdatedEvent',
  '2': const [
    const {'1': 'mon', '3': 1, '4': 1, '5': 11, '6': '.monster.Monster', '10': 'mon'},
  ],
};

/// Descriptor for `MonsterUpdatedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List monsterUpdatedEventDescriptor = $convert.base64Decode('ChNNb25zdGVyVXBkYXRlZEV2ZW50EiIKA21vbhgBIAEoCzIQLm1vbnN0ZXIuTW9uc3RlclIDbW9u');
@$core.Deprecated('Use monsterEventDescriptor instead')
const MonsterEvent$json = const {
  '1': 'MonsterEvent',
  '2': const [
    const {'1': 'addedEvent', '3': 1, '4': 1, '5': 11, '6': '.monster.MonsterAddedEvent', '9': 0, '10': 'addedEvent'},
    const {'1': 'deleteEvent', '3': 2, '4': 1, '5': 11, '6': '.monster.MonsterDeletedEvent', '9': 0, '10': 'deleteEvent'},
    const {'1': 'updateEvent', '3': 3, '4': 1, '5': 11, '6': '.monster.MonsterUpdatedEvent', '9': 0, '10': 'updateEvent'},
  ],
  '8': const [
    const {'1': 'event'},
  ],
};

/// Descriptor for `MonsterEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List monsterEventDescriptor = $convert.base64Decode('CgxNb25zdGVyRXZlbnQSPAoKYWRkZWRFdmVudBgBIAEoCzIaLm1vbnN0ZXIuTW9uc3RlckFkZGVkRXZlbnRIAFIKYWRkZWRFdmVudBJACgtkZWxldGVFdmVudBgCIAEoCzIcLm1vbnN0ZXIuTW9uc3RlckRlbGV0ZWRFdmVudEgAUgtkZWxldGVFdmVudBJACgt1cGRhdGVFdmVudBgDIAEoCzIcLm1vbnN0ZXIuTW9uc3RlclVwZGF0ZWRFdmVudEgAUgt1cGRhdGVFdmVudEIHCgVldmVudA==');
@$core.Deprecated('Use monServiceCallDescriptor instead')
const MonServiceCall$json = const {
  '1': 'MonServiceCall',
  '2': const [
    const {'1': 'addMon', '3': 1, '4': 1, '5': 11, '6': '.monster.AddMonCall', '9': 0, '10': 'addMon'},
    const {'1': 'getbyId', '3': 2, '4': 1, '5': 11, '6': '.monster.GetMonByIDCall', '9': 0, '10': 'getbyId'},
    const {'1': 'getbyuserId', '3': 3, '4': 1, '5': 11, '6': '.monster.GetMonByUserIDCall', '9': 0, '10': 'getbyuserId'},
    const {'1': 'update', '3': 4, '4': 1, '5': 11, '6': '.monster.UpdateMonCall', '9': 0, '10': 'update'},
    const {'1': 'delete', '3': 5, '4': 1, '5': 11, '6': '.monster.DeleteMonCall', '9': 0, '10': 'delete'},
    const {'1': 'metaData', '3': 6, '4': 1, '5': 11, '6': '.common.MetaData', '10': 'metaData'},
  ],
  '8': const [
    const {'1': 'method'},
  ],
};

/// Descriptor for `MonServiceCall`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List monServiceCallDescriptor = $convert.base64Decode('Cg5Nb25TZXJ2aWNlQ2FsbBItCgZhZGRNb24YASABKAsyEy5tb25zdGVyLkFkZE1vbkNhbGxIAFIGYWRkTW9uEjMKB2dldGJ5SWQYAiABKAsyFy5tb25zdGVyLkdldE1vbkJ5SURDYWxsSABSB2dldGJ5SWQSPwoLZ2V0Ynl1c2VySWQYAyABKAsyGy5tb25zdGVyLkdldE1vbkJ5VXNlcklEQ2FsbEgAUgtnZXRieXVzZXJJZBIwCgZ1cGRhdGUYBCABKAsyFi5tb25zdGVyLlVwZGF0ZU1vbkNhbGxIAFIGdXBkYXRlEjAKBmRlbGV0ZRgFIAEoCzIWLm1vbnN0ZXIuRGVsZXRlTW9uQ2FsbEgAUgZkZWxldGUSLAoIbWV0YURhdGEYBiABKAsyEC5jb21tb24uTWV0YURhdGFSCG1ldGFEYXRhQggKBm1ldGhvZA==');
@$core.Deprecated('Use monsterListDescriptor instead')
const MonsterList$json = const {
  '1': 'MonsterList',
  '2': const [
    const {'1': 'monsters', '3': 1, '4': 3, '5': 11, '6': '.monster.Monster', '10': 'monsters'},
  ],
};

/// Descriptor for `MonsterList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List monsterListDescriptor = $convert.base64Decode('CgtNb25zdGVyTGlzdBIsCghtb25zdGVycxgBIAMoCzIQLm1vbnN0ZXIuTW9uc3RlclIIbW9uc3RlcnM=');
@$core.Deprecated('Use monServiceResponseDescriptor instead')
const MonServiceResponse$json = const {
  '1': 'MonServiceResponse',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.common.MonId', '9': 0, '10': 'id'},
    const {'1': 'mon', '3': 2, '4': 1, '5': 11, '6': '.monster.Monster', '9': 0, '10': 'mon'},
    const {'1': 'status', '3': 3, '4': 1, '5': 11, '6': '.common.ResponseStatus', '9': 0, '10': 'status'},
    const {'1': 'monsters', '3': 4, '4': 1, '5': 11, '6': '.monster.MonsterList', '9': 0, '10': 'monsters'},
    const {'1': 'metaData', '3': 5, '4': 1, '5': 11, '6': '.common.MetaData', '10': 'metaData'},
  ],
  '8': const [
    const {'1': 'data'},
  ],
};

/// Descriptor for `MonServiceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List monServiceResponseDescriptor = $convert.base64Decode('ChJNb25TZXJ2aWNlUmVzcG9uc2USHwoCaWQYASABKAsyDS5jb21tb24uTW9uSWRIAFICaWQSJAoDbW9uGAIgASgLMhAubW9uc3Rlci5Nb25zdGVySABSA21vbhIwCgZzdGF0dXMYAyABKAsyFi5jb21tb24uUmVzcG9uc2VTdGF0dXNIAFIGc3RhdHVzEjIKCG1vbnN0ZXJzGAQgASgLMhQubW9uc3Rlci5Nb25zdGVyTGlzdEgAUghtb25zdGVycxIsCghtZXRhRGF0YRgFIAEoCzIQLmNvbW1vbi5NZXRhRGF0YVIIbWV0YURhdGFCBgoEZGF0YQ==');
