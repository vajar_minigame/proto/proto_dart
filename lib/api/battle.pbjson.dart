///
//  Generated code. Do not modify.
//  source: battle.proto
//
// @dart = 2.7
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use teamDescriptor instead')
const Team$json = const {
  '1': 'Team',
  '2': const [
    const {'1': 'users', '3': 1, '4': 3, '5': 11, '6': '.common.UserId', '10': 'users'},
    const {'1': 'mons', '3': 2, '4': 3, '5': 11, '6': '.common.MonId', '10': 'mons'},
  ],
};

/// Descriptor for `Team`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List teamDescriptor = $convert.base64Decode('CgRUZWFtEiQKBXVzZXJzGAEgAygLMg4uY29tbW9uLlVzZXJJZFIFdXNlcnMSIQoEbW9ucxgCIAMoCzINLmNvbW1vbi5Nb25JZFIEbW9ucw==');
@$core.Deprecated('Use battleStateDescriptor instead')
const BattleState$json = const {
  '1': 'BattleState',
  '2': const [
    const {'1': 'activeBattle', '3': 1, '4': 1, '5': 11, '6': '.battle.ActiveBattle', '9': 0, '10': 'activeBattle'},
    const {'1': 'end', '3': 2, '4': 1, '5': 11, '6': '.battle.BattleEnded', '9': 0, '10': 'end'},
  ],
  '8': const [
    const {'1': 'state'},
  ],
};

/// Descriptor for `BattleState`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleStateDescriptor = $convert.base64Decode('CgtCYXR0bGVTdGF0ZRI6CgxhY3RpdmVCYXR0bGUYASABKAsyFC5iYXR0bGUuQWN0aXZlQmF0dGxlSABSDGFjdGl2ZUJhdHRsZRInCgNlbmQYAiABKAsyEy5iYXR0bGUuQmF0dGxlRW5kZWRIAFIDZW5kQgcKBXN0YXRl');
@$core.Deprecated('Use battleDescriptor instead')
const Battle$json = const {
  '1': 'Battle',
  '2': const [
    const {'1': 'Id', '3': 1, '4': 1, '5': 5, '10': 'Id'},
    const {'1': 'teams', '3': 2, '4': 3, '5': 11, '6': '.battle.Team', '10': 'teams'},
    const {'1': 'state', '3': 3, '4': 1, '5': 11, '6': '.battle.BattleState', '10': 'state'},
    const {'1': 'last_update', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'lastUpdate'},
    const {'1': 'is_test', '3': 5, '4': 1, '5': 8, '10': 'isTest'},
  ],
};

/// Descriptor for `Battle`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleDescriptor = $convert.base64Decode('CgZCYXR0bGUSDgoCSWQYASABKAVSAklkEiIKBXRlYW1zGAIgAygLMgwuYmF0dGxlLlRlYW1SBXRlYW1zEikKBXN0YXRlGAMgASgLMhMuYmF0dGxlLkJhdHRsZVN0YXRlUgVzdGF0ZRI7CgtsYXN0X3VwZGF0ZRgEIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSCmxhc3RVcGRhdGUSFwoHaXNfdGVzdBgFIAEoCFIGaXNUZXN0');
@$core.Deprecated('Use activeBattleDescriptor instead')
const ActiveBattle$json = const {
  '1': 'ActiveBattle',
  '2': const [
    const {'1': 'start_time', '3': 1, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startTime'},
    const {'1': 'turnCount', '3': 3, '4': 1, '5': 5, '10': 'turnCount'},
    const {'1': 'turnQueue', '3': 4, '4': 3, '5': 11, '6': '.common.MonId', '10': 'turnQueue'},
    const {'1': 'last_action', '3': 5, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'lastAction'},
  ],
};

/// Descriptor for `ActiveBattle`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List activeBattleDescriptor = $convert.base64Decode('CgxBY3RpdmVCYXR0bGUSOQoKc3RhcnRfdGltZRgBIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSCXN0YXJ0VGltZRIcCgl0dXJuQ291bnQYAyABKAVSCXR1cm5Db3VudBIrCgl0dXJuUXVldWUYBCADKAsyDS5jb21tb24uTW9uSWRSCXR1cm5RdWV1ZRI7CgtsYXN0X2FjdGlvbhgFIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSCmxhc3RBY3Rpb24=');
@$core.Deprecated('Use reportDescriptor instead')
const Report$json = const {
  '1': 'Report',
  '2': const [
    const {'1': 'report', '3': 1, '4': 3, '5': 9, '10': 'report'},
  ],
};

/// Descriptor for `Report`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List reportDescriptor = $convert.base64Decode('CgZSZXBvcnQSFgoGcmVwb3J0GAEgAygJUgZyZXBvcnQ=');
@$core.Deprecated('Use turnQueueEventDescriptor instead')
const TurnQueueEvent$json = const {
  '1': 'TurnQueueEvent',
  '2': const [
    const {'1': 'b', '3': 1, '4': 1, '5': 11, '6': '.battle.Battle', '10': 'b'},
  ],
};

/// Descriptor for `TurnQueueEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List turnQueueEventDescriptor = $convert.base64Decode('Cg5UdXJuUXVldWVFdmVudBIcCgFiGAEgASgLMg4uYmF0dGxlLkJhdHRsZVIBYg==');
@$core.Deprecated('Use battleStartedEventDescriptor instead')
const BattleStartedEvent$json = const {
  '1': 'BattleStartedEvent',
  '2': const [
    const {'1': 'b', '3': 1, '4': 1, '5': 11, '6': '.battle.Battle', '10': 'b'},
  ],
};

/// Descriptor for `BattleStartedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleStartedEventDescriptor = $convert.base64Decode('ChJCYXR0bGVTdGFydGVkRXZlbnQSHAoBYhgBIAEoCzIOLmJhdHRsZS5CYXR0bGVSAWI=');
@$core.Deprecated('Use battleEndedEventDescriptor instead')
const BattleEndedEvent$json = const {
  '1': 'BattleEndedEvent',
  '2': const [
    const {'1': 'ev', '3': 1, '4': 1, '5': 11, '6': '.battle.Report', '10': 'ev'},
    const {'1': 'winningTeam', '3': 2, '4': 1, '5': 11, '6': '.battle.Team', '10': 'winningTeam'},
  ],
};

/// Descriptor for `BattleEndedEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleEndedEventDescriptor = $convert.base64Decode('ChBCYXR0bGVFbmRlZEV2ZW50Eh4KAmV2GAEgASgLMg4uYmF0dGxlLlJlcG9ydFICZXYSLgoLd2lubmluZ1RlYW0YAiABKAsyDC5iYXR0bGUuVGVhbVILd2lubmluZ1RlYW0=');
@$core.Deprecated('Use battleEventDescriptor instead')
const BattleEvent$json = const {
  '1': 'BattleEvent',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.common.BattleId', '10': 'id'},
    const {'1': 'turnUpdate', '3': 2, '4': 1, '5': 11, '6': '.battle.TurnQueueEvent', '9': 0, '10': 'turnUpdate'},
    const {'1': 'action', '3': 3, '4': 1, '5': 11, '6': '.battle.ActionEvent', '9': 0, '10': 'action'},
    const {'1': 'battleStarted', '3': 4, '4': 1, '5': 11, '6': '.battle.BattleStartedEvent', '9': 0, '10': 'battleStarted'},
    const {'1': 'battleEnded', '3': 5, '4': 1, '5': 11, '6': '.battle.BattleEndedEvent', '9': 0, '10': 'battleEnded'},
    const {'1': 'battleAdded', '3': 6, '4': 1, '5': 11, '6': '.battle.BattleAdded', '9': 0, '10': 'battleAdded'},
  ],
  '8': const [
    const {'1': 'event'},
  ],
};

/// Descriptor for `BattleEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleEventDescriptor = $convert.base64Decode('CgtCYXR0bGVFdmVudBIgCgJpZBgBIAEoCzIQLmNvbW1vbi5CYXR0bGVJZFICaWQSOAoKdHVyblVwZGF0ZRgCIAEoCzIWLmJhdHRsZS5UdXJuUXVldWVFdmVudEgAUgp0dXJuVXBkYXRlEi0KBmFjdGlvbhgDIAEoCzITLmJhdHRsZS5BY3Rpb25FdmVudEgAUgZhY3Rpb24SQgoNYmF0dGxlU3RhcnRlZBgEIAEoCzIaLmJhdHRsZS5CYXR0bGVTdGFydGVkRXZlbnRIAFINYmF0dGxlU3RhcnRlZBI8CgtiYXR0bGVFbmRlZBgFIAEoCzIYLmJhdHRsZS5CYXR0bGVFbmRlZEV2ZW50SABSC2JhdHRsZUVuZGVkEjcKC2JhdHRsZUFkZGVkGAYgASgLMhMuYmF0dGxlLkJhdHRsZUFkZGVkSABSC2JhdHRsZUFkZGVkQgcKBWV2ZW50');
@$core.Deprecated('Use battleAddedDescriptor instead')
const BattleAdded$json = const {
  '1': 'BattleAdded',
  '2': const [
    const {'1': 'b', '3': 1, '4': 1, '5': 11, '6': '.battle.Battle', '10': 'b'},
  ],
};

/// Descriptor for `BattleAdded`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleAddedDescriptor = $convert.base64Decode('CgtCYXR0bGVBZGRlZBIcCgFiGAEgASgLMg4uYmF0dGxlLkJhdHRsZVIBYg==');
@$core.Deprecated('Use battleEndedDescriptor instead')
const BattleEnded$json = const {
  '1': 'BattleEnded',
  '2': const [
    const {'1': 'ev', '3': 1, '4': 1, '5': 11, '6': '.battle.Report', '10': 'ev'},
    const {'1': 'winningTeam', '3': 2, '4': 1, '5': 11, '6': '.battle.Team', '10': 'winningTeam'},
  ],
};

/// Descriptor for `BattleEnded`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleEndedDescriptor = $convert.base64Decode('CgtCYXR0bGVFbmRlZBIeCgJldhgBIAEoCzIOLmJhdHRsZS5SZXBvcnRSAmV2Ei4KC3dpbm5pbmdUZWFtGAIgASgLMgwuYmF0dGxlLlRlYW1SC3dpbm5pbmdUZWFt');
@$core.Deprecated('Use actionEventDescriptor instead')
const ActionEvent$json = const {
  '1': 'ActionEvent',
  '2': const [
    const {'1': 'source', '3': 1, '4': 1, '5': 11, '6': '.common.MonId', '10': 'source'},
    const {'1': 'target', '3': 2, '4': 1, '5': 11, '6': '.common.MonId', '10': 'target'},
    const {'1': 'effect', '3': 3, '4': 1, '5': 11, '6': '.monster.BattleValues', '10': 'effect'},
  ],
};

/// Descriptor for `ActionEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionEventDescriptor = $convert.base64Decode('CgtBY3Rpb25FdmVudBIlCgZzb3VyY2UYASABKAsyDS5jb21tb24uTW9uSWRSBnNvdXJjZRIlCgZ0YXJnZXQYAiABKAsyDS5jb21tb24uTW9uSWRSBnRhcmdldBItCgZlZmZlY3QYAyABKAsyFS5tb25zdGVyLkJhdHRsZVZhbHVlc1IGZWZmZWN0');
@$core.Deprecated('Use battleResponseDescriptor instead')
const BattleResponse$json = const {
  '1': 'BattleResponse',
  '2': const [
    const {'1': 'ev', '3': 1, '4': 1, '5': 11, '6': '.battle.Report', '9': 0, '10': 'ev'},
    const {'1': 'turnU', '3': 3, '4': 1, '5': 11, '6': '.battle.TurnQueueEvent', '9': 0, '10': 'turnU'},
  ],
  '8': const [
    const {'1': 'message'},
  ],
};

/// Descriptor for `BattleResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleResponseDescriptor = $convert.base64Decode('Cg5CYXR0bGVSZXNwb25zZRIgCgJldhgBIAEoCzIOLmJhdHRsZS5SZXBvcnRIAFICZXYSLgoFdHVyblUYAyABKAsyFi5iYXR0bGUuVHVyblF1ZXVlRXZlbnRIAFIFdHVyblVCCQoHbWVzc2FnZQ==');
@$core.Deprecated('Use commandDescriptor instead')
const Command$json = const {
  '1': 'Command',
  '2': const [
    const {'1': 'battleId', '3': 1, '4': 1, '5': 11, '6': '.common.BattleId', '10': 'battleId'},
    const {'1': 'attack', '3': 2, '4': 1, '5': 11, '6': '.battle.Attack', '9': 0, '10': 'attack'},
    const {'1': 'random', '3': 3, '4': 1, '5': 11, '6': '.battle.RandomCommand', '9': 0, '10': 'random'},
  ],
  '8': const [
    const {'1': 'command'},
  ],
};

/// Descriptor for `Command`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List commandDescriptor = $convert.base64Decode('CgdDb21tYW5kEiwKCGJhdHRsZUlkGAEgASgLMhAuY29tbW9uLkJhdHRsZUlkUghiYXR0bGVJZBIoCgZhdHRhY2sYAiABKAsyDi5iYXR0bGUuQXR0YWNrSABSBmF0dGFjaxIvCgZyYW5kb20YAyABKAsyFS5iYXR0bGUuUmFuZG9tQ29tbWFuZEgAUgZyYW5kb21CCQoHY29tbWFuZA==');
@$core.Deprecated('Use battleRequestDescriptor instead')
const BattleRequest$json = const {
  '1': 'BattleRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 11, '6': '.common.BattleId', '10': 'id'},
    const {'1': 'monIds', '3': 2, '4': 3, '5': 11, '6': '.common.MonId', '10': 'monIds'},
    const {'1': 'userId', '3': 3, '4': 1, '5': 11, '6': '.common.UserId', '10': 'userId'},
  ],
};

/// Descriptor for `BattleRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleRequestDescriptor = $convert.base64Decode('Cg1CYXR0bGVSZXF1ZXN0EiAKAmlkGAEgASgLMhAuY29tbW9uLkJhdHRsZUlkUgJpZBIlCgZtb25JZHMYAiADKAsyDS5jb21tb24uTW9uSWRSBm1vbklkcxImCgZ1c2VySWQYAyABKAsyDi5jb21tb24uVXNlcklkUgZ1c2VySWQ=');
@$core.Deprecated('Use battleListDescriptor instead')
const BattleList$json = const {
  '1': 'BattleList',
  '2': const [
    const {'1': 'battles', '3': 1, '4': 3, '5': 11, '6': '.battle.Battle', '10': 'battles'},
  ],
};

/// Descriptor for `BattleList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List battleListDescriptor = $convert.base64Decode('CgpCYXR0bGVMaXN0EigKB2JhdHRsZXMYASADKAsyDi5iYXR0bGUuQmF0dGxlUgdiYXR0bGVz');
@$core.Deprecated('Use responseStatusDescriptor instead')
const ResponseStatus$json = const {
  '1': 'ResponseStatus',
  '2': const [
    const {'1': 'success', '3': 1, '4': 1, '5': 8, '10': 'success'},
  ],
};

/// Descriptor for `ResponseStatus`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List responseStatusDescriptor = $convert.base64Decode('Cg5SZXNwb25zZVN0YXR1cxIYCgdzdWNjZXNzGAEgASgIUgdzdWNjZXNz');
@$core.Deprecated('Use randomCommandDescriptor instead')
const RandomCommand$json = const {
  '1': 'RandomCommand',
  '2': const [
    const {'1': 'source', '3': 1, '4': 1, '5': 11, '6': '.common.MonId', '10': 'source'},
  ],
};

/// Descriptor for `RandomCommand`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List randomCommandDescriptor = $convert.base64Decode('Cg1SYW5kb21Db21tYW5kEiUKBnNvdXJjZRgBIAEoCzINLmNvbW1vbi5Nb25JZFIGc291cmNl');
@$core.Deprecated('Use attackDescriptor instead')
const Attack$json = const {
  '1': 'Attack',
  '2': const [
    const {'1': 'source', '3': 1, '4': 1, '5': 11, '6': '.common.MonId', '10': 'source'},
    const {'1': 'target', '3': 2, '4': 1, '5': 11, '6': '.common.MonId', '10': 'target'},
  ],
};

/// Descriptor for `Attack`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List attackDescriptor = $convert.base64Decode('CgZBdHRhY2sSJQoGc291cmNlGAEgASgLMg0uY29tbW9uLk1vbklkUgZzb3VyY2USJQoGdGFyZ2V0GAIgASgLMg0uY29tbW9uLk1vbklkUgZ0YXJnZXQ=');
